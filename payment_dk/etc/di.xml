<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <virtualType name="Afterpay\Payment\Gateway\Config\DigitalInvoiceDK\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_DI</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\DigitalInvoiceDKB2B\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_B2B_DI</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\CampaignDK\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_CP</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\Campaign2DK\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_CP2</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\Campaign3DK\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_CP3</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\FlexDK\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_FX</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\InstallmentDK\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_IN</argument>
        </arguments>
    </virtualType>


    <!-- 14-day invoice DK -->


    <virtualType name="DigitalInvoiceDKFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_DI</argument>
            <argument name="valueHandlerPool" xsi:type="object">DigitalInvoiceDKValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">DIDKCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">DIDKValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">DIDKAuthorizeCommand</item>
                <item name="capture" xsi:type="string">DIDKCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">DIDKSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">DIDKRefundCommand</item>
                <item name="void" xsi:type="string">DIDKVoidCommand</item>
                <item name="cancel" xsi:type="string">DIDKVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">DIDKConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">DIDKCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="DigitalInvoiceDKValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">DIDKConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">DIDKCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Campaign invoice FI -->


    <virtualType name="CampaignDKFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_CP</argument>
            <argument name="valueHandlerPool" xsi:type="object">CampaignDKValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CPDKCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CPDKValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CPDKAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CPDKCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CPDKSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CPDKRefundCommand</item>
                <item name="void" xsi:type="string">CPDKVoidCommand</item>
                <item name="cancel" xsi:type="string">CPDKVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPDKTransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CPDKConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CPDKCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\CampaignDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CampaignDKValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CPDKConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\CampaignDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPDKValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CPDKCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- SECOND Campaign invoice DK -->


    <virtualType name="Campaign2DKFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_CP2</argument>
            <argument name="valueHandlerPool" xsi:type="object">Campaign2DKValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CP2DKCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CP2DKValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CP2DKAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CP2DKCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CP2DKSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CP2DKRefundCommand</item>
                <item name="void" xsi:type="string">CP2DKVoidCommand</item>
                <item name="cancel" xsi:type="string">CP2DKVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2DKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2DKTransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CP2DKConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CP2DKCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2DKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2DKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign2DK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Campaign2DKValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CP2DKConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign2DK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2DKValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CP2DKCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- THIRD Campaign invoice DK -->


    <virtualType name="Campaign3DKFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_CP3</argument>
            <argument name="valueHandlerPool" xsi:type="object">Campaign3DKValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CP3DKCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CP3DKValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CP3DKAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CP3DKCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CP3DKSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CP3DKRefundCommand</item>
                <item name="void" xsi:type="string">CP3DKVoidCommand</item>
                <item name="cancel" xsi:type="string">CP3DKVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3DKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3DKTransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CP3DKConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CP3DKCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3DKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3DKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign3DK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Campaign3DKValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CP3DKConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign3DK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3DKValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CP3DKCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Flex payment DK -->


    <virtualType name="FlexDKFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_FX</argument>
            <argument name="valueHandlerPool" xsi:type="object">FlexDKValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">FXDKCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">FXDKValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">FXDKAuthorizeCommand</item>
                <item name="capture" xsi:type="string">FXDKCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">FXDKSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">FXDKRefundCommand</item>
                <item name="void" xsi:type="string">FXDKVoidCommand</item>
                <item name="cancel" xsi:type="string">FXDKVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">FXDKAuthorizeRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">FXDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">FXDKConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKAuthorizeRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="payment_flex" xsi:type="string">Afterpay\Payment\Gateway\Request\PaymentFlexDataBuilder</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">FXDKCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="FlexDKValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">FXDKConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXDKValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">FXDKCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Fixed instalments DK -->


    <virtualType name="InstallmentDKFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_IN</argument>
            <argument name="valueHandlerPool" xsi:type="object">InstallmentDKValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">INDKCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">INDKValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">INDKAuthorizeCommand</item>
                <item name="capture" xsi:type="string">INDKCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">INDKSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">INDKRefundCommand</item>
                <item name="void" xsi:type="string">INDKVoidCommand</item>
                <item name="cancel" xsi:type="string">INDKVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">INSEAuthorizeRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">INDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">INDKConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">INDKCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INDKTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="InstallmentDKValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">INDKConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentDK\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="INDKValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">INDKCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>

    <!-- 14-day invoice DK B2B -->

    <virtualType name="DigitalInvoiceDKB2BFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_B2B_DI</argument>
            <argument name="valueHandlerPool" xsi:type="object">DigitalInvoiceDKB2BValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">DIDKB2BCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">DIDKB2BValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">DIDKB2BAuthorizeCommand</item>
                <item name="capture" xsi:type="string">DIDKB2BCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">DIDKB2BSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">DIDKB2BRefundCommand</item>
                <item name="void" xsi:type="string">DIDKB2BVoidCommand</item>
                <item name="cancel" xsi:type="string">DIDKB2BVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">DIDKB2BRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">DIDKB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">DIDKB2BConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">DIDKB2BCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">DIDKB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDKB2B\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="DigitalInvoiceDKB2BValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">DIDKB2BConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDKB2B\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">DIDKB2BCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="DIDKB2BRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="customer_business" xsi:type="string">Afterpay\Payment\Gateway\Request\BusinessCustomerDataRestBuilder</item>
            </argument>
        </arguments>
    </virtualType>
</config>
