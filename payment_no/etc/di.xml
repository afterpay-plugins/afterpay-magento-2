<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <virtualType name="Afterpay\Payment\Gateway\Config\OpenInvoiceNO\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_OI</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\OpenInvoiceNOB2B\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_B2B_OI</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\CampaignNO\Config" type="Magento\Payment\Gateway\Config\Config">
    <arguments>
        <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_CP</argument>
    </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\Campaign2NO\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_CP2</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\Campaign3NO\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_CP3</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\InstallmentNO\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_IN</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Gateway\Config\FlexNO\Config" type="Magento\Payment\Gateway\Config\Config">
        <arguments>
            <argument name="methodCode" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_FX</argument>
        </arguments>
    </virtualType>


    <!-- OPEN INVOICE NO -->


    <virtualType name="OpenInvoiceNOFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_OI</argument>
            <argument name="valueHandlerPool" xsi:type="object">OpenInvoiceNOValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">OINOCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">OINOValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">OINOAuthorizeCommand</item>
                <item name="capture" xsi:type="string">OINOCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">OINOSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">OINORefundCommand</item>
                <item name="void" xsi:type="string">OINOVoidCommand</item>
                <item name="cancel" xsi:type="string">OINOVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">OINOConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">OINOCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINORefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OpenInvoiceNOValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">OINOConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">OINOCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Campaign invoice NO -->


    <virtualType name="CampaignNOFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_CP</argument>
            <argument name="valueHandlerPool" xsi:type="object">CampaignNOValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CPNOCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CPNOValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CPNOAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CPNOCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CPNOSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CPNORefundCommand</item>
                <item name="void" xsi:type="string">CPNOVoidCommand</item>
                <item name="cancel" xsi:type="string">CPNOVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPNOTransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CPNOConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CPNOCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNORefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CPNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\CampaignNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CampaignNOValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CPNOConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\CampaignNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CPNOValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CPNOCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- SECOND Campaign invoice NO -->


    <virtualType name="Campaign2NOFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_CP2</argument>
            <argument name="valueHandlerPool" xsi:type="object">Campaign2NOValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CP2NOCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CP2NOValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CP2NOAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CP2NOCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CP2NOSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CP2NORefundCommand</item>
                <item name="void" xsi:type="string">CP2NOVoidCommand</item>
                <item name="cancel" xsi:type="string">CP2NOVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2NOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2NOTransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CP2NOConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CP2NOCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2NOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NORefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP2NOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign2NO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Campaign2NOValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CP2NOConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign2NO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP2NOValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CP2NOCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- THIRD Campaign invoice NO -->


    <virtualType name="Campaign3NOFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_CP3</argument>
            <argument name="valueHandlerPool" xsi:type="object">Campaign3NOValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">CP3NOCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">CP3NOValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">CP3NOAuthorizeCommand</item>
                <item name="capture" xsi:type="string">CP3NOCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">CP3NOSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">CP3NORefundCommand</item>
                <item name="void" xsi:type="string">CP3NOVoidCommand</item>
                <item name="cancel" xsi:type="string">CP3NOVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3NOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3NOTransferFactory</argument>
            <argument name="requestBuilder" xsi:type="object">CPSEAuthorizeRequestBuilder</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">CP3NOConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">CP3NOCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3NOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NORefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">CP3NOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign3NO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Campaign3NOValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">CP3NOConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\Campaign3NO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="CP3NOValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">CP3NOCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Flex payment NO -->


    <virtualType name="FlexNOFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_FX</argument>
            <argument name="valueHandlerPool" xsi:type="object">FlexNOValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">FXNOCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">FXNOValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">FXNOAuthorizeCommand</item>
                <item name="capture" xsi:type="string">FXNOCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">FXNOSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">FXNORefundCommand</item>
                <item name="void" xsi:type="string">FXNOVoidCommand</item>
                <item name="cancel" xsi:type="string">FXNOVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">FXNOAuthorizeRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">FXNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">FXNOConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOAuthorizeRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="payment_flex" xsi:type="string">Afterpay\Payment\Gateway\Request\PaymentFlexDataBuilder</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">FXNOCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNORefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">FXNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="FlexNOValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">FXNOConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="FXNOValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">FXNOCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- Fixed instalments NO -->
    <virtualType name="InstallmentNOFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_IN</argument>
            <argument name="valueHandlerPool" xsi:type="object">InstallmentNOValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">INNOCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">INNOValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">INNOAuthorizeCommand</item>
                <item name="capture" xsi:type="string">INNOCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">INNOSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">INNORefundCommand</item>
                <item name="void" xsi:type="string">INNOVoidCommand</item>
                <item name="cancel" xsi:type="string">INNOVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">INSEAuthorizeRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">INNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">INNOConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">INNOCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNORefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">INNOTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="InstallmentNOValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">INNOConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="INNOValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">INNOCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>


    <!-- OPEN INVOICE NO B2B -->

    <virtualType name="OpenInvoiceNOB2BFacade" type="DigitalInvoiceDEFacade">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_B2B_OI</argument>
            <argument name="valueHandlerPool" xsi:type="object">OpenInvoiceNOB2BValueHandlerPool</argument>
            <argument name="commandPool" xsi:type="object">OINOB2BCommandPool</argument>
            <argument name="validatorPool" xsi:type="object">OINOB2BValidatorPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BCommandPool" type="DigitalInvoiceNLCommandPool">
        <arguments>
            <argument name="commands" xsi:type="array">
                <item name="authorize" xsi:type="string">OINOB2BAuthorizeCommand</item>
                <item name="capture" xsi:type="string">OINOB2BCaptureStrategyCommand</item>
                <item name="sale" xsi:type="string">OINOB2BSaleCaptureCommand</item>
                <item name="refund" xsi:type="string">OINOB2BRefundCommand</item>
                <item name="void" xsi:type="string">OINOB2BVoidCommand</item>
                <item name="cancel" xsi:type="string">OINOB2BVoidCommand</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BVoidCommand" type="DigitalInvoiceDEVoidCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BAuthorizeCommand" type="DigitalInvoiceDEAuthorizeCommand">
        <arguments>
            <argument name="requestBuilder" xsi:type="object">OINOB2BRequestBuilder</argument>
            <argument name="transferFactory" xsi:type="object">OINOB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BTransferFactory" type="Afterpay\Payment\Gateway\Http\TransferFactory">
        <arguments>
            <argument name="configValueHandler" xsi:type="object">OINOB2BConfigValueHandler</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BCaptureStrategyCommand" type="CaptureStrategyCommand">
        <arguments>
            <argument name="commandPool" xsi:type="object">OINOB2BCommandPool</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BSaleCaptureCommand" type="DINLSaleCaptureCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BRefundCommand" type="DigitalInvoiceDERefundCommand">
        <arguments>
            <argument name="transferFactory" xsi:type="object">OINOB2BTransferFactory</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BConfigValueHandler" type="Magento\Payment\Gateway\Config\ConfigValueHandler">
        <arguments>
            <argument name="configInterface" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceNOB2B\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OpenInvoiceNOB2BValueHandlerPool" type="DigitalInvoiceDEValueHandlerPool">
        <arguments>
            <argument name="handlers" xsi:type="array">
                <item name="default" xsi:type="string">OINOB2BConfigValueHandler</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BCountryValidator" type="Magento\Payment\Gateway\Validator\CountryValidator">
        <arguments>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceNOB2B\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BValidatorPool" type="DigitalInvoiceDEValidatorPool">
        <arguments>
            <argument name="validators" xsi:type="array">
                <item name="country" xsi:type="string">OINOB2BCountryValidator</item>
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="OINOB2BRequestBuilder" type="DigitalInvoiceDEAuthorizeRequest">
        <arguments>
            <argument name="builders" xsi:type="array">
                <item name="customer_business" xsi:type="string">Afterpay\Payment\Gateway\Request\BusinessCustomerDataRestBuilder</item>
            </argument>
        </arguments>
    </virtualType>
</config>
