<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Order\Processor;
use Afterpay\Payment\Model\ScaHandler;
use Magento\Sales\Model\Order;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Payment;
use Magento\Framework\DB\Transaction;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Service\InvoiceService;

class CaptureAfterAuthorize implements ObserverInterface
{
    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * @var Processor
     */
    protected $orderProcessor;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @param Advanced $advancedConfig
     * @param Transaction $transaction
     * @param InvoiceService $invoiceService
     * @param Processor $orderProcessor
     * @param OrderRepository $orderRepository
     * @param Data $dataHelper
     */
    public function __construct(
        Advanced $advancedConfig,
        Transaction $transaction,
        InvoiceService $invoiceService,
        Processor $orderProcessor,
        OrderRepository $orderRepository,
        Data $dataHelper
    ) {
        $this->advancedConfig = $advancedConfig;
        $this->transaction = $transaction;
        $this->invoiceService = $invoiceService;
        $this->orderProcessor = $orderProcessor;
        $this->orderRepository = $orderRepository;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param Observer $observer
     *
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');
        /** @var Payment $payment */
        if (isset($order) && $this->dataHelper->isAfterpayOrder($order)
            && $this->advancedConfig->captureModeAuto($order->getStoreId())
            && $order->getStatus() !== ScaHandler::ORDER_STATUS_SCA_PENDING_CODE 
            && $order->getStatus() !== ScaHandler::ORDER_STATUS_HCP_PENDING_CODE
            && $order->hasInvoices() == false
        ) {
            try {
                // We need reset transaction ID data to null because otherwise
                // authorize transaction will get overwritten
                $order->getPayment()->setTransactionId(null);
                $this->orderProcessor->capture($order);
                $order->addCommentToStatusHistory(__('Automatically captured payment after place order action'));
                $this->orderRepository->save($order);
            } catch (LocalizedException $exception) {
                $order->addCommentToStatusHistory(
                    __(
                        'Failed to automatically capture payment after place order action. Error: %1',
                        $exception->getMessage()
                    )
                );
                $this->orderRepository->save($order);
            }
        }
    }
}
