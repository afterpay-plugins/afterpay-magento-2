<?php
/**
 * Copyright (c) 2022  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;

class MyParcelPickUpToShippingAddress implements ObserverInterface
{
    const MYPARCEL_DELIVERY_OPTIONS = 'myparcel_delivery_options';

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        OrderRepository $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();

        if (!empty($order->getData(self::MYPARCEL_DELIVERY_OPTIONS))) {
            $this->addPickUpAddress($order);
        }
    }

    /**
     * @param Order $order
     * @return void
     */
    private function addPickUpAddress(Order $order)
    {
        $deliveryInformation = $order->getData(self::MYPARCEL_DELIVERY_OPTIONS);
        $fullInformation = $this->getInformationAboutDelivery($deliveryInformation);

        if (!isset($fullInformation['pickupLocation'])) {
            return;
        }

        $shippingInfo = $order->getShippingAddress();
        $shippingInfo->setStreet($fullInformation['pickupLocation']['street'] . ' ' . $fullInformation['pickupLocation']['number']);
        $shippingInfo->setCity($fullInformation['pickupLocation']['city']);
        $shippingInfo->setCompany($fullInformation['pickupLocation']['location_name'] ?: '');
        $shippingInfo->setPostcode($fullInformation['pickupLocation']['postal_code']);
        $shippingInfo->setCountryId($fullInformation['pickupLocation']['cc']);
        $shippingInfo->setFirstname($order->getBillingAddress()->getFirstname());
        $shippingInfo->setLastname($order->getBillingAddress()->getLastname());
        $shippingInfo->setEmail($order->getCustomerEmail());
        $shippingInfo->setTelephone($order->getBillingAddress()->getTelephone());

        try {
            $this->orderRepository->save($order);
        } catch (AlreadyExistsException | InputException | NoSuchEntityException $e) {
            return;
        }
    }

    /**
     * @param $information
     * @return mixed
     */
    private function getInformationAboutDelivery($information)
    {
        return json_decode($information, true);
    }
}
