<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Afterpay\Payment\Model\Config\Advanced;
use stdClass;

class ScaRedirectObserver implements ObserverInterface
{
    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * @param Advanced $advancedConfig
     */
    public function __construct(Advanced $advancedConfig) {
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        $response = $this->getResponseData($observer);
        $order = $this->getOrder($observer);
        if (!isset($order)) {
            return;
        }
        $redirectUrl = $this->advancedConfig->isHostedCheckoutEnabled() ? $response->redirectUrl : $response->secureLoginUrl;
        $order->setAfterpayScaUrl($redirectUrl);
    }

    /**
     * Returns response object
     *
     * @param Observer $observer
     *
     * @return stdClass
     */
    protected function getResponseData(Observer $observer): stdClass
    {
        return $observer->getData('response');
    }

    /**
     * @param Observer $observer
     *
     * @return mixed
     */
    protected function getOrder(Observer $observer)
    {
        $payment = $observer->getData('payment');
        return $payment->getPayment()->getOrder();
    }
}
