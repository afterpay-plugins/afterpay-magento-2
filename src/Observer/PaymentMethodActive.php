<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Observer;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Afterpay;
use Afterpay\Payment\Model\Config\Visitor;
use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Afterpay\Payment\Model\Config\Advanced;
use stdClass;

class PaymentMethodActive implements ObserverInterface
{
    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var Visitor
     */
    private $visitor;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var Serialize
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * @param Visitor         $visitor
     * @param Data            $helper
     * @param Afterpay        $afterpay
     * @param Session         $session
     * @param Currency        $currency
     * @param CacheInterface  $cache
     * @param Serialize       $serializer
     * @param StoreInterface  $currentStore
     * @param CheckoutSession $checkoutSession
     * @param Advanced        $advancedConfig
     */
    public function __construct(
        Visitor $visitor,
        Data $helper,
        Afterpay $afterpay,
        Session $session,
        Currency $currency,
        CacheInterface $cache,
        Serialize $serializer,
        StoreManagerInterface $storeManager,
        CheckoutSession $checkoutSession,
        Advanced $advancedConfig
    ) {
        $this->visitor = $visitor;
        $this->helper = $helper;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->cache = $cache;
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * @param Observer $observer
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        /* @var MethodInterface $methodInstance */
        $methodInstance = $event->getData('method_instance');
        /* @var Quote $quote */
        $quote = $event->getData('quote');
        $result = $event->getData('result');

        if ($this->shouldSkip($quote, $result, $methodInstance)) {
            return;
        }

        $available = $this->availablePaymentMethod($methodInstance, $quote)
            && $this->allowedForGroup($methodInstance, $quote)
            && $this->allowedIp($methodInstance)
            && $this->allowedShippingMethod($methodInstance, $quote)
            && $this->allowedAccountMethod($methodInstance, $quote);
        $result->setData('is_available', $available);
    }

    /**
     * @param Quote|null $quote
     * @param DataObject $result
     * @param MethodInterface $methodInstance
     *
     * @return bool
     */
    private function shouldSkip($quote, DataObject $result, MethodInterface $methodInstance): bool
    {
        return $quote === null
            || $result->getData('is_available') === false
            || strpos($methodInstance->getCode(), 'afterpay') !== 0; // does not start with
    }

    /**
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     */
    private function allowedForGroup(MethodInterface $methodInstance, Quote $quote): bool
    {
        if ($methodInstance->getConfigData('allowspecificgroup')) {
            $allowedGroups = explode(',', $methodInstance->getConfigData('allowspecificgroup'));

            return in_array((string)$quote->getCustomerGroupId(), $allowedGroups, true);
        }

        return true;
    }

    /**
     * @param MethodInterface $methodInstance
     *
     * @return bool
     */
    private function allowedIp(MethodInterface $methodInstance): bool
    {
        if ($methodInstance->getConfigData('restrict')) {
            return $this->visitor->allowedByIp();
        }

        return true;
    }

    /**
     * Is allowed payment for shipping method or not
     *
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     */
    private function allowedShippingMethod(MethodInterface $methodInstance, Quote $quote): bool
    {
        $config = $methodInstance->getConfigData('excludeships');
        if ($config === null) {
            return true;
        }
        $methods = explode(',', $config);
        $shippingMethod = $quote->getShippingAddress()->getShippingMethod();

        if(isset($shippingMethod)) {
            if(strpos($shippingMethod, '_matrixrate') !== false) {
                foreach($methods as $method) {
                    if(strpos($method, '_matrixrate') !== false){
                        return false;
                    }
                }
                return true;
            }
    
            if(strpos($shippingMethod, '_sendcloud') !== false) {
                foreach($methods as $method) {
                    if(strpos($method, '_sendcloud') !== false){
                        return false;
                    }
                }
                return true;
            }

            return !in_array($shippingMethod, $methods, true);
        }
        else {
            return true;
        }
    }

    /**
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function allowedAccountMethod(MethodInterface $methodInstance, Quote $quote): bool
    {
        $billingAddress = $quote->getBillingAddress();
        $country = $billingAddress->getData('country_id');
        if (!isset($country)) {
            $shippingAddress = $quote->getShippingAddress();
            $country = $shippingAddress->getData('country_id');
        }
        if (strtoupper(explode('_', $methodInstance->getCode())[1]) == $country) {
            if (\in_array($methodInstance->getCode(), Data::$allowedMethods, true)) {
                $auth = $this->helper->getConfiguration($methodInstance->getCode(), $quote->getStoreId());
                $cacheKey = sprintf('%s_%s_%s', $auth['apiKey'], $quote->getGrandTotal(), $quote->getStoreId());
                $cacheLifetime = 5;
                $cache = $this->cache->load($cacheKey);
                if ($cache) {
                    $data = $this->serializer->unserialize($cache);
                    $data = json_decode(json_encode($data), false);
                } else {
                    $data = $this->checkoutSession->getAvailablePayments()[$methodInstance->getCode()]['response'];
                    $serializedData = $this->serializer->serialize($data);
                    $this->cache->save($serializedData, $cacheKey, [], $cacheLifetime);
                }
                return $this->parseResponse($data, $methodInstance->getCode());
            }
        }

        return true;
    }

    /**
     * @param stdClass $response
     * @param string $methodCode
     *
     * @return bool
     */
    private function parseResponse(stdClass $response, string $methodCode): bool
    {
        $campaignMethods = [];
        if (property_exists($response, 'paymentMethods')) {
            foreach ($response->paymentMethods as $paymentMethod) {
                if (\in_array($methodCode, Data::$allowedFlex, true)
                ) {
                    if ($paymentMethod->type === 'Account' && property_exists($paymentMethod, 'account')) {
                        $profileNo = $paymentMethod->account->profileNo;
                        $this->session->setAccountProfileNo($profileNo);
                        return true;
                    }
                }
                if (\in_array($methodCode, Data::$allowedInstalment, true)
                ) {
                    if ($paymentMethod->type === 'Installment' && property_exists($paymentMethod, 'installment')) {
                        return true;
                    }
                }
            }
            if (\in_array($methodCode, Data::$allowedCampaigns, true)) {
                foreach ($response->paymentMethods as $paymentMethod) {
                    if ($paymentMethod->type === 'Invoice' && property_exists($paymentMethod, 'campaigns')) {
                        $campaignMethods[] = $paymentMethod;
                    }
                }
                $campaignPosition = $this->getCampaignPosition($methodCode);
                if ($campaignMethods
                    && $campaignPosition !== null
                    && array_key_exists($campaignPosition, $campaignMethods)
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param MethodInterface $methodInstance
     * @param Quote $quote
     *
     * @return bool
     */
    private function availablePaymentMethod(MethodInterface $methodInstance, Quote $quote): bool
    {
        if ((str_contains($methodInstance->getCode(), 'be') || str_contains($methodInstance->getCode(), 'nl'))
            && ! str_contains($methodInstance->getCode(), 'rest')) {
            return true;
        }
        if ((str_contains($methodInstance->getCode(), 'b2b') || str_contains($methodInstance->getCode(), 'business_2_business'))
            && $this->advancedConfig->isHostedCheckoutEnabled()) {
            return false;
        }
        $paymentMethodExist = false;
        $auth = $this->helper->getConfiguration($methodInstance->getCode(), $quote->getStoreId());
        $availablePaymentMethods = $this->getAvailablePaymentsData($auth, $quote, $methodInstance->getCode());
        $availablePaymentMethodsArray = json_decode(json_encode($availablePaymentMethods), true);

        if ($availablePaymentMethodsArray == null) {
            return false;
        }

        if (array_key_exists('paymentMethods', $availablePaymentMethodsArray)) {
            $paymentMethodTypes = array_unique(array_column($availablePaymentMethodsArray['paymentMethods'], 'type'));
        }
        else {
            $paymentMethodTypes = [];
        }

        if (! in_array($this->getPaymentMethodType($methodInstance->getCode()), $paymentMethodTypes)) {
            return false;
        }

        foreach ($availablePaymentMethodsArray['paymentMethods'] as $paymentMethod) {
            if ($paymentMethod['type'] == $this->getPaymentMethodType($methodInstance->getCode())) {
                if ($paymentMethod['type'] == 'Invoice') {
                    if (str_contains($methodInstance->getCode(), 'direct_debit') && array_key_exists('directDebit', $paymentMethod)
                        && $paymentMethod['directDebit']['available'] == true
                    ) {
                        $paymentMethodExist = true;
                        break;
                    }
                    elseif (str_contains($methodInstance->getCode(), 'campaign') && array_key_exists('campaigns', $paymentMethod)) {
                        $paymentMethodExist = true;
                        break;
                    }
                    else {
                        if (! str_contains($methodInstance->getCode(), 'direct_debit') && ! str_contains($methodInstance->getCode(), 'campaign')
                            && ! array_key_exists('directDebit', $paymentMethod) && ! array_key_exists('campaigns', $paymentMethod)
                        ) {
                            $paymentMethodExist = true;
                            break;
                        }
                    }
                }
                else {
                    if ($paymentMethod['type'] == 'Installment') {
                        $availableInstallmentPlans['availableInstallmentPlans'] = array_column($availablePaymentMethodsArray['paymentMethods'], 'installment');
                        for ($i = 0; $i <= count($availableInstallmentPlans['availableInstallmentPlans']) - 1; $i++) {
                            $availableInstallmentPlans['availableInstallmentPlans'][$i] = (object) $availableInstallmentPlans['availableInstallmentPlans'][$i];
                        }
                        $this->checkoutSession->setAvailableInstallmentPlans((object) $availableInstallmentPlans);
                    }
                    if ($paymentMethod['type'] == 'PayinX') {
                        $availablePayinxDueAmount = array_column($paymentMethod['payInX'], 'dueAmount')[0];
                        $this->checkoutSession->setAvailablePayinxDueAmount($availablePayinxDueAmount);
                    }
                    $paymentMethodExist = true;
                    break;
                }
            }
        }

        return $paymentMethodExist;
    }

    /**
     * @param array $auth
     * @param Quote $quote
     * @param string $methodCode
     *
     * @return mixed
     */
    private function getAvailablePaymentsData(array $auth, Quote $quote, $methodCode)
    {
        $billingAddress = $quote->getBillingAddress();
        $country = $billingAddress->getData('country_id');
        if (!isset($country)) {
            $shippingAddress = $quote->getShippingAddress();
            $country = $shippingAddress->getData('country_id');
        }
        if (strtoupper(explode('_', $methodCode)[1]) == $country) {
            $responseData = $this->checkoutSession->getAvailablePayments() ?? [];
            if (! array_key_exists($methodCode, $responseData)) {
                $newAvailablePayments = $this->helper->getAvailablePayments($auth, $quote);
                if (! empty($responseData)) {
                    if ($this->checkoutSession->getMethodCountry() != $country) {
                        $countryAvailablePayments = [];
                    }
                    else {
                        $countryAvailablePayments = $responseData;
                    }
                }
                $countryAvailablePayments[$methodCode]['total_gross_amount'] = round((float)$quote->getGrandTotal(), 2);
                $countryAvailablePayments[$methodCode]['api_key'] = $auth['apiKey'];
                $countryAvailablePayments[$methodCode]['connection_mode'] = $auth['mode'];
                $countryAvailablePayments[$methodCode]['conversation_language'] = $this->helper->getConversationLanguage();
                $countryAvailablePayments[$methodCode]['currency'] = $this->storeManager->getStore()->getCurrentCurrencyCode();
                $countryAvailablePayments[$methodCode]['response'] = $newAvailablePayments;
                $this->checkoutSession->setMethodCountry($country);
                $this->checkoutSession->setAvailablePayments($countryAvailablePayments);
                $responseData = $this->checkoutSession->getAvailablePayments();
            }
            else {
                if ($responseData[$methodCode]['api_key'] != $auth['apiKey']
				    || $responseData[$methodCode]['connection_mode'] != $auth['mode']
                    || $responseData[$methodCode]['conversation_language'] != $this->helper->getConversationLanguage()
                    || $responseData[$methodCode]['currency'] != $this->storeManager->getStore()->getCurrentCurrencyCode()
                    || $responseData[$methodCode]['total_gross_amount'] != round((float)$quote->getGrandTotal(), 2)) {
                        unset($responseData[$methodCode]);
                        $newAvailablePayments = $this->helper->getAvailablePayments($auth, $quote);
                        $countryAvailablePayments = $this->checkoutSession->getAvailablePayments();
                        $countryAvailablePayments[$methodCode]['total_gross_amount'] = round((float)$quote->getGrandTotal(), 2);
                        $countryAvailablePayments[$methodCode]['api_key'] = $auth['apiKey'];
                        $countryAvailablePayments[$methodCode]['connection_mode'] = $auth['mode'];
                        $countryAvailablePayments[$methodCode]['conversation_language'] = $this->helper->getConversationLanguage();
                        $countryAvailablePayments[$methodCode]['currency'] = $this->storeManager->getStore()->getCurrentCurrencyCode();
                        $countryAvailablePayments[$methodCode]['response'] = $newAvailablePayments;
                        $this->checkoutSession->setMethodCountry($country);
                        $this->checkoutSession->setAvailablePayments($countryAvailablePayments);
                        $responseData = $this->checkoutSession->getAvailablePayments();
                }
            }
        }
        else {
            $responseData = null;
            return $responseData;
        }
        return $responseData[$methodCode]['response'];
    }

    /**
     * Gets position to which use from the response, as the response can contain multiple campaign invoices
     *
     * @param string $methodCode
     *
     * @return int|null
     */
    private function getCampaignPosition(string $methodCode): ?int
    {
        $positionMapping = [
            'campaign' => 0,
            'campaign2' => 1,
            'campaign3' => 2,
        ];
        $splitMethodCode = explode('_', $methodCode);
        $normalizedString = end($splitMethodCode);
        return $positionMapping[$normalizedString] ?? null;
    }

    /**
     * @param string $methodCode
     *
     * @return string
     */
    private function getPaymentMethodType($methodCode): string
    {
        if (str_contains($methodCode, 'installment')) {
			$paymentMethodType = 'Installment';
		}
		elseif (str_contains($methodCode, 'flex')) {
			$paymentMethodType = 'Account';
		}
        elseif (str_contains($methodCode, 'payinx')) {
            $paymentMethodType = 'PayinX';
        }
		else {
			$paymentMethodType = 'Invoice';
		}

		return $paymentMethodType;
    }
}
