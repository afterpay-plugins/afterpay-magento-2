<?php

/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */

namespace Afterpay\Payment\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;

class GermanFieldValidation extends Field
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * Validating payment methods fields  
     *  
     * @return void
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $installmentIsEnabled = $this->scopeConfig->getValue('payment/afterpay_de_installment/active');
        $directDebitIsEnabled = $this->scopeConfig->getValue('payment/afterpay_de_direct_debit/active');
        $invoiceIsEnabled = $this->scopeConfig->getValue('payment/afterpay_de_invoice/active');

        if (strpos($element->getName(),"[afterpay_de_installment]") || strpos($element->getName(),"[afterpay_de_direct_debit]")) {
            if ($invoiceIsEnabled == 0) {
                $element->setDisabled('disabled');
            }
        } else {
            if ($directDebitIsEnabled == 1 || $installmentIsEnabled == 1) {
                $element->setDisabled('disabled');
            }
        }

        return $element->getElementHtml();
    }
}