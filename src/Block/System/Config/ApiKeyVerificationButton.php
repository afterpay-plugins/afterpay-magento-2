<?php
/**
 * Copyright (c) 2023  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2023 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Block\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\Js;
use Magento\Store\Model\StoreManagerInterface;

class ApiKeyVerificationButton extends Field
{
    /**
     * @var Js
     */
    protected $jsHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Context $context
     * @param Js $jsHelper
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Js $jsHelper,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {

        parent::__construct($context, $data);
        $this->jsHelper = $jsHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * Return script and button for api key verification
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $fieldPath = explode('/', $element->getFieldConfig()['path']);
        $fieldId = $element->getFieldConfig()['id'];
        $paymentMethodCode = $fieldPath[4];
        $connectionMode = explode('_', $fieldId)[0];
        $paymentMethodCountry = explode('_', $fieldPath[3])[3];

        $html = /** @lang HTML */
            '<button id="' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_button" class="riverty-api-key-verification-button" type="button">
                <span class="state-closed">Test API key</span>
             </button>
             <div class="api-key-verification-response" id="' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response">
                <span class="api-key-verification-check-mark" id="' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_check_mark"></span>
                <span id="' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_message"></span>
             </div>';

        $jsString = /** @lang JavaScript */
             'let timeout = null;
              let preloader_timeout = null;
              $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_button").click(function () {
                clearTimeout(preloader_timeout);
                clearTimeout(timeout);
                var body = $("body").loader();
                preloader_timeout = setTimeout(showApiKeyVerificationloader, 500);
                timeout = setTimeout(sendApiKeyVerificationRequest, 1000);

                function showApiKeyVerificationloader() {
                    $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").css("display", "none");
                    body.loader("show");
                }
                
                function sendApiKeyVerificationRequest() {
                    var payload = {
                        paymentMethodCode: "' . $paymentMethodCode . '",
                        apiKey: $("input[name=\'groups[afterpay_payment_methods][groups][afterpay_payment_countries][groups][afterpay_payment_methods_' . $paymentMethodCountry . '][groups][' . $paymentMethodCode . '][fields][' . $connectionMode . '_api_key][value]\']").val(),
                        connectionType: "' . $connectionMode . '",
                    };
        
                    $.ajax({
                        url: "' . $baseUrl . 'rest/V1/afterpay/apikey/verify",
                        type: "POST",
                        data: JSON.stringify(payload),
                        async: false,
                        contentType: "application/json",
                        beforeSend: function(xhr){
                        }
                    }).done(function (response) {
                        body.loader("hide");
                        if ( response[0] ) {
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_check_mark").css("display", "inline-block");
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").removeClass("invalid-api-key-verfication-response");
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").css("display", "inline-block");
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_message").text(response[1]);
                        }
                        else {
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_check_mark").css("display", "none");
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").addClass("invalid-api-key-verfication-response");
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").css("display", "inline-block");
                            $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_message").text(response[1]);
                        }
                    }).fail(function (response) {
                        body.loader("hide");
                        $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_check_mark").css("display", "none");
                        $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").addClass("invalid-api-key-verfication-response");
                        $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_response").css("display", "inline-block");
                        $("#' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_message").text("something wrong happened , please try again");
                    });
                }
              });';   

        $cssStyleString = /** css style */
                            '<style>
                                label[for="payment_other_afterpay_payment_methods_afterpay_payment_countries_afterpay_payment_methods_' . $paymentMethodCountry . '_' . $paymentMethodCode . '_' . $connectionMode . '_api_key_verification_button"] {
                                    display: none;
                                }
                            </style>';

        return $html . $this->jsHelper->getScript(
            'require([\'jquery\',\'mage/url\'],
                    function($, urlBuilder){$(document).ready(function(){' . $jsString . '});}
                );'
        ) . $cssStyleString;
    }
}
