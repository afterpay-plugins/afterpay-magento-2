<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Plugin;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Order\Processor;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Registry;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Model\Order\Shipment\Item;
use Magento\Sales\Model\Service\InvoiceService;

class ShipmentPlugin
{
    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var InvoiceManagementInterface
     */
    protected $invoiceManagement;
    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * @var Processor
     */
    protected $orderProcessor;

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var InvoiceRepositoryInterface
     */
    protected $invoiceRepository;
    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @param Advanced $advancedConfig
     * @param Processor $orderProcessor
     * @param Data $dataHelper
     * @param TransactionFactory $transactionFactory
     * @param InvoiceManagementInterface $invoiceManagement
     * @param Registry $registry
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param InvoiceService $invoiceService
     */
    public function __construct(
        Advanced $advancedConfig,
        Processor $orderProcessor,
        Data $dataHelper,
        TransactionFactory $transactionFactory,
        InvoiceManagementInterface $invoiceManagement,
        Registry $registry,
        InvoiceRepositoryInterface $invoiceRepository,
        InvoiceService $invoiceService
    ) {
        $this->advancedConfig = $advancedConfig;
        $this->orderProcessor = $orderProcessor;
        $this->dataHelper = $dataHelper;
        $this->transactionFactory = $transactionFactory;
        $this->invoiceManagement = $invoiceManagement;
        $this->registry = $registry;
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceService = $invoiceService;
    }

    /**
     * Before shipment registration try and invoice the order if config has been set
     *
     * @param ShipmentInterface|Shipment $subject
     *
     * @throws \Exception
     */
    public function beforeRegister(ShipmentInterface $subject)
    {
        $order = $subject->getOrder();

        if ($this->invoiceOnShipmentAvailable($order)) {
            try {
                $this->orderProcessor->capture($order);
                $order->addCommentToStatusHistory(
                    __('Invoice created automatically on shipment')
                );
            } catch (LocalizedException $e) {
                $order->addCommentToStatusHistory(__('Auto invoice on shipment failed: %1', $e->getMessage()));
            }
        } else {
            if ($this->invoiceOnShipmentForceAvailable($order)) {
                $invoiceQty = [];
                /**
                 * @var Invoice $invoice
                 */
                $invoice = $this->getInvoice($order);
                if ((int) $invoice->getState() !== Invoice::STATE_CANCELED) {
                    $invoice->setState(Invoice::STATE_OPEN);
                    $this->invoiceRepository->save($invoice);
                }
                if ($subject->getTotalQty() === (int) $order->getTotalQtyOrdered()) {
                    $invoice->getOrder()->setIsInProcess(true);
                    $this->invoiceManagement->setCapture($invoice->getEntityId());
                    $this->transactionFactory->create()->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    )->save();
                    $order->addCommentToStatusHistory(__('Capture created automatically on shipment'));

                    return;
                }
                if ((int) $invoice->getState() === Invoice::STATE_OPEN) {
                    $invoice->cancel();
                    $invoice->getOrder()->setIsInProcess(true);
                    $this->transactionFactory->create()->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    )->save();
                    $orderMessage = sprintf(
                        'Original invoice %s was cancelled and new partial invoice was created',
                        $invoice->getIncrementId()
                    );
                    $order->addCommentToStatusHistory($orderMessage);
                }
                foreach ($subject->getItems() as $item) {
                    /** @var $item Item */
                    $invoiceQty[$item->getOrderItemId()] = $item->getQty();
                }
                $originalInvoice = $invoice;
                $invoice = $this->invoiceService->prepareInvoice($order, $invoiceQty);
                $this->registry->unregister('current_invoice');
                $this->registry->register('current_invoice', $invoice);
                if (!$invoice->getTotalQty()) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __("The invoice can't be created without products. Add products and try again.")
                    );
                }
                $invoice->setRequestedCaptureCase(Invoice::CAPTURE_ONLINE);
                $invoice->setIncrementId($this->getNextIncrementId($originalInvoice));
                $invoice->register();
                $this->transactionFactory->create()->addObject(
                    $invoice
                )->addObject(
                    $invoice->getOrder()
                )->save();
                $order->addCommentToStatusHistory(__('Partial capture created automatically on shipment'));
            }
        }
    }

    /**
     * @param OrderInterface $order
     *
     * @return bool
     */
    private function invoiceOnShipmentAvailable(OrderInterface $order): bool
    {
        return $this->dataHelper->isAfterpayOrder($order)
            && $this->advancedConfig->captureModeShipping($order->getStoreId());
    }

    /**
     * @param OrderInterface $order
     *
     * @return bool
     */
    private function invoiceOnShipmentForceAvailable(OrderInterface $order): bool
    {
        return $this->dataHelper->isAfterpayOrder($order)
            && $this->advancedConfig->forceInvoiceShipment($order->getStoreId());
    }

    /**
     * @param $order
     *
     * @return mixed
     */
    private function getInvoice($order)
    {
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->registry->register('current_invoice', $invoice);
        return $invoice;
    }

    /**
     * @param Invoice $invoice
     *
     * @return string
     */
    private function getNextIncrementId(Invoice $invoice): string
    {
        $originalIncrementId = $invoice->getIncrementId();
        $count = (int) $invoice->getOrder()->getInvoiceCollection()->getTotalCount() + 1;
        return sprintf('%s-%s', $originalIncrementId, $count);
    }
}
