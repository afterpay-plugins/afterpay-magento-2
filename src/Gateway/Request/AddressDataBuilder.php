<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Helper\SubjectReader;

class AddressDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * AddressDataBuilder constructor.
     *
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $order = $paymentDO->getOrder();
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress() ?: $billingAddress;
        $addresses = $paymentDO->getPayment()->getOrder()->getAddresses();
        // Shipping address is the first element
        $splitShippingAddress = $this->getSplitStreet(
            implode(' ', array_values($addresses)[0]->getStreet())
        );
        // Billing address is the second element
        $splitBillingAddress = $this->getSplitStreet(
            implode(' ', array_values($addresses)[1]->getStreet())
        );
        $result = [
            'billtoaddress' => [
                'isocountrycode' => $billingAddress->getCountryId(),
                'city' => $billingAddress->getCity(),
                'postalcode' => $billingAddress->getPostcode(),
                'streetname' => $splitBillingAddress['streetname'],
                'housenumber' => $splitBillingAddress['housenumber'],
                'housenumberaddition' => $splitBillingAddress['houseNumberAddition']
            ],
            'shiptoaddress' => [
                'isocountrycode' => $shippingAddress->getCountryId(),
                'city' => $shippingAddress->getCity(),
                'postalcode' => $shippingAddress->getPostcode(),
                'streetname' => $splitShippingAddress['streetname'],
                'housenumber' => $splitShippingAddress['housenumber'],
                'housenumberaddition' => $splitShippingAddress['houseNumberAddition'],
            ]
        ];

        return $result;
    }

    /**
     * Split address
     *
     * @param $address
     * @param $attach_single_extension
     * @param $attach_whole_extension
     *
     * @return array
     * @throws LocalizedException
     */
    protected function getSplitStreet(
        $address,
        $attach_single_extension = false,
        $attach_whole_extension = false
    ) {
        $address = is_array($address) ? implode($address, ' ') : $address;
        $ret = [
            'streetname' => '',
            'housenumber' => '',
            'houseNumberAddition' => '',
        ];

        if (preg_match('/^(.+?)([0-9]+)(.*)/', $address, $matches)) {
            $ret['streetname'] = trim($matches[1]);
            $ret['housenumber'] = trim($matches[2]);
            $ret['houseNumberAddition'] = trim($matches[3]);
        }

        // If the streetname is empty after splitting, and the address contains characters, then just use the address.
        if($ret['streetname'] == '' && strlen(trim($address)) > 0) {
            $ret['streetname'] = $address;
        }

        if ($attach_single_extension == true && strlen($ret['houseNumberAddition']) == 1) {
            $ret['housenumber'] = $ret['housenumber'] . $ret['houseNumberAddition'];
            $ret['houseNumberAddition'] = '';
        }

        if ($attach_whole_extension == true) {
            $ret['housenumber'] = $ret['housenumber'] . $ret['houseNumberAddition'];
            $ret['houseNumberAddition'] = '';
        }

        return $ret;
    }
}