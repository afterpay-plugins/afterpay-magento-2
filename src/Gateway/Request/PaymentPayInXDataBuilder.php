<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Helper\Service\Data;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Checkout\Model\Session;
use Magento\Payment\Gateway\Helper\SubjectReader;

class PaymentPayInXDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Session
     */
    private $session;

    /**
     * PaymentPayInXDataBuilder constructor.
     *
     * @param SubjectReader $subjectReader
     * @param Data $helper
     * @param Session $session
     */
    public function __construct(
        SubjectReader $subjectReader,
        Data $helper,
        Session $session
    ) {
        $this->subjectReader = $subjectReader;
        $this->helper = $helper;
        $this->session = $session;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $additionalInfo = $paymentDO->getPayment()->getAdditionalInformation();
        if($this->session->getAvailablePayinxDueAmount()) {
            $dueAmount = $this->session->getAvailablePayinxDueAmount();
        }
        else {
            $quote = $this->session->getQuote();
            $auth = $this->helper->getConfiguration('afterpay_nl_rest_payinx', $quote->getStoreId());
            $availablePaymentMethods = $this->helper->getAvailablePayments($auth, $quote);
            $availablePaymentMethodsArray = json_decode(json_encode($availablePaymentMethods), true);
            foreach ($availablePaymentMethodsArray['paymentMethods'] as $paymentMethod) {
                if ($paymentMethod['type'] == $this->getPaymentMethodType($methodInstance->getCode())) {
                    if ($paymentMethod['type'] == 'PayinX') {
                        $dueAmount = array_column($paymentMethod['payInX'], 'dueAmount')[0];
                    }
                }
            }
        }

        return [
            'payInX' => [
                'dueAmount' => $dueAmount,
            ],
        ];
    }

    /**
     * @param string $methodCode
     *
     * @return string
     */
    public function getPaymentMethodType($methodCode): string
    {
        if (str_contains($methodCode, 'installment')) {
			$payment_method_type = 'Installment';
		}
		elseif (str_contains($methodCode, 'flex')) {
			$payment_method_type = 'Account';
		}
        elseif (str_contains($methodCode, 'payinx')) {
            $payment_method_type = 'PayinX';
        }
		else {
			$payment_method_type = 'Invoice';
		}

		return $payment_method_type;
    }
}
