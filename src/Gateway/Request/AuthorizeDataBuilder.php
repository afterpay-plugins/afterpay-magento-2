<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Config\Vat;
use Afterpay\Payment\Model\Config\Vat as VatConfig;
use Afterpay\Payment\Model\Request\GiftWrapping;
use Magento\Bundle\Model\Product\Price;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Wishlist\Model\Product\AttributeValueProvider;
use Magento\Framework\UrlInterface;
use Magento\Framework\Math\Random;

class AuthorizeDataBuilder implements BuilderInterface
{
    const MERCHANT_URL = 'afterpay/payment/sca';
    const GROUP_ID_CODE = 'product_group_code';

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Vat
     */
    protected $vatConfig;

    /**
     * @var array
     */
    protected $result = [];

    /**
     * @var BackendCheckoutSession|CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * @var AttributeValueProvider
     */
    protected $attributeValueProvider;

    /**
     * @var string[]
     */
    protected $lineItemLangNormal;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * @var GiftWrapping
     */
    private $giftWrapping;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @param SubjectReader          $subjectReader
     * @param Vat                    $vatConfig
     * @param State                  $appState
     * @param Advanced               $advancedConfig
     * @param AttributeValueProvider $attributeValueProvider
     * @param Data                   $helper
     * @param QuoteRepository        $quoteRepository
     * @param GiftWrapping           $giftWrapping
     * @param UrlInterface           $urlBuilder
     * @param Random                 $mathRandom
     */
    public function __construct(
        SubjectReader $subjectReader,
        VatConfig $vatConfig,
        State $appState,
        Advanced $advancedConfig,
        AttributeValueProvider $attributeValueProvider,
        Data $helper,
        QuoteRepository $quoteRepository,
        GiftWrapping $giftWrapping,
        UrlInterface $urlBuilder,
        Random $mathRandom
    ) {
        $this->subjectReader = $subjectReader;
        $this->vatConfig = $vatConfig;
        $this->appState = $appState;
        $this->advancedConfig = $advancedConfig;
        $this->attributeValueProvider = $attributeValueProvider;
        $this->helper = $helper;
        $this->quoteRepository = $quoteRepository;
        $this->giftWrapping = $giftWrapping;
        $this->urlBuilder = $urlBuilder;
        $this->mathRandom = $mathRandom;
    }

    /**
     * @param array $buildSubject
     *
     * @return array
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();

        return $this->gatherAuthorizationData($payment);
    }

    /**
     * Prepare product lines
     *
     * @param Payment $payment
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function prepareProductLines(Payment $payment)
    {
        $order = $payment->getOrder();
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $attributeCode = $payment->getMethodInstance()->getConfigData(self::GROUP_ID_CODE);
        $quoteItems = $quote->getAllItems();
        /* @var Item $item */
        foreach ($quoteItems as $item) {
            if ($this->shouldBeSeparateLine($item)) {
                $vatAmount = $item->getTaxAmount();
                $unitPrice = $item->getRowTotalInclTax() * 100;
                if ($item->getDiscountAmount() > 0.00 && !$this->advancedConfig->discountCalculationMode()) {
                    $vatAmount = $item->getBaseRowTotal() * $item->getTaxPercent() / 100;
                }
                if ($this->advancedConfig->discountCalculationMode()) {
                    if (!$this->advancedConfig->getDiscountTaxMode($order->getStoreId())) {
                        $unitPrice =
                            ($item->getBaseRowTotal() - $item->getDiscountAmount() + $item->getTaxAmount()) * 100;
                    } else {
                        $unitPrice = ($item->getRowTotalInclTax() - $item->getDiscountAmount()) * 100;
                    }
                }
                $this->result['orderlines'][] = [
                    $item->getSku(),
                    $item->getQty() . ' x ' . $item->getName(),
                    '1',
                    (string) $unitPrice,
                    $this->vatConfig->getAfterpayVATCategory(
                        $order->getStoreId(),
                        $item->getTaxClassId(),
                        $this->vatConfig->isSoapRequest($payment->getMethod())
                    ),
                    $vatAmount,
                    null,
                    null,
                    $item->getProduct()->getProductUrl(),
                    $item->getAfterpayProductImage(),
                    $this->getGroupIdValue($attributeCode, $item)
                ];
                if ($item->getWeeeTaxApplied() && $item->getWeeeTaxAppliedAmount() > 0) {
                    if (!$weeTax = json_decode($item->getWeeeTaxApplied(), true)) {
                        continue;
                    }
                    $weeTax = $weeTax[0]; # object is an array

                    $this->result['orderlines'][] = [
                        $weeTax['title'] . ': ' . $item->getSku(),
                        'Weee tax for product; name: ' . $weeTax['title'],
                        '1',
                        $item->getWeeeTaxAppliedAmount() * 100,
                        4,
                        0
                    ];
                }
            }
        }
    }

    /**
     * Determine whether quote item should be converted into order line. If item is not configurable child product or
     * bundle parent product with price 0.00 or dynamic price type, it will be added to order.
     *
     * @param Item $item
     *
     * @return bool
     */
    private function shouldBeSeparateLine(Item $item): bool
    {
        if ($parent = $item->getParentItem()) {
            if ($parent->getProduct()->getTypeId() !== Type::TYPE_BUNDLE) {
                return false;
            }
        } else {
            if ($item->getProduct()->getTypeId() === Type::TYPE_BUNDLE) {
                if ((int) $item->getProduct()->getPriceType() === Price::PRICE_TYPE_DYNAMIC
                    || $item->getProduct()->getFinalPrice() === 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Prepare discount line
     *
     * @param Order $order
     */
    private function prepareDiscountLine(Order $order)
    {
        // Check if discount calculation is enabled per items,
        // if it's enabled we skip rest of the logic and return early
        if ($this->advancedConfig->discountCalculationMode()) {
            return;
        }
        $discountAmountItems = 0;
        $discountAmountCompensation = 0;
        $quoteItems = $order->getAllItems();
        foreach ($quoteItems as $item) {
            $discountAmountItems += $item->getDiscountAmount();
            $discountAmountCompensation += $item->getDiscountTaxCompensationAmount();
        }
        // Add shipping discount too
        $discountAmountItems += $order->getShippingDiscountAmount();
        $discountAmountCompensation += $order->getShippingDiscountTaxCompensationAmount();
        // discount is supposed to be negative
        $discountAmount = -1 * ($discountAmountItems * 100);
        // TODO: Implement a better way todo this
        if ($discountAmount !== 0 && $discountAmount !== -0.0) {
            $this->result['orderlines']['tempDiscount'] = [
                $this->lineItemLangNormal['discount']['key'],
                $this->lineItemLangNormal['discount']['name'],
                '1',
                (string) $discountAmount,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'discount',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                ),
                $discountAmountCompensation
            ];
        }
    }

    /**
     * Prepare shipping fee line
     *
     * @param OrderInterface $order
     */
    private function prepareShippingFeeLine(OrderInterface $order)
    {
        if ($this->advancedConfig->discountCalculationMode()) {
            $shippingFee = ($order->getShippingInclTax() - $order->getShippingDiscountAmount()) * 100;
        } else {
            $shippingFee = $order->getShippingInclTax() * 100;
        }
        if ($shippingFee !== 0) {
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['shipping']['key'],
                $this->lineItemLangNormal['shipping']['name'],
                '1',
                (string) $shippingFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'shipping',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                ),
                $order->getShippingTaxAmount()
            ];
        }
    }

    /**
     * Prepare payment fee line
     *
     * @param OrderInterface $order
     */
    private function preparePaymentFeeLine(OrderInterface $order)
    {
        $paymentFee = $order->getAfterpayPaymentFee() * 100;
        if ($paymentFee !== 0) {
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['fee']['key'],
                $this->lineItemLangNormal['fee']['name'],
                '1',
                (string) $paymentFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'fee',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                )
            ];
        }
    }

    /**
     * Prepare payment fee line for Fooman Surcharge
     *
     * @param Order $order
     */
    private function prepareFoomanTotalLines(Order $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }
        if (!method_exists($extensionAttributes, 'getFoomanTotalGroup')) {
            return;
        }
        $quoteAddressTotalGroup = $extensionAttributes->getFoomanTotalGroup();
        if (!$quoteAddressTotalGroup) {
            return;
        }
        $totals = $quoteAddressTotalGroup->getItems();
        if (empty($totals)) {
            return;
        }
        foreach ($totals as $total) {
            $paymentFee = ($total->getBaseAmount() + $total->getBaseTaxAmount()) * 100;
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['fee']['key'],
                $total->getLabel(),
                '1',
                (string) $paymentFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'fee',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                ),
                $total->getTaxAmount()
            ];
        }
    }

    /**
     * Prepare reward point line
     *
     * @param OrderInterface $order
     */
    private function prepareStoreCreditLine(OrderInterface $order)
    {
        $storeCredit = $order->getBaseCustomerBalAmountUsed();
        if ($storeCredit) {
            $storeCredit = $storeCredit * 100 * -1;
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['storecredit']['key'],
                $this->lineItemLangNormal['storecredit']['name'],
                '1',
                (string) $storeCredit,
                4
            ];
        }
    }

    /**
     * Prepare gift card line
     *
     * @param OrderInterface $order
     */
    private function prepareGiftCardLine(OrderInterface $order)
    {
        $giftCard = $order->getBaseGiftCardsAmount();
        if ($giftCard) {
            $giftCard = $giftCard * 100 * -1;
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['giftcard']['key'],
                $this->lineItemLangNormal['giftcard']['name'],
                '1',
                (string) $giftCard,
                4
            ];
        }
    }

    /**
     * Prepare reward point line
     *
     * @param OrderInterface $order
     */
    private function prepareRewardPointsLine(OrderInterface $order)
    {
        $rewardPoints = $order->getBaseRewardCurrencyAmount();
        if ($rewardPoints) {
            $rewardPoints = $rewardPoints * 100 * -1;
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['reward']['key'],
                $this->lineItemLangNormal['reward']['name'],
                '1',
                (string) $rewardPoints,
                4
            ];
        }
    }

    /**
     * @param Payment $payment
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function gatherAuthorizationData(Payment $payment): array
    {
        $orderDO = $payment->getOrder();
        $this->lineItemLangNormal = $this->helper->getLineItemLangNormalized($payment->getMethodInstance()->getCode());
        $this->prepareProductLines($payment);
        $this->prepareDiscountLine($orderDO);
        $this->preparePaymentFeeLine($orderDO);
        $this->prepareShippingFeeLine($orderDO);
        $this->prepareFoomanTotalLines($orderDO);
        $this->prepareGiftCardLine($orderDO);
        $this->prepareRewardPointsLine($orderDO);
        $this->prepareStoreCreditLine($orderDO);
        $this->prepareAmastyGiftWrappingLine($orderDO);
        
        if($this->advancedConfig->isHostedCheckoutEnabled()) { 
            $this->prepareHostedCheckoutData($orderDO);
        }

        return $this->result;
    }

    private function prepareAmastyGiftWrappingLine(OrderInterface $order)
    {
        $wrapping = $this->giftWrapping->getWrappingPrice((int) $order->getQuoteId());

        if ($wrapping) {
            $wrapping *= 100;
            $this->result['orderlines'][] = [
                'GIFTWRAP',
                'Gift Wrapping',
                '1',
                $wrapping,
                4,
                0
            ];
        }
    }

    /**
     * Prepare required info to send hosted checkout request
     *
     * @param Order $order
     */
    private function prepareHostedCheckoutData(Order $order)
    {
        $hash = $this->mathRandom->getUniqueHash();
        $order->setAfterpayScaHash($hash);
        $this->result['merchanturl'] = $this->urlBuilder->getUrl(self::MERCHANT_URL, ['token' => $hash]);
        $this->result['isHostedCheckout'] = 'true';
    }

    /**
     * @param $attributeCode
     * @param $item
     *
     * @return string|null
     */
    private function getGroupIdValue($attributeCode, $item)
    {
        if (!$attributeCode) {
            return null;
        }
        return $this->attributeValueProvider->getRawAttributeValue(
            (int) $item->getProduct()->getId(),
            $attributeCode,
            (int) $item->getStoreId()
        );
    }
}
