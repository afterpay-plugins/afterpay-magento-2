<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Config\Vat as VatConfig;
use Magento\Bundle\Model\Product\Price;
use Magento\Catalog\Model\Product\Type;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;

class OrderDataBuilder implements BuilderInterface
{
    /**
     * @var bool
     */
    protected $isPartialInvoice = false;

    /**
     * @var bool
     */
    protected $isSpecialCaseInvoice = false;

    /**
     * @var array
     */
    protected $result = [];

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var VatConfig
     */
    protected $vatConfig;

    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * @var string[]
     */
    protected $lineItemLangNormal;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param SubjectReader $subjectReader
     * @param VatConfig $vatConfig
     * @param Advanced $advancedConfig
     * @param Data $helper
     */
    public function __construct(
        SubjectReader $subjectReader,
        VatConfig $vatConfig,
        Advanced $advancedConfig,
        Data $helper
    ) {
        $this->subjectReader = $subjectReader;
        $this->vatConfig = $vatConfig;
        $this->advancedConfig = $advancedConfig;
        $this->helper = $helper;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $payment = $paymentDO->getPayment();

        return $this->gatherInvoiceData($payment);
    }

    /**
     * @param Payment $payment
     *
     * @return array
     */
    protected function gatherInvoiceData(Payment $payment): array
    {
        $orderDO = $payment->getOrder();
        $this->result['payment'] = $payment;
        $this->lineItemLangNormal = $this->helper->getLineItemLangNormalized($payment->getMethodInstance()->getCode());
        $this->prepareInvoiceData($orderDO);
        $this->prepareOrderLines($orderDO);
        $this->prepareDiscountLine($orderDO);
        $this->prepareShippingFeeLine($orderDO);
        $this->prepareFoomanTotalLines($orderDO);
        $this->prepareGiftCardLine($orderDO);

        return $this->result;
    }

    /**
     * Determine whether quote item should be converted into order line. If item is not configurable child product or
     * bundle parent product with price 0.00 or dynamic price type, it will be added to order.
     *
     * @param Item $item
     *
     * @return bool
     */
    private function shouldBeSeparateLine(Item $item): bool
    {
        if ($parent = $item->getParentItem()) {
            if ($parent->getProduct()->getTypeId() !== Type::TYPE_BUNDLE) {
                return false;
            }
        } else {
            if ($item->getProduct()->getTypeId() === Type::TYPE_BUNDLE) {
                if ((int) $item->getProduct()->getPriceType() === Price::PRICE_TYPE_DYNAMIC
                    || $item->getProduct()->getFinalPrice() === 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Prepare order item line
     *
     * @param Order $order
     */
    protected function prepareOrderLines(Order $order)
    {
        if (!$this->isPartialInvoice && !$this->isSpecialCaseInvoice) {
            return;
        }
        /**
         * @var Invoice $invoice
         */
        $invoice = $order->getInvoiceCollection()->getLastItem();
        foreach ($invoice->getItems() as $item) {
            /**
             * @var InvoiceItem $item
             */
            if ($item->getOrderItem()->getQtyInvoiced() > 0) {
                if ($this->shouldBeSeparateLine($item->getOrderItem())) {
                    $vatAmount = $item->getTaxAmount();
                    if ($item->getDiscountAmount() > 0.00 && !$this->advancedConfig->discountCalculationMode()) {
                        $vatAmount = $item->getOrderItem()->getBaseRowInvoiced() * $item->getOrderItem()
                                                                                        ->getTaxPercent() / 100;
                    }
                    if ($this->advancedConfig->discountCalculationMode()) {
                        $unitPrice = ($item->getRowTotalInclTax() - $item->getDiscountAmount()) * 100;
                    } else {
                        $unitPrice = $item->getRowTotalInclTax() * 100;
                    }
                    $this->result['orderlines'][] = [
                        $item->getSku(),
                        $item->getOrderItem()->getQtyInvoiced() . ' x ' . $item->getName(),
                        '1',
                        (string) $unitPrice,
                        $this->vatConfig->getAfterpayVATCategory(
                            $order->getStoreId(),
                            $item->getOrderItem()->getProduct()->getTaxClassId(),
                            $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                        ),
                        $vatAmount,
                        null,
                        null
                    ];
                }
            }
        }
    }

    /**
     * Prepare general invoice data
     *
     * @param Order $order
     */
    protected function prepareInvoiceData(Order $order)
    {
        /**
         * @var Invoice $invoice
         */
        $invoice = $order->getInvoiceCollection()->getLastItem();
        if (round((float) $order->getGrandTotal(), 2) === round((float) $invoice->getGrandTotal(), 2)) {
            $total = $order->getGrandTotal();
            $subtotal = ($order->getGrandTotal() - $order->getTaxAmount());
            if ($this->isSpecialRestMethod($order->getPayment()->getMethod())) {
                // For no real reason BE REST payment methods want to have item list.
                $this->isSpecialCaseInvoice = true;
            }
        } else {
            $this->isPartialInvoice = true;
            $total = $invoice->getGrandTotal();
            $subtotal = ($invoice->getGrandTotal() - $invoice->getTaxAmount());
        }

        $this->result['billtoaddress']['isocountrycode'] = $order->getBillingAddress()->getCountryId();
        $this->result['invoicenumber'] = $invoice->getIncrementId();
        $this->result['ordernumber'] = $order->getIncrementId();
        $this->result['totalamount'] = ($total * 100);
        $this->result['totalNetAmount'] = $subtotal;
        $this->result['partialinvoice'] = $this->isPartialInvoice;
        $this->result['specialCaseInvoice'] = $this->isSpecialCaseInvoice;
    }

    /**
     * Prepare discount line
     *
     * @param Order $order
     */
    private function prepareDiscountLine(Order $order)
    {
        if ((!$this->isPartialInvoice && !$this->isSpecialCaseInvoice)
            || $this->advancedConfig->discountCalculationMode()) {
            return;
        }
        /**
         * @var Invoice $invoice
         */
        $invoice = $order->getInvoiceCollection()->getLastItem();
        // discount is supposed to be negative
        $discountAmount = $invoice->getDiscountAmount() * 100;
        // TODO: Implement a better way todo this
        if ($discountAmount !== 0 && $discountAmount !== -0.0) {
            $this->result['orderlines']['tempDiscount'] = [
                $this->lineItemLangNormal['discount']['key'],
                $this->lineItemLangNormal['discount']['name'],
                '1',
                (string) $discountAmount,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'discount',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                ),
                $invoice->getDiscountTaxCompensationAmount()
            ];
        }
    }

    /**
     * Prepare shipping fee line
     *
     * @param OrderInterface $order
     */
    private function prepareShippingFeeLine(OrderInterface $order)
    {
        if (!$this->isPartialInvoice && !$this->isSpecialCaseInvoice) {
            return;
        }
        /**
         * @var Invoice $invoice
         */
        $invoice = $order->getInvoiceCollection()->getLastItem();
        $shippingFee = $invoice->getShippingInclTax() * 100;
        if ($order->getShippingDiscountAmount() > 0.00) {
            $shippingFee = ($order->getShippingInclTax() - $order->getShippingDiscountAmount()) * 100;
        }
        if ($shippingFee !== 0) {
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['shipping']['key'],
                $this->lineItemLangNormal['shipping']['name'],
                '1',
                (string) $shippingFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'shipping',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                ),
                $invoice->getShippingTaxAmount()
            ];
        }
    }

    /**
     * Prepare payment fee line for Fooman Surcharge
     *
     * @param Order $order
     */
    private function prepareFoomanTotalLines(Order $order)
    {
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $extensionAttributes = $invoice->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }
        if (!method_exists($extensionAttributes, 'getFoomanTotalGroup')) {
            return;
        }
        $quoteAddressTotalGroup = $extensionAttributes->getFoomanTotalGroup();
        if (!$quoteAddressTotalGroup) {
            return;
        }
        $totals = $quoteAddressTotalGroup->getItems();
        if (empty($totals)) {
            return;
        }
        foreach ($totals as $total) {
            $paymentFee = ($total->getBaseAmount() + $total->getBaseTaxAmount()) * 100;
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['fee']['key'],
                $total->getLabel(),
                '1',
                (string) $paymentFee,
                $this->vatConfig->getAfterpayVATCategory(
                    $order->getStoreId(),
                    'fee',
                    $this->vatConfig->isSoapRequest($order->getPayment()->getMethod())
                ),
                $total->getTaxAmount()
            ];
        }
    }

    /**
     * Prepare gift card line
     *
     * @param OrderInterface $order
     */
    private function prepareGiftCardLine(OrderInterface $order)
    {
        if (!$this->isPartialInvoice && !$this->isSpecialCaseInvoice) {
            return;
        }
        /**
         * @var Invoice $invoice
         */
        $invoice = $order->getInvoiceCollection()->getLastItem();
        $giftCard = $invoice->getBaseGiftCardsAmount();
        if ($giftCard) {
            $giftCard = $giftCard * 100 * -1;
            $this->result['orderlines'][] = [
                $this->lineItemLangNormal['giftcard']['key'],
                $this->lineItemLangNormal['giftcard']['name'],
                '1',
                (string) $giftCard,
                4
            ];
        }
    }

    /**
     * @param string $methodCode
     *
     * @return bool
     */
    private function isSpecialRestMethod(string $methodCode): bool
    {
        $allowedPaymentMethods = [
            Data::AFTERPAY_BE_REST_B2B,
            Data::AFTERPAY_BE_REST_DD,
            Data::AFTERPAY_BE_REST_DI,
            Data::AFTERPAY_NL_REST_B2B,
            Data::AFTERPAY_NL_REST_DD,
            Data::AFTERPAY_NL_REST_DI
        ];
        return in_array($methodCode, $allowedPaymentMethods, true);
    }
}
