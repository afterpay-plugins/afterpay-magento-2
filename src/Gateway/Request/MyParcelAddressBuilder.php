<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\Serialize\Serializer\Json;

class MyParcelAddressBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @param Manager $manager
     * @param SubjectReader $subjectReader
     * @param Json $json
     */
    public function __construct(
        Manager $manager,
        SubjectReader $subjectReader,
        Json $json
    ) {
        $this->manager = $manager;
        $this->subjectReader = $subjectReader;
        $this->json = $json;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $order = $paymentDO->getPayment()->getOrder();
        $data = [];

        if ($this->manager->isEnabled('MyParcelNL_Magento')) {
            $pickupAddress = $this->getMyParcelPickupAddress($order);
            if ($pickupAddress && $pickupAddress['deliveryType'] === 'pickup') {
                $data['shiptoaddress']['city'] = $pickupAddress['pickupLocation']['city'];
                $data['shiptoaddress']['streetname'] = $pickupAddress['pickupLocation']['street'];
                $data['shiptoaddress']['housenumber'] = $pickupAddress['pickupLocation']['number'];
                $data['shiptoaddress']['housenumberaddition'] = $pickupAddress['pickupLocation']['number_suffix'];
                $data['shiptoaddress']['postalcode'] = $pickupAddress['pickupLocation']['postal_code'];
                $data['shiptoaddress']['referenceperson']['firstname'] = 'A';
                $data['shiptoaddress']['referenceperson']['initials'] = 'A';
                $data['shiptoaddress']['referenceperson']['lastname'] = sprintf(
                    'Afhaalpunt %s',
                    $pickupAddress['pickupLocation']['location_name']
                );
            }
        }

        return $data;
    }

    /**
     * @param $order
     *
     * @return array|false
     */
    private function getMyParcelPickupAddress($order)
    {
        $deliveryInformation = $order->getData('myparcel_delivery_options');
        if (!$deliveryInformation) {
            return false;
        }
        return $this->json->unserialize($deliveryInformation);
    }
}
