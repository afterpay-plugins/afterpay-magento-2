<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Magento\Customer\Model\Session;
use Magento\Payment\Gateway\Helper\SubjectReader;

class PaymentFlexDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var DebugHelper
     */
    private $debugHelper;

    /**
     * PaymentDataBuilder constructor.
     *
     * @param DebugHelper $debugHelper
     * @param Session $session
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        DebugHelper $debugHelper,
        Session $session,
        SubjectReader $subjectReader
    ) {
        $this->debugHelper = $debugHelper;
        $this->session = $session;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $profileNo = $this->session->getAccountProfileNo();
        $result = [
            'account' => [
                'profileNo' => $profileNo
            ]
        ];
        $this->debugHelper->debug(
            $paymentDO->getPayment()->getMethodInstance()->getCode(),
            [
                'payment_flex_data' => $result
            ],
            true
        );

        return $result;
    }
}
