<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Bundle\Model\Product\Price;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Sales\Model\Order\Payment;
use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Model\Config\Advanced;
use Afterpay\Payment\Model\Config\Vat as VatConfig;
use Afterpay\Payment\Model\Config\Vat;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Backend\Model\Session\Quote as BackendCheckoutSession;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Wishlist\Model\Product\AttributeValueProvider;

class VoidDataBuilder implements BuilderInterface
{
    const GROUP_ID_CODE = 'product_group_code';

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @var Vat
     */
    protected $vatConfig;

    /**
     * @var array
     */
    protected $result = [];

    /**
     * @var BackendCheckoutSession|CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @var Advanced
     */
    protected $advancedConfig;

    /**
     * @var AttributeValueProvider
     */
    protected $attributeValueProvider;

    /**
     * @var string[]
     */
    protected $lineItemLangNormal;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * AuthorizeDataBuilder constructor.
     *
     * @param SubjectReader $subjectReader
     * @param Vat $vatConfig
     * @param BackendCheckoutSession $backendSession
     * @param State $appState
     * @param CheckoutSession $checkoutSession
     * @param Advanced $advancedConfig
     * @param AttributeValueProvider $attributeValueProvider
     * @param Data $helper
     *
     * @throws LocalizedException
     */
    public function __construct(
        SubjectReader $subjectReader,
        VatConfig $vatConfig,
        BackendCheckoutSession $backendSession,
        State $appState,
        CheckoutSession $checkoutSession,
        Advanced $advancedConfig,
        AttributeValueProvider $attributeValueProvider,
        Data $helper
    ) {
        $this->subjectReader = $subjectReader;
        $this->vatConfig = $vatConfig;
        $this->appState = $appState;
        # TODO checkoutSession must not be necessary here
        $this->checkoutSession =
            ($this->appState->getAreaCode() === Area::AREA_ADMINHTML) ? $backendSession : $checkoutSession;
        $this->advancedConfig = $advancedConfig;
        $this->attributeValueProvider = $attributeValueProvider;
        $this->helper = $helper;
    }

    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();
        return $this->gatherVoidData($payment);
    }

    /**
     * @param Payment $payment
     *
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function gatherVoidData(Payment $payment): array
    {
        $orderDO = $payment->getOrder();
        $this->lineItemLangNormal = $this->helper->getLineItemLangNormalized(
            $payment->getMethodInstance()->getCode()
        );
        $this->prepareVoidData($payment);
        return $this->result;
    }

    /**
     * Determine whether quote item should be converted into order line. If item is not configurable child product or
     * bundle parent product with price 0.00 or dynamic price type, it will be added to order.
     *
     * @param \Magento\Sales\Model\Order\Item $item
     *
     * @return bool
     */
    private function shouldBeSeparateLine(Item $item): bool
    {
        if ($parent = $item->getParentItem()) {
            if ($parent->getProduct()->getTypeId() !== Type::TYPE_BUNDLE) {
                return false;
            }
        } else {
            if ($item->getProduct()->getTypeId() === Type::TYPE_BUNDLE) {
                if ((int) $item->getProduct()->getPriceType() === Price::PRICE_TYPE_DYNAMIC
                    || $item->getProduct()->getFinalPrice() === 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param Payment $payment
     */
    protected function prepareVoidData(Payment $payment): void
    {
        $orderDO = $payment->getOrder();
        $this->result['ordernumber'] = $orderDO->getIncrementId();
        $this->result['order_country'] = $orderDO->getBillingAddress()->getCountryId();
        $this->result['payment'] = $payment;
    }
}
