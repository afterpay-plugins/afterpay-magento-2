<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Afterpay\Payment\Helper\Service\Data;
use Afterpay\Payment\Helper\Service\Data as Helper;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;

class TransactionSale extends AbstractTransaction
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * Debug
     *
     * @var DebugHelper
     */
    protected $debugHelper;

    /**
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param AfterpayFactory $afterpayFactory
     * @param DebugHelper $debugHelper
     *
     * @param Helper $helper
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $customLogger,
        AfterpayFactory $afterpayFactory,
        DebugHelper $debugHelper,
        Helper $helper
    ) {
        parent::__construct($logger, $customLogger, $afterpayFactory);
        $this->debugHelper = $debugHelper;
        $this->helper = $helper;
    }

    /**
     * @inheritdoc
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(array $data, array $clientConfig): \stdClass
    {
        $allowedB2bMethods = [
            Data::AFTERPAY_NL_B2B,
            Data::AFTERPAY_NL_REST_B2B,
            Data::AFTERPAY_DE_B2B,
            Data::AFTERPAY_AT_B2B,
            Data::AFTERPAY_BE_REST_B2B,
            Data::AFTERPAY_NO_B2B_OI,
            Data::AFTERPAY_SE_B2B_OI,
            Data::AFTERPAY_FI_B2B_OI,
            Data::AFTERPAY_DK_B2B_DI,
        ];
        $orderType = 'B2C';
        if (in_array($data['payment']->getMethod(), $allowedB2bMethods, true)) {
            $orderType = 'B2B';
        }
        // needs to be done before setting data on afterpay object
        if ($this->isRestOrder($clientConfig)) {
            $this->afterpay->setRest();
        }

        /**
         * This isn't the best way to handle this case because SOAP and REST clients are used and
         * both of them expect different discount (i.e. positive or negative amount).
         */
        if (array_key_exists('tempDiscount', $data['orderlines'])) {
            /** @var array $discountLine */
            $discountLine = $data['orderlines']['tempDiscount'];
            unset($data['orderlines']['tempDiscount']);
            if ($this->isRest()) {
                $discountLine[5] *= -1; //VAT amount
            }
            $data['orderlines'][] = $discountLine;
        }

        foreach ($data['orderlines'] as $line) {
            $this->afterpay->create_order_line(...array_values($line));
        }

        $this->afterpay->set_order($data, $orderType);
        $this->afterpay->do_request(
            $clientConfig,
            $clientConfig['modus'],
            $this->helper->getCurrentLocaleNormalized()
        );

        return $this->afterpay->order_result->return;
    }

    /**
     * @return bool
     */
    private function isRest(): bool
    {
        return $this->afterpay->useRest;
    }
}
