<?php
/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Controller\Adminhtml\Order;

use Afterpay\Payment\Model\ScaHandler;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;

/**
 * Class MassCancel
 */
class MassCancel extends AbstractMassAction
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ScaHandler
     */
    private $scaHandler;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param Order $order
     * @param OrderRepository $orderRepository
     * @param ScaHandler $scaHandler
     */
    public function __construct(
        Context                  $context,
        Filter                   $filter,
        CollectionFactory        $collectionFactory,
        OrderRepository          $orderRepository,
        ScaHandler               $scaHandler
    )
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderRepository = $orderRepository;
        $this->scaHandler = $scaHandler;
    }

    /**
     * Cancel selected orders
     * @param AbstractCollection $collection
     *
     * @return Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $countCanceledOrder = 0;
        $paymentReviewState = Order::STATE_PAYMENT_REVIEW;
        $scaFailedStatus = ScaHandler::ORDER_STATUS_SCA_FAILED_CODE;
        $hcpFailedStatus = ScaHandler::ORDER_STATUS_HCP_FAILED_CODE;

        foreach ($collection->getItems() as $order) {
            if (!$order->getEntityId()) {
                continue;
            }

            $loadedOrder = $this->orderRepository->get($order->getEntityId());

            if ($loadedOrder->getState() !== $paymentReviewState && $loadedOrder->getStatus() !== $scaFailedStatus 
                && $loadedOrder->getStatus() !== $hcpFailedStatus) {
                continue;
            }

            $loadedOrder->getPayment()->cancel();
            $loadedOrder->registerCancellation();
            $this->orderRepository->save($loadedOrder);
            $countCanceledOrder++;
        }
        $countNotCanceledOrder = $collection->count() - $countCanceledOrder;

        if ($countNotCanceledOrder && $countCanceledOrder)
        {
            $this->messageManager->addError(__('%1 Riverty failed order(s) were not canceled.',
                $countNotCanceledOrder));
        }

        if ($countNotCanceledOrder) {
            $this->messageManager->addError(__('No Riverty order(s) were canceled.'));
        }

        if ($countCanceledOrder)
        {
            $this->messageManager->addSuccess(__('You have canceled %1 Riverty failed order(s).',
                $countCanceledOrder));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}
