<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Setup;

use Afterpay\Payment\Model\ScaHandler;
use Exception;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Status;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var EavSetup $eavSetup
     */
    private $eavSetup;

    /**
     * @var StatusResourceFactory
     */
    private $statusResourceFactory;

    /**
     * @var StatusFactory
     */
    private $statusFactory;

    /**
     * @var Status
     */
    private $status;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param CustomerSetupFactory $customerSetupFactory
     * @param StatusResourceFactory $resourceStatusFactory
     * @param StatusFactory $statusFactory
     * @param Status $status
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        StatusResourceFactory $statusResourceFactory,
        StatusFactory $statusFactory,
        Status $status
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->statusResourceFactory = $statusResourceFactory;
        $this->statusFactory = $statusFactory;
        $this->status = $status;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     * @throws LocalizedException
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.7.1', '<')) {
            $this->addCocAttributeToQuote($setup);
        }

        if (version_compare($context->getVersion(), '3.2.0', '<')) {
            $this->addNewOrderStatuses();
        }

        if (version_compare($context->getVersion(), '3.4.8', '<')) {
            $this->removeGenderFields($setup);
        }

        if (version_compare($context->getVersion(), '3.5.0', '<')) {
            $this->editConfigFields($setup);
            $this->removeGenderFieldsBelgium($setup);
        }

        if (version_compare($context->getVersion(), '3.5.1', '<')) {
            $this->removeTitleAndDescriptionFields($setup);
        }

        if (version_compare($context->getVersion(), '3.5.2', '<')) {
            $this->removeAbandonedFieldsForDob($setup);
        }

        if (version_compare($context->getVersion(), '4.0.0', '<')) {
            $this->updateCaptureModeDefaultConfigValue($setup);
        }

        if (version_compare($context->getVersion(), '4.2.5', '<')) {
            $this->addPaymentMethodsDefaultTitles($setup);
        }

        if (version_compare($context->getVersion(), '4.2.9', '<')) {
            $this->updateConnectionModeConfigSetup($setup);
        }

        if (version_compare($context->getVersion(), '4.3.1', '<')) {
            $this->addHostedCheckoutOrderStatuses();
            $this->removeAfterpayCapturedOrderStatus();
        }

        $setup->endSetup();
    }


    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function editConfigFields(ModuleDataSetupInterface $setup)
    {
        $defaultConfigValues = [
            'payment/afterpay_be_rest_direct_debit/active' => '0'
        ];

        foreach ($defaultConfigValues as $path => $value) {
            $setup->getConnection()->update(
                $setup->getTable('core_config_data'),
                [
                    'value' => $value
                ],
                $setup->getConnection()->quoteInto('path = ?', $path)
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @throws LocalizedException
     */
    private function addCocAttributeToQuote(ModuleDataSetupInterface $setup)
    {
        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (!$this->eavSetup->getAttribute(Customer::ENTITY, 'cocnumber')) {
            $this->eavSetup->addAttribute(Customer::ENTITY, 'cocnumber', [
                'type' => 'static',
                'input' => 'text',
                'label' => 'CoC number',
                'required' => false,
                'system' => 0,
            ]);
        }

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $cocnumberAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'cocnumber');
        $cocnumberAttribute->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $cocnumberAttribute->save();
    }

    private function addNewOrderStatuses(): void
    {
        $statuses = [
            [
                'label' => ScaHandler::ORDER_STATUS_SCA_PENDING_LABEL,
                'code' => ScaHandler::ORDER_STATUS_SCA_PENDING_CODE
            ],
            [
                'label' => ScaHandler::ORDER_STATUS_SCA_FAILED_LABEL,
                'code' => ScaHandler::ORDER_STATUS_SCA_FAILED_CODE
            ]
        ];

        foreach ($statuses as $statusArray) {
            /** @var StatusResource $statusResource */
            $statusResource = $this->statusResourceFactory->create();
            $status = $this->statusFactory->create();
            $status->setData(
                [
                    'status' => $statusArray['code'],
                    'label' => $statusArray['label'],
                ]
            );
            try {
                $statusResource->save($status);
                $status->assignState(Order::STATE_PAYMENT_REVIEW, false, false);
            } catch (AlreadyExistsException | Exception $exception) {
                return;
            }
        }
    }

    /**
     * Remove config setting for gender field where it isn't necessary
     *
     * @param $setup
     */
    private function removeGenderFields($setup): void
    {
        $configItems = [
            'payment/afterpay_at_direct_debit/gender',
            'payment/afterpay_at_installment/gender',
            'payment/afterpay_at_open_invoice/gender',
            'payment/afterpay_de_b2b/gender',
            'payment/afterpay_de_direct_debit/gender',
            'payment/afterpay_de_installment/gender',
            'payment/afterpay_de_invoice/gender',
            'payment/afterpay_nl_rest_b2b/gender',
            'payment/afterpay_nl_rest_direct_debit/gender',
            'payment/afterpay_nl_rest_invoice_extra/gender',
            'payment/afterpay_nl_rest_invoice/gender',
            'payment/afterpay_ch_open_invoice/gender'
        ];

        $setup->getConnection()->delete(
            $setup->getTable('core_config_data'),
            "path IN ('" . implode("','", $configItems) . "')"
        );
    }

    /**
     * Remove config setting for gender field for Belgium REST methods
     *
     * @param $setup
     */
    private function removeGenderFieldsBelgium($setup): void
    {
        $configItems = [
            'payment/afterpay_be_rest_invoice/gender',
            'payment/afterpay_be_rest_direct_debit/gender',
            'payment/afterpay_be_rest_b2b/gender',
            'payment/afterpay_be_rest_invoice_extra/gender',
        ];

        $setup->getConnection()->delete(
            $setup->getTable('core_config_data'),
            "path IN ('" . implode("','", $configItems) . "')"
        );
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function removeTitleAndDescriptionFields(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->delete(
            $setup->getTable('core_config_data'),
            'path like "%payment/afterpay%" and ( path like "%/title%" or path like "%/description%" )'
        );
    }

    private function removeAbandonedFieldsForDob(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->delete(
            $setup->getTable('core_config_data'),
            'path like "%afterpay%dob" and value = 0'
        );
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function updateCaptureModeDefaultConfigValue(ModuleDataSetupInterface $setup)
    {
        $defaultConfigValues = [
            'payment/afterpay_capture/active' => '1'
        ];

        foreach ($defaultConfigValues as $path => $value) {
            $setup->getConnection()->update(
                $setup->getTable('core_config_data'),
                [
                    'value' => $value
                ],
                $setup->getConnection()->quoteInto('path = ?', $path)
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function addPaymentMethodsDefaultTitles(ModuleDataSetupInterface $setup)
    {
        $defaultConfigValues = [
            'payment/afterpay_at_b2b/title' => 'B2B 14-day invoice',
            'payment/afterpay_at_direct_debit/title' => 'Lastschrift',
            'payment/afterpay_at_installment/title' => 'Ratenzahlung',
            'payment/afterpay_at_open_invoice/title' => 'Rechnung',
            'payment/afterpay_be_digital_invoice_extra/title' => 'Achteraf betalen',
            'payment/afterpay_be_digital_invoice/title' => 'Achteraf betalen',
            'payment/afterpay_be_rest_b2b/title' => 'Veilig achteraf betalen voor bedrijven',
            'payment/afterpay_be_rest_direct_debit/title' => 'Veilig achteraf betalen (eenmalige machtiging)',
            'payment/afterpay_be_rest_invoice_extra/title' => 'Veilig achteraf betalen',
            'payment/afterpay_be_rest_invoice/title' => 'Veilig achteraf betalen',
            'payment/afterpay_dk_b2b_digital_invoice/title' => 'Faktura til virksomheder',
            'payment/afterpay_dk_campaign/title' => 'Campaign invoice',
            'payment/afterpay_dk_campaign2/title' => 'Campaign invoice',
            'payment/afterpay_dk_campaign3/title' => 'Campaign invoice',
            'payment/afterpay_dk_digital_invoice/title' => 'Faktura 14 dage',
            'payment/afterpay_dk_flex/title' => 'Riverty Flex - Betal i egen takt',
            'payment/afterpay_dk_installment/title' => 'Riverty Fix – Faste afdrag hver måned',
            'payment/afterpay_fi_b2b_open_invoice/title' => 'Yrityslasku',
            'payment/afterpay_fi_campaign/title' => 'Campaign invoice',
            'payment/afterpay_fi_campaign2/title' => 'Campaign invoice',
            'payment/afterpay_fi_campaign3/title' => 'Campaign invoice',
            'payment/afterpay_fi_flex/title' => 'Flex - maksa joustavasti osissa',
            'payment/afterpay_fi_installment/title' => 'Maksa tasaisissa kuukausierissä',
            'payment/afterpay_fi_open_invoice/title' => 'Maksa myöhemmin',
            'payment/afterpay_de_b2b/title' => 'B2B 14-day invoice',
            'payment/afterpay_de_direct_debit/title' => 'Lastschrift',
            'payment/afterpay_de_installment/title' => 'Ratenzahlung',
            'payment/afterpay_de_invoice/title' => 'Riverty Rechnung',
            'payment/afterpay_nl_business_2_business/title' => 'Achteraf betalen voor bedrijven',
            'payment/afterpay_nl_digital_invoice_extra/title' => 'Achteraf betalen',
            'payment/afterpay_nl_digital_invoice/title' => 'Achteraf betalen',
            'payment/afterpay_nl_direct_debit/title' => 'Automatische incasso',
            'payment/afterpay_nl_rest_b2b/title' => 'Veilig achteraf betalen voor bedrijven',
            'payment/afterpay_nl_rest_direct_debit/title' => 'Veilig achteraf betalen (eenmalige machtiging)',
            'payment/afterpay_nl_rest_invoice_extra/title' => 'Veilig achteraf betalen',
            'payment/afterpay_nl_rest_invoice/title' => 'Veilig achteraf betalen',
            'payment/afterpay_nl_rest_payinx/title' => 'Betaal in 3',
            'payment/afterpay_no_b2b_open_invoice/title' => 'Faktura for bedrifter',
            'payment/afterpay_no_campaign/title' => 'Campaign invoice',
            'payment/afterpay_no_campaign2/title' => 'Campaign invoice',
            'payment/afterpay_no_campaign3/title' => 'Campaign invoice',
            'payment/afterpay_no_flex/title' => 'Riverty Flex – Betal i egen takt',
            'payment/afterpay_no_installment/title' => 'Riverty Fix – Fast nedbetaling hver måned',
            'payment/afterpay_no_open_invoice/title' => 'Riverty – Kjøp nå, betal senere',
            'payment/afterpay_se_b2b_open_invoice/title' => 'Faktura för företag',
            'payment/afterpay_se_campaign/title' => 'Campaign invoice',
            'payment/afterpay_se_campaign2/title' => 'Campaign invoice',
            'payment/afterpay_se_campaign3/title' => 'Campaign invoice',
            'payment/afterpay_se_flex/title' => 'Dela upp din betalning',
            'payment/afterpay_se_installment/title' => 'Riverty Fix – Fast betalning varje månad',
            'payment/afterpay_se_open_invoice/title' => 'Faktura 14 dagar',
            'payment/afterpay_ch_open_invoice/title' => 'Rechnung',
        ];

        foreach ($defaultConfigValues as $path => $value) {
            $setup->getConnection()->insertOnDuplicate(
                $setup->getTable('core_config_data'),
                [
                    'path' => $path,
                    'value' => $value,
                ],
                ['value']
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function updateConnectionModeConfigSetup(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->update(
            $setup->getTable('core_config_data'),
            [
                'value' => 0
            ],
            'path like "%afterpay%testmode" and value = 2'
        );
    }

    private function addHostedCheckoutOrderStatuses(): void
    {
        $statuses = [
            [
                'label' => ScaHandler::ORDER_STATUS_HCP_PENDING_LABEL,
                'code' => ScaHandler::ORDER_STATUS_HCP_PENDING_CODE
            ],
            [
                'label' => ScaHandler::ORDER_STATUS_HCP_FAILED_LABEL,
                'code' => ScaHandler::ORDER_STATUS_HCP_FAILED_CODE
            ]
        ];

        foreach ($statuses as $statusArray) {
            /** @var StatusResource $statusResource */
            $statusResource = $this->statusResourceFactory->create();
            $status = $this->statusFactory->create();
            $status->setData(
                [
                    'status' => $statusArray['code'],
                    'label' => $statusArray['label'],
                ]
            );
            try {
                $statusResource->save($status);
                $status->assignState(Order::STATE_PAYMENT_REVIEW, false, false);
            } catch (AlreadyExistsException | Exception $exception) {
                return;
            }
        }
    }

    private function removeAfterpayCapturedOrderStatus(): void
    {
        $captureStatus = $this->status->load('afterpay_order_captured');
        if ($captureStatus->getId()) {
            $captureStatus->delete();
        }
    }
}
