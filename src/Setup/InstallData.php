<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var WriterInterface
     */
    protected $writer;

    /**
     * EAV setup factory
     *
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * Init
     *
     * @param EavSetup $eavSetup
     * @param WriterInterface $writer
     */
    public function __construct(EavSetup $eavSetup, WriterInterface $writer)
    {
        $this->eavSetup = $eavSetup;
        $this->writer = $writer;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) // @codingStandardsIgnoreLine
    {
        $setup->startSetup();

        $this->eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'cocnumber',
            [
                'type' => 'static',
                'input' => 'text',
                'label' => 'CoC number',
                'required' => false,
                'system' => 0,
            ]
        );

        $defaultConfigValues = [
            'payment/afterpay_capture/active' => '1',
            'payment/afterpay_refund/active' => '1'
        ];
        foreach ($defaultConfigValues as $path => $value) {
            $this->writer->save($path, $value, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        }

        $this->editPaymentMethodsOrder($setup);

        $this->editPhoneNumberFieldVisibility($setup);

        $this->addProfileTrackingDomainConfigValues($setup);

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function editPaymentMethodsOrder(ModuleDataSetupInterface $setup)
    {
        $defaultConfigValues = [
            'payment/afterpay_at_open_invoice/sort_order' => '0',
            'payment/afterpay_at_installment/sort_order' => '1',
            'payment/afterpay_at_direct_debit/sort_order' => '2',
            'payment/afterpay_at_b2b/sort_order' => '3',
            'payment/afterpay_de_invoice/sort_order' => '0',
            'payment/afterpay_de_installment/sort_order' => '1',
            'payment/afterpay_de_direct_debit/sort_order' => '2',
            'payment/afterpay_de_b2b/sort_order' => '3',
            'payment/afterpay_de_invoice_extra/sort_order' => '4',
            'payment/afterpay_dk_digital_invoice/sort_order' => '0',
            'payment/afterpay_dk_installment/sort_order' => '1',
            'payment/afterpay_dk_campaign/sort_order' => '2',
            'payment/afterpay_dk_campaign2/sort_order' => '3',
            'payment/afterpay_dk_campaign3/sort_order' => '4',
            'payment/afterpay_dk_flex/sort_order' => '5',
            'payment/afterpay_dk_b2b_digital_invoice/sort_order' => '6',
            'payment/afterpay_se_open_invoice/sort_order' => '0',
            'payment/afterpay_se_installment/sort_order' => '1',
            'payment/afterpay_se_campaign/sort_order' => '2',
            'payment/afterpay_se_campaign2/sort_order' => '3',
            'payment/afterpay_se_campaign3/sort_order' => '4',
            'payment/afterpay_se_flex/sort_order' => '5',
            'payment/afterpay_se_b2b_open_invoice/sort_order' => '6',
            'payment/afterpay_no_open_invoice/sort_order' => '0',
            'payment/afterpay_no_installment/sort_order' => '1',
            'payment/afterpay_no_campaign/sort_order' => '2',
            'payment/afterpay_no_campaign2/sort_order' => '3',
            'payment/afterpay_no_campaign3/sort_order' => '4',
            'payment/afterpay_no_flex/sort_order' => '5',
            'payment/afterpay_no_b2b_open_invoice/sort_order' => '6',
            'payment/afterpay_fi_open_invoice/sort_order' => '0',
            'payment/afterpay_fi_installment/sort_order' => '1',
            'payment/afterpay_fi_campaign/sort_order' => '2',
            'payment/afterpay_fi_campaign2/sort_order' => '3',
            'payment/afterpay_fi_campaign3/sort_order' => '4',
            'payment/afterpay_fi_flex/sort_order' => '5',
            'payment/afterpay_fi_b2b_open_invoice/sort_order' => '6',
        ];

        foreach ($defaultConfigValues as $path => $value) {
            $setup->getConnection()->update(
                $setup->getTable('core_config_data'),
                [
                    'value' => $value
                ],
                $setup->getConnection()->quoteInto('path = ?', $path)
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function editPhoneNumberFieldVisibility(ModuleDataSetupInterface $setup)
    {
        $countryCodes = [
            'payment/afterpay_nl',
            'payment/afterpay_be',
            'payment/afterpay_at',
            'payment/afterpay_de',
            'payment/afterpay_ch'
        ];

        foreach ($countryCodes as $country) {
            $value = $country == 'payment/afterpay_nl' || $country == 'payment/afterpay_be' ? '1' : '0';

            $setup->getConnection()->update(
                $setup->getTable('core_config_data'),
                [
                    'value' => $value
                ],
                $setup->getConnection()->quoteInto('path like "%/number%" and path like ?',"%".$country."%")
            );
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function addProfileTrackingDomainConfigValues(ModuleDataSetupInterface $setup)
    {
        $defaultConfigValues = [
            'payment/afterpay_at_b2b/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_at_direct_debit/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_at_installment/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_at_open_invoice/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_de_b2b/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_de_direct_debit/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_de_installment/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_de_invoice/tracking_domain' => 'whm.asip.cloud',
            'payment/afterpay_ch_open_invoice/tracking_domain' => 'whm.asip.cloud',
        ];

        foreach ($defaultConfigValues as $path => $value) {
            $setup->getConnection()->insertOnDuplicate(
                $setup->getTable('core_config_data'),
                [
                    'path' => $path,
                    'value' => $value,
                ],
                ['value']
            );
        }
    }
}
