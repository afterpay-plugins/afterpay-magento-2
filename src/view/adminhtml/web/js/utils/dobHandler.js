/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

define([
    'jquery',
    'mage/translate'
], function ($, $t) {
    'use strict';

    var afterPayDobFields = {
        // Function to update the days based on the current values of month and year
        updateNumberOfDays: function (paymentMethod) {
            var $days = $('#' + paymentMethod + '_dob_days'),
                month = $('#' + paymentMethod + '_dob_months').val(),
                year = $('#' + paymentMethod + '_dob_years').val(),
                days = this.daysInMonth(month, year);

            $days.append($('<option />').val('').html($t('Day')));

            for (var i = 1; i < days + 1; i++) {
                $days.append($('<option />').val(i).html(i));
            }
        },

        // Helper function
        daysInMonth: function (month, year) {
            return new Date(year, month, 0).getDate();
        },

        addDobFields: function (paymentMethod) {
            var $paymentMethodEl = $('#p_method_' + paymentMethod),
                self = this;
            if (!$('#dob').length) {
                if ($paymentMethodEl.length) {
                    var $dobMonths,
                        $dobYears;
                    $dobMonths = $('#' + paymentMethod + '_dob_months');
                    $dobYears = $('#' + paymentMethod + '_dob_years');

                    // Populate our months select box
                    $dobMonths.append($('<option />').val('').html($t('Month')));
                    for (var i = 1; i < 13; i++) {
                        $dobMonths.append($('<option />').val(i).html(i));
                    }

                    // Populate our years select box
                    $dobYears.append($('<option />').val('').html($t('Year')));
                    for (i = new Date().getFullYear(); i > 1900; i--) {
                        $dobYears.append($('<option />').val(i).html(i));
                    }

                    // Populate our Days select box
                    self.updateNumberOfDays(paymentMethod);

                    // Listen for change events
                    $('#' + paymentMethod + '_dob_years, #' + paymentMethod + '_dob_months').change(function () {
                        self.updateNumberOfDays(paymentMethod);
                        self.setDob(paymentMethod);
                    });

                    $('#' + paymentMethod + '_dob_days').change(function () {
                        self.setDob(paymentMethod);
                    });
                }
            }
        },
        setDob: function (paymentMethod) {
            var month = $('#' + paymentMethod + '_dob_months').val(),
                day = $('#' + paymentMethod + '_dob_days').val(),
                year = $('#' + paymentMethod + '_dob_years').val(),
                dob = $('input[name="payment[customer_dob]"]');

            if (month && day && year) {
                if (month < 10) {
                    month = '0' + month;
                }

                if (day < 10) {
                    day = '0' + day;
                }

                dob.val(month + '/' + day + '/' + year);
            } else {
                dob.val(null);
            }
        },
        updateDobFields: function (methodCode) {
            // DoB hidden value is for example as 12/28/1988 formatted
            var dobValue = $('#'+ methodCode +'_dob_hidden').val().split('/'),
                month = dobValue[0],
                day = dobValue[1],
                year = dobValue[2];
            if (day && month && year) {
                if (month < 10) {
                    month = month.replace(0, '')
                }
                if (day < 10) {
                    day = day.replace(0, '');
                }
                $('#' + methodCode + '_dob_months option[value="'+ month +'"]').prop('selected', true);
                $('#' + methodCode + '_dob_days option[value="'+ day +'"]').prop('selected', true);
                $('#' + methodCode + '_dob_years option[value="'+ year +'"]').prop('selected', true);
            }
        }
    };

    return function (config, element) {
        afterPayDobFields.addDobFields(config.methodCode);
        afterPayDobFields.updateDobFields(config.methodCode);
    };
});

