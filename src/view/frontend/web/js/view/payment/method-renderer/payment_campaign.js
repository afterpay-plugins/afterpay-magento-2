/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
 define(
    [
        'Afterpay_Payment/js/view/payment/method-renderer/payment_default',
        'ko',
        'Magento_Checkout/js/model/full-screen-loader',
        'jquery',
        'mage/url'
    ],
    function (
        Component,
        ko,
        loader,
        $,
        urlBuilder
    ) {
        'use strict';

        return Component.extend({
            afterpayConfig: '',
            defaults: {
                template: 'Afterpay_Payment/payment/campaign-payment',
            },

            initialize: function () {
                var self = this;
                self._super();
                self.config = {
                    ...self.config,
                    'campaign_url': 'rest/V1/afterpay/campaign/lookup'
                };
                self.campaignData = [];
                self.campaignInfo = ko.observableArray(self.campaignData);
            },

            getCampaignInfo: function () {
                return  this.campaignInfo()[0];
            },

            getCampaignDataFromServer: function () {
                loader.startLoader();
                var payload = {
                    paymentMethod: this.methodCode
                };

                $.ajax({
                    url: urlBuilder.build(this.config.campaign_url),
                    type: 'POST',
                    data: JSON.stringify(payload),
                    async: false,
                    contentType: 'application/json'
                }).done(
                    function (response) {
                        this.campaignData.push(response[0]);
                    }.bind(this)
                ).fail(
                    function (response) {
                    }
                ).always(
                    function () {
                        loader.stopLoader();
                    }
                );
            },

            getTitle: function () {
                return this.methodsData[0]['methodTitle'] ? this.methodsData[0]['methodTitle'] : this.getMethodDefaultTranslation('title');
            },
    
            getDescription: function () {
                return this.methodsData[0]['methodDescription'] ? this.methodsData[0]['methodDescription'] : this.getMethodDefaultTranslation('description');
            },

            getConsumerFeeAmount: function () {
                return this.getCampaignInfo().consumerFeeAmount;
            },

            getCampaignNumber: function () {
                return this.getCampaignInfo().campaignNumber;
            },

            getData: function () {
                var self = this,
                    result = self._super();

                if (self.getCampaignNumber()) {
                    if (result.additional_data) {
                        result.additional_data.campaign_id = self.getCampaignNumber();
                    } else {
                        result.additional_data = {
                            campaign_id: self.getCampaignNumber()
                        }
                    }
                }

                return result;
            },
        });
    }
);
