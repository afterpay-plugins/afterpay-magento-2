/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

define(
    [
        'Afterpay_Payment/js/view/payment/method-renderer/payment_default',
        'ko',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/storage',
        'jquery',
        'mage/url',
        'mage/translate',
    ],
    function (
        Component,
        ko,
        loader,
        storage,
        $,
        urlBuilder,
        $t
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Afterpay_Payment/payment/installment-payment',
            },

            initialize: function () {
                var self = this;
                self._super();
                self.config = {
                    ...self.config,
                    'url': 'rest/V1/afterpay/installments/lookup'
                };
                self.methodCode = 'afterpay_de_installment';
                self.methodName = 'fix_installments'
                self.afterpayConfig = self.getAfterpayConfig();
                self.installmentData = [];
                self.installmentDetails = '';
                self.infoTemplate = 'Afterpay_Payment/payment/installment-payment-info';
                self.installments = ko.observableArray(self.installmentData);
                self.selectionChange = self.onSelectionChange.bind(self);
                self.installment = ko.observableArray([]);
                self.selectFirst = self.doSelectFirst.bind(self);
                self.isSelected = self.doIsSelected.bind(self);
                self.termsLocale = self.afterpayConfig.terms_locale;
            },

            getInstallmentPlans: function () {
                loader.startLoader();
                var payload = {
                    paymentMethod: this.methodCode
                }, self = this;

                $.ajax({
                    url: urlBuilder.build(this.config.url),
                    type: 'POST',
                    data: JSON.stringify(payload),
                    async: false,
                    contentType: 'application/json'
                }).done(
                    function (response) {
                        response.sort(function (a,b) {
                            return a['installmentAmount'].replace(/\D/g,'') - b['installmentAmount'].replace(/\D/g,'');
                        });
                        $.each(response, function (index, option) {
                            if (index === 0) {
                                option.isThisFirst = true;
                                self.installment(option);
                            }

                            self.installmentData.push(option);
                        });
                    }.bind(this)
                ).fail(
                    function (response) {
                    }
                ).always(
                    function () {
                        loader.stopLoader();
                    }
                );
            },

            getInstallmentDetails: function () {
                var response = this.installmentData,
                    installmentMonths = response[0].installmentMonths,
                    lowestInstallmentAmount = response[0].installmentAmount;

                this.installmentDetails += this.getDescription().replace('%1', installmentMonths).replace('%2', lowestInstallmentAmount);

                return this.installmentDetails;
            },

            getInstallmentInfoTemplate: function () {
                return this.infoTemplate;
            },

            doSelectFirst(installment) {
                if (installment.isThisFirst === true) {
                    return true;
                }

                return false;
            },

            onSelectionChange: function (selection) {
                this.installment(selection);

                return true;
            },

            doIsSelected: function (installment) {
                return installment.value === this.installment().value;
            },

            getIsNordicCountry: function () {
                console.log(this.afterpayConfig);
                let methodCode = this.afterpayConfig.code;
                let norway_installment_code = "afterpay_no_installment";
                let sweden_istallment_code = "afterpay_se_installment";
                let finland_installment_code = "afterpay_fi_installment";
                if (methodCode == norway_installment_code) {
                    return true;
                }
                if (methodCode == sweden_istallment_code) {
                    return true;
                }
                if (methodCode == finland_installment_code){
                    return true;
                }
                return false;
            },

            getTermsLocale: function () {
                return this.afterpayConfig.terms_locale;
            },

            getRestMerchantId: function () {
                return this.afterpayConfig.rest_merchant_id;
            }
        });
    }
);
