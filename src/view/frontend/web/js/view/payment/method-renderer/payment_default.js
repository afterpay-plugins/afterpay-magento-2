/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

 define(['ko', 'Magento_Checkout/js/view/payment/default', 'jquery', 'Magento_Checkout/js/checkout-data', 'mage/translate', 'Magento_Checkout/js/model/quote', 'uiLayout', 'mage/url', 'Magento_Checkout/js/model/full-screen-loader', 'inputmask', 'iban'], function (ko, Component, $, checkoutData, $t, quote, layout, urlBuilder, loader, inputmask, iban) {
    'use strict';

    return Component.extend({
        afterpayConfig: '', defaults: {
            template: 'Afterpay_Payment/payment/payment',
            customerGender: 2, // Female,
            dobFull: '',
            termsAndConditions: false,
            telNumber: '',
            cocNumber: '',
            companyName: '',
            legalForm: '',
            bankaccountNumber: '',
            ssn: '',
            methodCode: '',
            redirectAfterPlaceOrder: false,
            termsLinkBase: 'https://documents.riverty.com/terms_conditions/payment_methods/',
            privacyLinkBase: 'https://documents.riverty.com/privacy_statement/checkout/',
            termsLocale: 'en_us',
            trackingPixelUserConsent: false,
            requiresCustomerConsent: false,
        },

        initialize: function () {
            var self = this;
            self._super();
            self.afterpayConfig = self.getAfterpayConfig();
            self.customerGender = ko.observable(this.getCustomerGender());
            self.dobFull = ko.observable(this.getFullDob());
            self.termsAndConditions = ko.observable();
            self.telNumber = ko.observable(this.getTelNumber());
            self.cocNumber = ko.observable(this.getCocNumber());
            self.companyName = ko.observable(this.getCompanyName());
            self.legalForm = ko.observable(this.getLegalForm());
            self.bankaccountNumber = ko.observable(this.getBankaccountNumber());
            self.ssn = ko.observable(this.getSsn());
            self.installment = ko.observable();
            self.defaultGender = 2;
            self.config = {
                'flex_url': 'rest/V1/afterpay/flex/lookup',
                'redirect_url': 'afterpay/payment/redirect',
                'extractMethodsInfo': 'rest/V1/afterpay/availability',
                'bankaccount_verify': 'rest/V1/afterpay/bankaccount/verify'
            };
            self.flexData = [];
            self.flexInfo = ko.observableArray(self.flexData);
            self.methodName = '';
            self.methodTranslation = '';
            self.trackingInputId = '';
            self.availableMethods = {
                'invoice': 'invoice',
                'debit': 'direct_debit',
                'installment': 'fix_installments',
                'campaign': 'campaign_invoice',
                'flex': 'part_payment',
                'b2b': 'b2b',
                'payinx': 'payinx'
            };
            self.availableMethodsDefaultTranslation = {
                'invoice_title': $t('Invoice - 14 days'),
                'invoice_description': $t('Buy now, Pay in 14 days'),
                'debit_title': $t('Direct debit'),
                'debit_description': $t('Have the amount conveniently collected from your account'),
                'installment_title': $t('Installments'),
                'installment_description': $t('Divide in %1 months from %2'),
                'campaign_title': $t('campaign invoice title'),
                'campaign_description': $t('campaign invoice description'),
                'flex_title': $t('Afterpay Account').replace('Afterpay', ''),
                'flex_description': $t('Buy now, pay in your own pace'),
                'b2b_title': $t('Invoice - 14 days for businesses'),
                'b2b_description': $t('Buy now, Pay later'),
                'payinx_title': $t('Pay in 3'),
                'payinx_description': $t('Pay in 3 instalments without interest')
            };
            self.trackingPixelUserConsent = ko.observable(false);
            self.methodsData = [];
            self.methodsInfo = ko.observableArray(self.methodsData);
            self.invoiceMethodCode = '_invoice';
            self.installmentMethodCode = '_installment';
            self.directDebitMethodCode = '_direct_debit';
            self.payInXMethodCode = '_payinx';
        },

        getCode: function () {
            return this.methodCode;
        },

        isActive: function () {
            return true;
        },

        isAvailable: function () {
            return true;
        },

        getTitle: function () {
            if (typeof this.methodsData[0] !== 'undefined') {
                if (this.methodCode.includes('_b2b')) {
                    return (_.size(this.methodsData[0]) > 0 && this.methodsData[0]['methodTitle']) ? this.methodsData[0]['methodTitle'] + ' ' + $t('for businesses') : this.getMethodDefaultTranslation('title');
                }
                else {
                    return (_.size(this.methodsData[0]) > 0 && this.methodsData[0]['methodTitle']) ? this.methodsData[0]['methodTitle'] : this.getMethodDefaultTranslation('title');
                }
            }
            return this.getMethodDefaultTranslation('title'); 
        },

        getDescription: function () {
            if (typeof this.methodsData[0] !== 'undefined') {
                return (_.size(this.methodsData[0]) > 0 && this.methodsData[0]['methodDescription']) ? this.methodsData[0]['methodDescription'] : this.getMethodDefaultTranslation('description');
            }
            return this.getMethodDefaultTranslation('description');
        },

        getCodeOfConduct: function () {
            return this.methodsData[0]['methodCodeOfConduct'] ? this.methodsData[0]['methodCodeOfConduct'] : $t('You must be at least 18+ to use this service. If you pay on time, you will avoid additional costs and ensure that you can use Riverty services again in the future. By continuing, you accept the Terms and Conditions and confirm that you have read the Privacy Statement and Cookie Statement.');
        },

        getMethodDefaultTranslation: function ($type) {
            for (let key in this.availableMethods) {
                if (this.methodCode.indexOf(key) !== -1) {
                    this.methodTranslation = this.availableMethodsDefaultTranslation[key + '_' + $type];
                }
            }

            if (this.methodCode.indexOf('business_2_business') !== -1) {
                this.methodTranslation = this.availableMethodsDefaultTranslation['b2b_' + $type];
            }

            if (this.methodCode.indexOf('campaign2') !== -1 || this.methodCode.indexOf('campaign3') !== -1) {
                this.methodTranslation = this.availableMethodsDefaultTranslation['campaign_' + $type];
            }

            return this.methodTranslation;
        },

        isPlaceOrderActionAllowedAfterpay: function () {
            return this.validateCountries();
        },

        getCountryCode: function () {
            var billingAddress = false;
            if (checkoutData.getSelectedBillingAddress() === null) {
                if (checkoutData.getShippingAddressFromData() === null && window.checkoutConfig.customerData.hasOwnProperty('addresses')) {
                    // 'Use same address' has been checked and user hasn't explicitly selected any shipping method,
                    // falling back to default shipping address.
                    $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                        if (address.default_billing) {
                            billingAddress = address;
                        }
                    }.bind(this));
                } else {
                    // 'Use same address' has been checked and user has entered shipping info
                    billingAddress = checkoutData.getShippingAddressFromData();
                }
            } else if (checkoutData.getSelectedBillingAddress() === 'new-customer-address') {
                // New billing address entered
                billingAddress = checkoutData.getNewCustomerBillingAddress();
            } else if (checkoutData.getSelectedBillingAddress() === 'new-customer-billing-address') {
                // New billing address entered
                billingAddress = checkoutData.getNewCustomerBillingAddress();
            } else {
                // Predefined address selected as custom billing address
                var selectedAddressId = parseInt(checkoutData.getSelectedBillingAddress().substring('customer-address'.length));
                $.each(window.checkoutConfig.customerData.addresses, function (index, address) {
                    if (parseInt(address.id) === selectedAddressId) {
                        billingAddress = address;
                    }
                }.bind(this));
            }

            return billingAddress;
        },

        validateCountries: function () {
            let billingAddress = this.getCountryCode();
            return this.validateCountryCode(billingAddress);
        },

        validateCountryCode: function (address) {
            if (this.afterpayConfig && address) {
                if (this.afterpayConfig.allowspecific === "0") {
                    return true
                }

                var allowedCodes = this.afterpayConfig.allowed_countries.split(',');
                return (allowedCodes.indexOf(address.country_id) >= 0);
            }
            return false;
        },

        getAfterpayConfig: function () {
            if (window.checkoutConfig.payment) {
                var afterpayConfig = window.checkoutConfig.payment[this.methodCode];
                if (afterpayConfig) {
                    return afterpayConfig;
                }
            }
            return null;
        },

        getTestmodeLabel: function () {
            if (this.isTestmode()) {
                return this.afterpayConfig.testmode_label;
            }
        },

        getPaymentIcon: function () {
            if (typeof this.methodsData[0] !== 'undefined') {
                return (_.size(this.methodsData[0]) > 0 && this.methodsData[0]['methodLogo']) ? this.methodsData[0]['methodLogo'] : this.afterpayConfig.payment_icon;
            }
            return this.afterpayConfig.payment_icon;
        },

        isTestmode: function () {
            // Force it to return boolean
            return !!parseInt(this.afterpayConfig.testmode);
        },

        getCustomerGender: function () {
            if (window.customerData && window.customerData.gender) {
                return window.customerData.gender;
            }
            return this.defaultGender;

        },

        getTelNumber: function () {
            return quote.billingAddress() ? quote.billingAddress().telephone : null;
        },

        getFullDob: function () {
            return window.customerData.dob;
        },

        getGenderValues: function () {
            return [{'optionText': $t('Female'), optionValue: 2}, {'optionText': $t('Male'), optionValue: 1}]
        },

        getLegalFormValues: function () {
            var legalFormValues = [];
            if (this.methodCode.includes('at')) {
                legalFormValues = this.getAustriaLegalForms();
            }
            else {
                legalFormValues = this.getGermanyLegalForms();
            }
            return legalFormValues;
        },

        getAustriaLegalForms: function () {
            return [
                {'optionText': 'AG (Aktiengesellschaft)', optionValue: 'AG (Aktiengesellschaft)(required)'},
                {'optionText': 'AG & Co. KG', optionValue: 'AG & Co. KG(required)'},
                {'optionText': 'eG (eingetragene Genossenschaft)', optionValue: 'eG (eingetragene Genossenschaft)(optional)'},
                {'optionText': 'eingetragener Einzelunternehmer', optionValue: 'eingetragener Einzelunternehmer(required)'},
                {'optionText': 'EK (eingetragener Kaufmann)', optionValue: 'EK (eingetragener Kaufmann)(optional)'},
                {'optionText': 'e.V. (eingetragener Verein)', optionValue: 'e.V. (eingetragener Verein)(optional)'},
                {'optionText': 'Einzelfirma', optionValue: 'Einzelfirma(optional)'},
                {'optionText': 'Europäische wirtschaftliche Interessenvereinigung', optionValue: 'Europäische wirtschaftliche Interessenvereinigung(optional)'},
                {'optionText': 'Freiberufler/Kleingewerbetreibender/Handelsvertreter', optionValue: 'Freiberufler/Kleingewerbetreibender/Handelsvertreter(required)'},
                {'optionText': 'Stiftung', optionValue: 'Stiftung(optional)'},
                {'optionText': 'GbR/BGB (Gesellschaft bürgerlichen Rechts)', optionValue: 'GbR/BGB (Gesellschaft bürgerlichen Rechts)(optional)'},
                {'optionText': 'GmbH (Gesellschaft mit beschränkter Haftung)', optionValue: 'GmbH (Gesellschaft mit beschränkter Haftung)(required)'},
                {'optionText': 'GmbH & Co. KG', optionValue: 'GmbH & Co. KG(required)'},
                {'optionText': 'KG (Kommanditgesellschaft)', optionValue: 'KG (Kommanditgesellschaft)(required)'},
                {'optionText': 'Limited', optionValue: 'Limited(required)'},
                {'optionText': 'Limited & Co. KG', optionValue: 'Limited & Co. KG(optional)'},
                {'optionText': 'sonstige Personengesellschaftt', optionValue: 'sonstige Personengesellschaftt(optional)'},
                {'optionText': 'sonstige Kapitalgesellschaft', optionValue: 'sonstige Kapitalgesellschaft(optional)'},
                {'optionText': 'offene Gesellschaft', optionValue: 'offene Gesellschaft(optional)'},
                {'optionText': 'OHG (offene Handelsgesellschaft)', optionValue: 'OHG (offene Handelsgesellschaft)(required)'},
                {'optionText': 'öffentliche Einrichtung', optionValue: 'öffentliche Einrichtung(optional)'},
                {'optionText': 'UG (Unternehmensgesellschaft haftungsbeschränkt)', optionValue: 'UG (Unternehmensgesellschaft haftungsbeschränkt)(required)'},
            ]
        },

        getGermanyLegalForms: function () {
            return [
                {'optionText': 'AG (Aktiengesellschaft)', optionValue: 'AG (Aktiengesellschaft)(required)'},
                {'optionText': 'AG & Co. KG', optionValue: 'AG & Co. KG(required)'},
                {'optionText': 'eG (eingetragene Genossenschaft)', optionValue: 'eG (eingetragene Genossenschaft)(optional)'},
                {'optionText': 'eingetragener Einzelunternehmer', optionValue: 'eingetragener Einzelunternehmer(required)'},
                {'optionText': 'EK (eingetragener Kaufmann)', optionValue: 'EK (eingetragener Kaufmann)(optional)'},
                {'optionText': 'e.V. (eingetragener Verein)', optionValue: 'e.V. (eingetragener Verein)(optional)'},
                {'optionText': 'Einzelfirma', optionValue: 'Einzelfirma(optional)'},
                {'optionText': 'Europäische wirtschaftliche Interessenvereinigung', optionValue: 'Europäische wirtschaftliche Interessenvereinigung(optional)'},
                {'optionText': 'Freiberufler/Kleingewerbetreibender/Handelsvertreter', optionValue: 'Freiberufler/Kleingewerbetreibender/Handelsvertreter(optional)'},
                {'optionText': 'Stiftung', optionValue: 'Stiftung(required)'},
                {'optionText': 'GbR/BGB (Gesellschaft bürgerlichen Rechts)', optionValue: 'GbR/BGB (Gesellschaft bürgerlichen Rechts)(optional)'},
                {'optionText': 'GmbH (Gesellschaft mit beschränkter Haftung)', optionValue: 'GmbH (Gesellschaft mit beschränkter Haftung)(required)'},
                {'optionText': 'GmbH & Co. KG', optionValue: 'GmbH & Co. KG(required)'},
                {'optionText': 'KG (Kommanditgesellschaft)', optionValue: 'KG (Kommanditgesellschaft)(required)'},
                {'optionText': 'Limited', optionValue: 'Limited(required)'},
                {'optionText': 'Limited & Co. KG', optionValue: 'Limited & Co. KG(optional)'},
                {'optionText': 'sonstige Personengesellschaftt', optionValue: 'sonstige Personengesellschaftt(optional)'},
                {'optionText': 'sonstige Kapitalgesellschaft', optionValue: 'sonstige Kapitalgesellschaft(optional)'},
                {'optionText': 'offene Gesellschaft', optionValue: 'offene Gesellschaft(optional)'},
                {'optionText': 'OHG (offene Handelsgesellschaft)', optionValue: 'OHG (offene Handelsgesellschaft)(optional)'},
                {'optionText': 'öffentliche Einrichtung', optionValue: 'öffentliche Einrichtung(optional)'},
                {'optionText': 'UG (Unternehmensgesellschaft haftungsbeschränkt)', optionValue: 'UG (Unternehmensgesellschaft haftungsbeschränkt)(required)'},
            ]
        },

        getData: function () {
            var self = this, result = {
                'method': self.item.method, 'po_number': null, 'additional_data': null
            };

            // Add customer gender
            if (self.canShowGender && self.customerGender()) {
                result.additional_data = {
                    customer_gender: self.customerGender()
                };
            } else if (!self.canShowGender() && self.afterpayConfig.gender_fallback) {
                // Load value by name and select firt one
                var genderValue = $("[name='" + self.afterpayConfig.gender_fallback + "']:first");
                result.additional_data = {
                    customer_gender: genderValue.val()
                };
            }

            // Add customer dob
            if (self.canShowDob() && self.getDob()) {
                if (result.additional_data) {
                    result.additional_data.customer_dob = self.getDob();
                } else {
                    result.additional_data = {
                        customer_dob: self.getDob()
                    }
                }
            }

            // Add terms and conditions
            if (self.canShowConditions()) {
                if (result.additional_data) {
                    result.additional_data.terms_and_conditions = self.termsAndConditions();
                } else {
                    result.additional_data = {
                        terms_and_conditions: self.termsAndConditions()
                    }
                }
            }

            // Add tel number
            if (self.canShowNumber() && self.telNumber()) {
                if (result.additional_data) {
                    result.additional_data.customer_telephone = self.telNumber();
                } else {
                    result.additional_data = {
                        customer_telephone: self.telNumber()
                    }
                }
            } else if (!self.canShowNumber() && self.afterpayConfig.phone_fallback) {
                // Load value by name and select firt one
                var phoneValue = $("[name='" + self.afterpayConfig.phone_fallback + "']:first");
                if (result.additional_data) {
                    result.additional_data.customer_telephone = phoneValue.val();
                } else {
                    result.additional_data = {
                        customer_telephone: phoneValue.val()
                    }
                }
            }

            // Add social security number
            if (self.canShowSsn()) {
                if (result.additional_data) {
                    result.additional_data.ssn = self.ssn();
                } else {
                    result.additional_data = {
                        ssn: self.ssn()
                    }
                }
            }

            // Add coc number
            if (self.cocNumber()) {
                if (result.additional_data) {
                    result.additional_data.coc_number = self.cocNumber();
                } else {
                    result.additional_data = {
                        coc_number: self.cocNumber()
                    }
                }
            }

            // Add company name
            if (self.companyName()) {
                if (result.additional_data) {
                    result.additional_data.company_name = self.companyName();
                } else {
                    result.additional_data = {
                        company_name: self.companyName()
                    }
                }
            }

            // Add company legal form
            if (self.legalForm()) {
                if (result.additional_data) {
                    result.additional_data.legal_form = self.legalForm();
                } else {
                    result.additional_data = {
                        legal_form: self.legalForm()
                    }
                }
                if (result.additional_data.legal_form.includes('(required)')) {
                    result.additional_data.legal_form = result.additional_data.legal_form.replace('(required)', '');
                }
                else {
                    result.additional_data.legal_form = result.additional_data.legal_form.replace('(optional)', '');
                }
            }

            //Add bank account number
            if (self.bankaccountNumber()) {
                if (result.additional_data) {
                    result.additional_data.bankaccountnumber = self.bankaccountNumber()
                } else {
                    result.additional_data = {
                        bankaccountnumber: self.bankaccountNumber()
                    }
                }
            }

            //Add installment ID
            if (self.installment()) {
                if (result.additional_data) {
                    result.additional_data.installment = self.installment().value;
                } else {
                    result.additional_data = {
                        installment: self.installment().value
                    }
                }
            }

            if(self.isTrackingActive()) {
                $('input[type=radio][id=' + this.methodCode + ']').on('click', function(e) {
					var tracking_script = document.getElementById('afterpay_tracking_pixel');
                	var tracking_noscript = document.getElementById('afterpay_tracking_noscript');

					if (tracking_script) {
						tracking_script.remove();
						tracking_noscript.remove();
					}
					self.getTrackingPixel();
				});	
				if($('input[type=radio][id=' + this.methodCode + ']').is(':checked')) { 
					var tracking_script = document.getElementById('afterpay_tracking_pixel');
                	var tracking_noscript = document.getElementById('afterpay_tracking_noscript');

					if (tracking_script) {
						tracking_script.remove();
						tracking_noscript.remove();
					}
					self.getTrackingPixel();
				}
                if (result.additional_data) {
                    result.additional_data.tracking_id = this.afterpayConfig.unique_id;
                } else {
                    result.additional_data = {
                        tracking_id: this.afterpayConfig.unique_id
                    }
                }
            }

            if (this.canShowLegalForm()) {
                var legalFormInput = document.getElementById(this.methodCode + '_legal_form');
                var cocNumberInput = document.getElementById(this.methodCode + '_coc_number');
                var dateOfBirthFieldSet = document.getElementById(this.methodCode + '_dob_fieldset');
                var cocNumberFieldSet = document.getElementById(this.methodCode + '_coc_number_fieldset');
                var cocNumberDiv = document.getElementById(this.methodCode + '_coc_number_div');
                if(cocNumberDiv != null && cocNumberInput != null) {
                    if ( this.methodCode.includes('at') && legalFormInput.value == 'Freiberufler/Kleingewerbetreibender/Handelsvertreter(required)') {
                        cocNumberFieldSet.style.display = 'none';
                        dateOfBirthFieldSet.style.display = 'block';
                        if (result.additional_data) {
                            result.additional_data.coc_number = null;
                            result.additional_data.customer_dob = self.getDob();
                        } else {
                            result.additional_data = {
                                coc_number: null,
                                customer_dob: self.getDob()
                            }
                        }
                    }
                    else {
                        dateOfBirthFieldSet.style.display = 'none';
                        cocNumberFieldSet.style.display = 'block';
                        if (result.additional_data) {
                            result.additional_data.coc_number = self.cocNumber();
                            result.additional_data.customer_dob = null;
                        } else {
                            result.additional_data = {
                                coc_number: self.cocNumber(),
                                customer_dob: null
                            }
                        }
                        if (legalFormInput.value.includes('(required)')) {
                            cocNumberDiv.classList.add('required', 'required-input-entry');
                            cocNumberInput.classList.add('required-entry');
                            cocNumberInput.setAttribute('data-validate', '{required:true}');
                        }
                        else {
                            cocNumberDiv.classList.remove('required', 'required-input-entry');
                            cocNumberInput.classList.remove('required-entry');
                            cocNumberInput.setAttribute('data-validate', '{required:false}');
                        }
                    }
                }
            }

            return result;
        },

        getDobMonths: function () {
            var result = [{'optionText': $t('Month'), 'optionValue': ''}];

            // Populate our months select box
            for (var i = 1; i < 13; i++) {
                result.push({'optionText': $t(i), 'optionValue': i});
            }

            return result;
        },

        getDobYears: function () {
            var result = [{'optionText': $t('Year'), 'optionValue': ''}];

            // Populate our years select box
            for (var i = this.firstValidDobYear(); i > 1900; i--) {
                result.push({'optionText': $t(i), 'optionValue': i});
            }

            return result;
        },

        /**
         * Do not show last 18 years in dropdown as person needs to be at least 18 to use Afterpay
         *
         * @returns {number}
         */
        firstValidDobYear: function () {
            return new Date().getFullYear() - 18;
        },

        // Dob helper
        daysInMonth: function (month, year) {
            return new Date(year, month, 0).getDate();
        },

        getDob: function () {
            var self = this, fullDate = self.dobFull();

            return fullDate;
        },


        getCocNumber: function () {
            try {
                return window.customerData.custom_attributes.cocnumber.value;
            } catch (e) {
                return null;
            }
        },

        getBankaccountNumber: function () {
            return quote.billingAddress() ? quote.billingAddress().bankaccountNumber : null;
        },

        getCompanyName: function () {
            return quote.billingAddress() ? quote.billingAddress().company : null;
        },

        getLegalForm: function () {
            return quote.billingAddress() ? quote.billingAddress().legalForm : null;
        },

        getSsn: function () {
            return quote.billingAddress() ? quote.billingAddress().ssn : null;
        },

        getMethodName: function () {
            for (let key in this.availableMethods) {
                if (this.methodCode.indexOf(key) !== -1) {
                    this.methodName = this.availableMethods[key];
                }
            }

            if (this.methodCode.indexOf('business_2_business') !== -1 || this.methodCode.indexOf('b2b') !== -1) {
                this.methodName = 'b2b_invoice';
            }

            return this.methodName;
        },

        getPostfix: function () {
            var currentLocale = this.termsLocale.split('_')[0];
            if (this.allocateCountryFromLocale(this.termsLocale) === 'be') {
                var countryCode = (currentLocale == 'nl') ? 'be_nl' : ((currentLocale == 'fr') ? 'be_fr' : 'be_en');
            }
            else if (this.allocateCountryFromLocale(this.termsLocale) === 'de') {
                var countryCode = ( currentLocale == 'de' ) ? 'de_de' : 'de_en';
                return countryCode + '/' + (this.afterpayConfig.rest_merchant_id || 'default');
            }
            else if (this.allocateCountryFromLocale(this.termsLocale) === 'at') {
                var countryCode = ( currentLocale == 'de' ) ? 'at_de' : 'at_en';
                return countryCode + '/' + (this.afterpayConfig.rest_merchant_id || 'default');
            }
            else {
                var countryCode = (currentLocale == 'nl') ? 'nl_nl' : 'nl_en';
                if (this.methodCode.includes('_b2b')) {
                    return countryCode + '/' + (this.afterpayConfig.rest_merchant_id || 'default');
                }
            }

            return (this.getMethodName() || '') + '/' + countryCode + '/' + 'default';
        },

        getPrivacyStatementPostFix: function () {
            var currentLocale = this.termsLocale.split('_')[0];
            if (this.allocateCountryFromLocale(this.termsLocale) === 'be') {
                var countryCode = ( currentLocale == 'nl' ) ? 'be_nl' : ( ( currentLocale == 'fr' ) ? 'be_fr' : 'be_en' );
            }
            else if (this.allocateCountryFromLocale(this.termsLocale) === 'de') {
                var countryCode = ( currentLocale == 'de' ) ? 'de_de' : 'de_en';
            }
            else if (this.allocateCountryFromLocale(this.termsLocale) === 'at') {
                var countryCode = ( currentLocale == 'de' ) ? 'at_de' : 'at_en';
            }
            else {
                var countryCode = ( currentLocale == 'nl' ) ? 'nl_nl' : 'nl_en';
            }

            return countryCode;
        },

        getConditionsLink: function () {
            var b2bMethodAllowedCountries = ['de', 'at', 'nl'];
            if (b2bMethodAllowedCountries.includes(this.methodCode.split('_')[1]) && this.methodCode.includes('_b2b')) {
                return 'https://documents.riverty.com/terms_conditions/payment_methods/b2b_invoice/' + this.getPostfix();
            }
            return this.methodsData[0]['methodConditionsLink'] ? this.methodsData[0]['methodConditionsLink'] : this.termsLinkBase + this.getPostfix();
        },

        getPrivacystatementLink: function () {
            var b2bMethodAllowedCountries = ['de', 'at', 'nl'];
            if (b2bMethodAllowedCountries.includes(this.methodCode.split('_')[1]) && this.methodCode.includes('_b2b')) {
                return 'https://documents.riverty.com/privacy_statement/b2b_checkout/' + this.getPrivacyStatementPostFix();
            }
            return this.methodsData[0]['methodPrivacyLink'] ? this.methodsData[0]['methodPrivacyLink'] : this.privacyLinkBase + this.getPrivacyStatementPostFix();
        },

        getTrackingConditionsLink: function () {
            var currentLocale = this.termsLocale.split('_')[0];
            var paymentMehtodCountry = this.methodCode.split('_')[1];
            var countryCode = ( currentLocale == 'de' ) ? paymentMehtodCountry + '_de' : paymentMehtodCountry + '_en';
            return this.termsLinkBase + (this.getMethodName() || '') + '/' + countryCode + '/' + (this.afterpayConfig.rest_merchant_id || 'default');
        },

        getTrackingPrivacyLink: function () {
            var currentLocale = this.termsLocale.split('_')[0];
            var paymentMehtodCountry = this.methodCode.split('_')[1];
            var countryCode = ( currentLocale == 'de' ) ? paymentMehtodCountry + '_de' : paymentMehtodCountry + '_en';
            if (this.methodCode.includes('_b2b')) {
                return 'https://documents.riverty.com/privacy_statement/b2b_checkout/' + countryCode;;
            }
            return this.privacyLinkBase + countryCode;;
        },

        getConditionsClass: function () {
            if (this.canShowLegalInfoCheckBox()) {
                return 'field required';
            }

            return 'field';
        },

        getPayInXConditionsText: function () {
            var conditionsText = this.methodsData[0]['methodConditonsText'] != null ? this.methodsData[0]['methodConditonsText'] : '';
            return conditionsText;
        },

        updatePayInXConditionsLinks: function () {
            $('.riverty_payinx_conditions a').attr('href', 'javascript:void(0)');
            $('.riverty_payinx_conditions a:nth-of-type(1)').attr('data-trigger', 'termstrigger_' + this.getCode());
            $('.riverty_payinx_conditions a:nth-of-type(2)').attr('data-trigger', 'privacytrigger_' + this.getCode());
            return '';
        },

        getSsnNoticeLink: function () {
        },

        canShowLegalInfoCheckBox: function () {
            return this.methodsData[0]['methodRequiresCustomerConsent'] != null ? this.methodsData[0]['methodRequiresCustomerConsent'] : this.requiresCustomerConsent;
        },

        canShowSsnNotice: function () {
            return false;
        },

        canShowConditionsInThisLocation: function () {
            if (this.allocateCountryFromLocale(this.termsLocale) === 'de' || this.allocateCountryFromLocale(this.termsLocale) === 'at' || this.allocateCountryFromLocale(this.termsLocale) === 'ch') {
                return false;
            } else {
                return true;
            }
        },

        isGermanSpeaking: function () {
            if (this.allocateCountryFromLocale(this.termsLocale) === 'de' || this.allocateCountryFromLocale(this.termsLocale) === 'at' || this.allocateCountryFromLocale(this.termsLocale) === 'ch') {
                return true;
            } else {
                return false;
            }
        },

        isScandinavianSpeaking: function () {
            if (this.allocateCountryFromLocale(this.termsLocale) === 'no' ||
                this.allocateCountryFromLocale(this.termsLocale) === 'se' ||
                this.allocateCountryFromLocale(this.termsLocale) === 'fi') {
                return true;
            }

            return false
        },

        isDanishSpeaking: function () {
            if (this.allocateCountryFromLocale(this.termsLocale) === 'dk') {
                return true;
            }

            return false;
        },

        isSoapPaymentMethod: function () {
            if (this.methodCode.includes('rest') == false) {
                return true;
            }

            return false;
        },

        canShowConditions: function () {
            return this.afterpayConfig.terms_and_conditions != 0;
        },

        canShowCodeOfConduct: function () {
            if (this.methodCode.includes('nl')) {
                return true;
            }
            return false
        },

        canShowCoc: function () {
            return this.afterpayConfig.can_coc != 0;
        },

        canShowBankaccount: function () {
            return this.afterpayConfig.can_bankaccount != 0;
        },

        canShowDob: function () {
            if (this.methodCode.includes('_b2b') || this.methodCode.includes('business')) {
                return false;
            }
            return this.afterpayConfig.can_dob == true;
        },

        canShowSsn: function () {
            return this.afterpayConfig.can_ssn != 0;
        },

        canShowCompanyName: function () {
            return this.afterpayConfig.can_company_name != 0;
        },

        canShowLegalForm: function () {
            if (this.methodCode.includes('_b2b')) {
                return this.afterpayConfig.can_legal_form == true;
            }
            return false;
        },

        canShowPrivacy: function () {
            return this.afterpayConfig.can_privacy === "1";
        },

        canShowGender: function () {
            return this.afterpayConfig.can_gender === "1";
        },

        canShowNumber: function () {
            return this.afterpayConfig.can_number === "1";
        },

        getTrackingPixel: function () {
            if (document.getElementById('afterpay_tracking_pixel')) {
                return;
            }
            ;
            var domain = this.afterpayConfig.tracking_domain ? this.afterpayConfig.tracking_domain : 'uc8.tv';
            window._itt = {
                c: this.afterpayConfig.tracking_id, s: this.afterpayConfig.unique_id, t: 'CO'
            };
            this.loadProfileTrackingNoScript(domain);
            this.loadProfileTrackingScript(domain);
        },

        loadProfileTrackingNoScript: function (domain) {
            var noscriptElement = document.createElement('noscript');
            noscriptElement.id = 'afterpay_tracking_noscript';
            var imageElement = document.createElement('img');
            imageElement.src = '//' + domain + '/img/7982/' + this.getTrackingSessionId();
            imageElement.border = '0';
            imageElement.height = '0';
            imageElement.width = '0';

            noscriptElement.appendChild(imageElement);
            var scriptElementTag = document.getElementsByTagName('script')[0];
            scriptElementTag.parentNode.insertBefore(noscriptElement, scriptElementTag);
        },

        loadProfileTrackingScript: function (domain) {
            var scriptElement = document.createElement('script');
            scriptElement.type = 'text/javascript';
            scriptElement.async = true;
            scriptElement.src = '//' + domain + '/7982.js';
            scriptElement.id = 'afterpay_tracking_pixel';

            var noscriptElementTag = document.getElementById('afterpay_tracking_noscript');
            noscriptElementTag.parentNode.insertBefore(scriptElement, noscriptElementTag);
        },

        getTrackingClass: function () {
            return 'field required';
        },

        getTrackingInputId: function () {
            if (this.trackingInputId.length === 0) {
                var result = '';
                var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;
                for (var i = 0; i < 5; i++) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }
                this.trackingInputId = 'afterpay_tracking_' + result;
            }
            return this.trackingInputId;
        },

        isTrackingRequired: function () {
            return '{required:true}';
        },

        isTrackingActive: function () {
            var profileTrackingAllowedCountries = ['de', 'at', 'ch'];
            if(profileTrackingAllowedCountries.includes(this.methodCode.split('_')[1])) {
                return true;
            }
            return false;
        },

        getTrackingSessionId: function () {
            return this.afterpayConfig.unique_id;
        },

        getTrackingValidationMessage: function () {
            var trackingValidationMessage = $t('To proceed with payment using Riverty, please accept the Data Privacy conditions.');
            return trackingValidationMessage;
        },

        createMessagesComponent: function () {
            var messagesComponent = {
                parent: this.name,
                name: this.name + '.messages',
                displayArea: 'messages',
                component: 'Afterpay_Payment/js/view/messages',
                config: {
                    messageContainer: this.messageContainer, messageTimeout: window.checkoutConfig.messageTimeout
                }
            };
            layout([messagesComponent]);

            return this;
        },

        canShowFlexInfo: function () {
            return this.afterpayConfig.can_show_flex;
        },

        getFlexInfo: function () {
            return this.flexInfo()[0];
        },

        getFlexDataFromServer: function () {
            loader.startLoader();
            var payload = {
                paymentMethod: this.methodCode
            };

            $.ajax({
                url: urlBuilder.build(this.config.flex_url),
                type: 'POST',
                data: JSON.stringify(payload),
                async: false,
                contentType: 'application/json'
            }).done(function (response) {
                this.flexData.push(response[0]);
            }.bind(this)).fail(function (response) {
            }).always(function () {
                loader.stopLoader();
            });
        },

        afterPlaceOrder: function () {
            window.location.replace(urlBuilder.build(this.config.redirect_url))
        },

        initializeDob: function () {
            // Adding date mask to dob field in checkout page
            $('input.dob').mask('00/00/0000', {placeholder: $t('DD/MM/YYYY')});
        },

        triggerValidationProcess: function () {
            let self = this, verificationBar = $('.afterpay-bankaccount-verification-bar');

            if (self.bankaccountNumber() || self.bankaccountNumber() == '') {
                if (self.bankaccountNumber() == '') {
                    verificationBar.html('');
                    if (verificationBar.hasClass('success')) {
                        verificationBar.removeClass('success');
                    }
                }
                $.validator.validateSingleElement($('.payment-method._active').find('.afterpay-bank-account'), {errorElement: 'div'});
            }
        },

        getConditionsTemplate: function () {
            return 'Afterpay_Payment/payment/fields/conditions';
        },

        getIntroductionBlock: function () {
            return 'Afterpay_Payment/payment/fields/introduction';
        },

        canShowIntroductionBlock: function () {
            if (this.methodCode.includes(this.invoiceMethodCode) || this.methodCode.includes(this.installmentMethodCode) || this.methodCode.includes(this.directDebitMethodCode)) {
                return true;
            }

            return false;
        },

        canShowIntroductionText: function (method) {
            if (this.methodCode.includes(this.invoiceMethodCode) && method === '14-days') {
                return true;
            }
            if (this.methodCode.includes(this.installmentMethodCode) && method === 'installment') {
                return true;
            }
            if (this.methodCode.includes(this.directDebitMethodCode) && method === 'direct_debit') {
                return true;
            }
        },

        canShowPaymentMethodFields: function () {
            return this.afterpayConfig.can_show_fields;
        },

        getCurrentLanguageCode: function () {
            var languageCode;
            var currentLocale = this.methodsData[0]['currentLocale'];
            switch (currentLocale) {
                case 'nl_NL':
                case 'nl_BE':
                    languageCode = 'nl';
                    break;
                case 'sv_SE':
                    languageCode = 'sv-SE';
                    break;
                case 'fi_FI':
                    languageCode = 'fi';
                    break;
                case 'da_DK':
                    languageCode = 'da';
                    break;
                case 'de_DE':
                    languageCode = 'de';
                    break;
                default:
                    languageCode = 'en';
                    break;
            }

            return languageCode;
        },

        getCartAmount: function () {
            return this.methodsData[0]['cartAmount'];
        },

        isPayInXPaymentMethod: function () {
            if (this.methodCode.includes(this.payInXMethodCode)) {
                return true;
            }

            return false;
        },

        getPayInXElement: function () {
            if (typeof this.methodsData[0] !== 'undefined') {
                return (_.size(this.methodsData[0]) > 0 && this.methodsData[0]['currentLocale'] && this.methodsData[0]['cartAmount']) ? '<riverty-split language="' + this.getCurrentLanguageCode() + '" theme="checkout" amount="' + this.getCartAmount() + '" split-in-parts="3" show-offer="false"></riverty-split>' : '';
            }
            return '';
        },

        getIntroInstallmentPrice: function () {
            return this.installments()[0]['installmentAmount'];
        },

        getIntroNumberOfInstallments: function () {
            return this.installments()[0]['numberOfInstallments'];
        },

        getFieldsDynamicValidation: function () {
            return 'Afterpay_Payment/payment/fields/fields-dynamic-validation';
        },

        canShowDynamicFields: function () {
            let phoneNumber = this.canShowNumber(), dateOfBirth = this.canShowDob(),
                bankAccountNumber = this.canShowBankaccount(), ssn = this.canShowSsn(),
                companyName = this.canShowCompanyName(), legalForm = this.canShowLegalForm(),
                companyNumber = this.canShowCoc(),
                fieldsList = [phoneNumber, dateOfBirth, bankAccountNumber, ssn, companyName, legalForm, companyNumber],
                canShow = false;

            $.each(fieldsList, function (index, value) {
                if (value === true) {
                    canShow = true;
                    return false;
                }
            });

            return canShow;
        },

        showDynamicFields: function () {
            let phoneNumber = this.canShowNumber(), dateOfBirth = this.canShowDob(),
                bankAccountNumber = this.canShowBankaccount(), ssn = this.canShowSsn(),
                companyName = this.canShowCompanyName(), legalForm = this.canShowLegalForm(),
                companyNumber = this.canShowCoc(),
                fieldsList = [companyName, phoneNumber, bankAccountNumber, legalForm, companyNumber, ssn, dateOfBirth,],
                fieldsValue = [$t('company name'), $t('phone number'), $t('bank account number'), $t('company legal form'), $t('company identification number'), $t('social security number'), $t('date of birth')],
                allowedFields = [];

            $.each(fieldsList, function (index, value) {
                if (value === true) {
                    allowedFields.push(fieldsValue[index] + ', ');
                }
            });

            // adding 'and' before the latest field, if number of fields is more than 1
            if (allowedFields.length >= 2) {
                allowedFields[allowedFields.length - 2] = allowedFields[allowedFields.length - 2].replace(', ', $t(' and '));
            }

            // replacing comma with a dot at the end of the latest field
            if (!this.isDutchSpecificText()) {
                allowedFields[allowedFields.length - 1] = allowedFields[allowedFields.length - 1].replace(', ', '.');
            } else {
                // replaces comma for the last item with the space as Dutch language has some words defined after that
                allowedFields[allowedFields.length - 1] = allowedFields[allowedFields.length - 1].replace(', ', ' ');
            }

            return allowedFields;
        },

        isDutchSpecificText: function () {
            if (this.allocateCountryFromLocale(this.termsLocale) === 'nl') {
                return true
            }

            return false;
        },

        isGermanOrDutch: function () {
            if (this.allocateCountryFromLocale(this.termsLocale) === 'nl' || this.allocateCountryFromLocale(this.termsLocale) === 'de' || this.allocateCountryFromLocale(this.termsLocale) === 'at') {
                return true;
            }

            return false;
        },

        getTermsConditionsText: function () {
            $('#termstrigger_' + this.getCode()).load(this.getConditionsLink() + ' .container');
        },

        getMethodsFields: function () {
            loader.startLoader();
            let payload = {
                    paymentMethod: this.methodCode
                },
                self = this;

            $.ajax({
                url: urlBuilder.build(this.config.extractMethodsInfo),
                type: 'POST',
                data: JSON.stringify(payload),
                async: false,
                contentType: 'application/json'
            }).done(
                function (response) {
                    let methodData = {
                        'methodTitle': response[0],
                        'methodDescription': response[1],
                        'methodLogo': response[2],
                        'methodConditionsLink' : response[3],
                        'methodPrivacyLink' : response[4],
                        'methodRequiresCustomerConsent' : response[5],
                        'methodConditonsText' : response[6],
                        'cartAmount' : response[7],
                        'currentLocale' : response[8]
                    }
                    self.afterpayConfig.unique_id = response[9];
                    if(response.length == 11) {
                        methodData['methodCodeOfConduct'] = response[10];
                    } 
                    self.methodsData.push(methodData);
                }
            ).fail(
                function (response) {
                }
            ).always(
                function () {
                    loader.stopLoader();
                }
            );
        },

        getBankAccountTemplate: function () {
            return 'Afterpay_Payment/payment/fields/bank-account';
        },

        initializeBAN: function () {
            let masks = ['AT00 0000 0000 0000 0000', 'BE00 0000 0000 0000', 'DK00 0000 0000 0000 00', 'FI00 0000 0000 0000 00', 'DE00 0000 0000 0000 0000 00', 'NL00 SSSS 0000 0000 00', 'NO00 0000 0000 000', 'SE00 0000 0000 0000 0000 0000', 'CH00 0000 0000 0000 0000 0'],
                country = this.getCountryCode() ? this.getCountryCode().country_id : '',
                maskIndex = 4; // DE

            $.each(masks, function (index, value) {
                if (value.indexOf(country) > -1) {
                    maskIndex = index;
                    return false;
                }
            });

            $('input.afterpay-bank-account').mask(masks[maskIndex], {placeholder: masks[maskIndex]});
        },

        initializeBANValidation: function (element) {
            var self = this, verificationBar = $('.afterpay-bankaccount-verification-bar');

            if (verificationBar.hasClass('success')) {
                verificationBar.removeClass('success');
            } else if (verificationBar.hasClass('failed')) {
                verificationBar.removeClass('failed');
            }

            verificationBar.addClass('loader');

            if (self.bankaccountNumber()) {
                if (iban.isValid(this.bankaccountNumber())) {
                    setTimeout(self.sendExternalBanValidationRequest(), 500);
                } else {
                    verificationBar.addClass('failed');
                    verificationBar.text($t('Something went wrong!'));
                }
            }

            verificationBar.removeClass('loader');
        },

        sendExternalBanValidationRequest: function (payload) {
            var payload = {
                paymentMethodCode: this.methodCode, bankNumber: this.bankaccountNumber()
            }, verificationBar = $('.afterpay-bankaccount-verification-bar'), self = this;

            $.ajax({
                url: urlBuilder.build(this.config.bankaccount_verify),
                type: 'POST',
                data: JSON.stringify(payload),
                async: false,
                contentType: 'application/json'
            }).done(function (response) {
                if (response[0] == true) {
                    verificationBar.addClass('success');
                } else {
                    verificationBar.addClass('failed');
                }

                verificationBar.text(response[1]);
            }).fail(function (response) {
            }).always(function () {
                verificationBar.removeClass('loader');
            });
        },

        getPrivacyPolicyText: function () {
            $('#privacytrigger_' + this.getCode()).load(this.getPrivacystatementLink() + ' .container');
        },

        allocateCountryFromLocale: function (locale) {
            let country = locale.split('_')[1] ? locale.split('_')[1] : locale;

            return country;
        }
    });
});
