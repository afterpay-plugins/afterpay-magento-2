/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(['jquery', 'iban', 'mage/url', 'mage/translate'], function ($, iban, urlBuilder, $t) {
    'use strict';

    let errorMessages = [$t('The entered bank details are invalid, please check and correct.')];
    let errorKey = 0;

    return function () {
        $.validator.addMethod('iban-validation', function (value, element) {
            let errorKey = 0, noError = false, self = this,
                verificationBar = $('.afterpay-bankaccount-verification-bar');

            verificationBar.addClass('loader');

            if (value) {
                if (iban.isValid(value)) {
                    if (!verificationBar.hasClass('success')) {
                        verificationBar.html('');
                        sendExternalBanValidationRequest(value, element.id);
                    }
                    verificationBar.removeClass('loader');
                    noError = true;
                } else {
                    if (verificationBar.hasClass('success')) {
                        verificationBar.html('');
                        verificationBar.removeClass('success');
                    }
                    verificationBar.removeClass('loader');
                }
            }

            return noError;
        }, function () {
            return $.mage.__(errorMessages[errorKey]);
        })
    }

    function sendExternalBanValidationRequest(bankNumber, methodCode) {
        var payload = {
            paymentMethodCode: methodCode, bankNumber: bankNumber
        }, verificationBar = $('.afterpay-bankaccount-verification-bar'), self = this;

        $.ajax({
            url: urlBuilder.build('rest/V1/afterpay/bankaccount/verify'),
            type: 'POST',
            data: JSON.stringify(payload),
            async: false,
            contentType: 'application/json'
        }).done(function (response) {
            if (response[0] == true) {
                if (verificationBar.hasClass('failed')) {
                    verificationBar.removeClass('failed');
                }

                verificationBar.addClass('success');
            } else {
                if (verificationBar.hasClass('success')) {
                    verificationBar.removeClass('success');
                }

                verificationBar.addClass('failed');
            }

            verificationBar.removeClass('loader');
            verificationBar.html($t(response[1]));
            if (verificationBar.hasClass('success')) {
                verificationBar.prepend('<span class="afterpay-success-iban"></span>');
            }
        }).fail(function (response) {
        }).always(function () {
            verificationBar.removeClass('loader');
        });
    }
});
