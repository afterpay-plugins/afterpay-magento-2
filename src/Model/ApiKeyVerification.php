<?php
/**
 * Copyright (c) 2023  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2023 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Api\ApiKeyVerificationInterface;
use Afterpay\Payment\Helper\Service\Data;

class ApiKeyVerification implements ApiKeyVerificationInterface
{
    /**
     * @var string
     */
    private $paymentMethodCode;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @param AfterpayFactory $afterpay
     * @param Data $helper
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        AfterpayFactory $afterpay,
        Data $helper
    ) {
        $this->afterpay = $afterpay->create();
        $this->helper = $helper;
    }

    /**
     * @param $paymentMethodCode
     * @param $apiKey
     * @param $connectionType
     * 
     * @return array
     */
    public function verify($paymentMethodCode, $apiKey, $connectionType)
    {
        $this->setPaymentMethodCode($paymentMethodCode);
        $country = strtoupper(explode('_', $this->getPaymentMethodCode())[1]);
        $currency = $this->getPaymentMethodCurrency($country);
        $requestData = [
            'conversationLanguage' => 'EN',
            'country' => $country,
            'order' => [
                'totalGrossAmount' => '1000',
                'totalNetAmount' => '1000',
                'currency' => $currency
            ],
            'additionalData' => $this->helper->getAdditionalData()
        ];
        $auth = [];
        $auth['apiKey'] = $apiKey;
        $this->afterpay->setRest();
        $this->afterpay->set_ordermanagement('available_payment_methods');
        $this->afterpay->set_order($requestData, 'OM');
        $this->afterpay->do_request($auth, $this->getConnectionMode($connectionType), 'en');

    	$response = json_decode(json_encode($this->afterpay->order_result->return), true);
    
    	if (array_key_exists('paymentMethods', $response)) {
    		$isValid = true;
    		$message = "API key working";
    	}
    	else {
    		$isValid = false;
    		$message = "API key not working";
    	}
        $availablePaymentsResponse = [ $isValid, $message ];
        return $availablePaymentsResponse;
    }

    /**
     * @param int $paymentMethodCountry
     *
     * @return string
     */
    private function getPaymentMethodCurrency(string $paymentMethodCountry): string
    {
        switch ($paymentMethodCountry) {
            case 'NL':
            case 'BE':
            case 'DE':
            case 'AT':
            case 'FI':
                $paymentMethodCurrency = 'EUR';
                break;
            case 'CH':
                $paymentMethodCurrency = 'CHF';
                break;
            case 'NO':
                $paymentMethodCurrency = 'NOK';
                break;
            case 'SE':
                $paymentMethodCurrency = 'SEK';
                break;
            case 'DK':
                $paymentMethodCurrency = 'DKK';
                break;
            default:
                $paymentMethodCurrency= 'EUR';
                break;
        }
        return $paymentMethodCurrency;
    }

    /**
     * @param string $connectionType
     *
     * @return string
     */
    private function getConnectionMode(string $connectionType): string
    {
        if ($connectionType == 'testmode') {
            $connectionMode = 'test';
        }
        else {
            $connectionMode = 'live';
        }
        return $connectionMode;
    }

    /**
     * @param string $paymentMethodCode
     */
    public function setPaymentMethodCode(string $paymentMethodCode): void
    {
        $this->paymentMethodCode = $paymentMethodCode;
    }

    /**
     * @return string
     */
    public function getPaymentMethodCode(): string
    {
        return $this->paymentMethodCode;
    }
}
