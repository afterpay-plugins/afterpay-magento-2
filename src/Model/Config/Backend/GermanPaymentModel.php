<?php

/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */

namespace Afterpay\Payment\Model\Config\Backend;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\ValidatorException;

class GermanPaymentModel extends Value
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $scopeConfig
     * @param TypeListInterface $cacheTypeList
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $scopeConfig,
        TypeListInterface $cacheTypeList,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $registry, $scopeConfig, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * Validate current field before saving the field value
     *
     * @throws ValidatiorException*@throws \Exception
     */
    public function beforeSave()
    {
        $paymentMethod = explode('/',$this->getPath())[1];
        $installmentIsEnabled = $this->scopeConfig->getValue('payment/afterpay_de_installment/active');
        $directDebitIsEnabled = $this->scopeConfig->getValue('payment/afterpay_de_direct_debit/active');
        $invoiceIsEnabled = $this->scopeConfig->getValue('payment/afterpay_de_invoice/active');

        if ($paymentMethod == 'afterpay_de_installment' || $paymentMethod == 'afterpay_de_direct_debit') {
            if ($this->getValue() == 1 && $invoiceIsEnabled == 0) {
                throw new ValidatorException(
                    __('Please make sure you have enabled also the german Invoice payment method. Installment and Direct Debit german payment methods are only available in combination with german 14-day invoice.')
                );
            }
        } else {
            if ($this->getValue() == 0 && ($directDebitIsEnabled == 1 || $installmentIsEnabled == 1)) {
                throw new ValidatorException(
                    __('Please make sure you have disabled the Installment or Direct Debit german payment methods if you want to disable the german “14-day invoice” payment method. Installment and Direct Debit german payment methods are only available in combination with german Digital Invoice.')
                );
            }
        }

        parent::beforeSave();
    }
}
