<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model\Config\Provider\Base;

use Afterpay\Payment\Helper\Service\Data;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use Psr\Log\LoggerInterface;
use Magento\Payment\Gateway\Config\Config;
use Afterpay\Payment\Model\Config\Advanced;

/**
 * Payment method configuration provider
 */
class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'afterpay_base';

    /**
     * @var Repository
     */
    protected $assetRepo;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var null|string
     */
    protected $code;

    /**
     * @var Session
     */
    protected $customerSession;
    /**
     * @var Random
     */
    protected $random;

    private $localeResolver;

    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * @param Config $config
     * @param Repository $assetRepo
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param LoggerInterface $logger
     * @param Session $customerSession
     * @param Random $random
     * @param Advanced $advancedConfig
     * @param string|null $code
     */
    public function __construct(
        Config $config,
        Repository $assetRepo,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        ResolverInterface $localeResolver,
        LoggerInterface $logger,
        Session $customerSession,
        Random $random,
        Advanced $advancedConfig,
        string $code = null
    ) {
        $this->assetRepo = $assetRepo;
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        $this->logger = $logger;
        $this->config = $config;
        $this->code = $code;
        $this->customerSession = $customerSession;
        $this->random = $random;
        $this->localeResolver = $localeResolver;
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    public function getConfig(): array
    {
        return [
            'payment' => [
                $this->code => [
                    'code' => $this->code,
                    'payment_icon' => Data::AFTERPAY_SVG_ICON,
                    'title' => $this->config->getValue('title'),
                    'description' => $this->config->getValue('description'),
                    'allowed_countries' => $this->config->getValue('specificcountry'),
                    'allowspecific' => $this->config->getValue('allowspecific'),
                    'success' => $this->config->getValue('success'),
                    'testmode' => $this->config->getValue('testmode'),
                    'testmode_label' => Data::TEST_MODE_LABEL,
                    'tracking_id' => $this->config->getValue('tracking_id'),
                    'tracking_domain' => $this->config->getValue('tracking_domain'),
                    'can_coc' => $this->config->getValue('coc'),
                    'can_bankaccount' => $this->config->getValue('bankaccount'),
                    'can_dob' => $this->canShowDob(),
                    'can_company_name' => $this->config->getValue('company_name'),
                    'can_legal_form' => $this->canShowLegalForm(),
                    'can_ssn' => $this->config->getValue('ssn'),
                    'terms_and_conditions' => $this->config->getValue('terms_and_conditions'),
                    'can_privacy' => $this->config->getValue('privacy'),
                    'can_gender' => $this->config->getValue('gender'),
                    'can_number' => $this->config->getValue('number'),
                    'unique_id' => $this->getCustomerUniqueId(),
                    'phone_fallback' => $this->config->getValue('phone_fallback'),
                    'gander_fallback' => $this->config->getValue('gander_fallback'),
                    'flex_information' => $this->getAccountInformation(),
                    'can_show_flex' => $this->canShowFlex(),
                    'rest_merchant_id' => $this->getMerchantId(),
                    'terms_locale' => $this->getTermsLocale(),
                    'can_show_fields' => $this->canShowFields(),
                ]
            ],
        ];
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    protected function getCustomerUniqueId(): string
    {
        if ($this->customerSession->getCustomerUniqueId()) {
            return $this->customerSession->getCustomerUniqueId();
        }
        $uniqueId = $this->random->getUniqueHash();
        $this->customerSession->setCustomerUniqueId($uniqueId);

        return $uniqueId;
    }

    /**
     * @return array|null
     */
    private function getAccountInformation(): ?array
    {
        if ($this->canShowFlex()) {
            return $this->customerSession->getAccountInformation();
        }

        return [];
    }

    /**
     * @return bool
     */
    private function canShowFlex(): bool
    {
        return in_array($this->code, Data::$flexAllowedInfoList, true);
    }

    /**
     * Is not SOAP method merchant ID. Used for REST methods for T&C and privacy links.
     */
    private function getMerchantId(): ?string
    {
        return $this->config->getValue('rest_merchant_id');
    }

    /**
     * Return locale to use for T&C and  privacy URLs i.e. at_de or nl_nl
     * First part is storefront language, the second is methods country code
     *
     * @return string
     */
    private function getTermsLocale(): string
    {
        // first part of locale (en_US) is language code
        $lang = explode('_', $this->localeResolver->getLocale())[0];
        // in case none selected, fallback to NL
        $specificCountries = $this->config->getValue('specificcountry') ?: 'NL';

        $countries = explode(',', $specificCountries);
        $country = strtolower(array_shift($countries));

        return implode('_', [$lang, $country]);
    }

    /**
     * Return whether the dob field should be visible for selected country
     *
     * @return bool
     */
    private function canShowDob(): bool
    {
        // The list of countries where Date of Birth field is mandatory
        $mandatoryCountries = ['DE', 'AT', 'CH', 'DK', 'NL', 'BE'];
        if (in_array($this->config->getValue('specificcountry'), $mandatoryCountries)) {
            return true;
        }

        return false;
    }

    /**
     * Return whether the legal form field should be visible for selected country
     *
     * @return bool
     */
    private function canShowLegalForm(): bool
    {
        // The list of countries where legal form field is mandatory
        $mandatoryCountries = ['DE','AT'];
        if (in_array($this->config->getValue('specificcountry'), $mandatoryCountries)) {
            return true;
        }

        return false;
    }

    /**
     * Return whether the payment method input field should be visible or not
     *
     * @return bool
     */
    private function canShowFields(): bool
    {
        if ($this->advancedConfig->isHostedCheckoutEnabled()) {
            return false;
        }

        return true;
    }
}
