<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Afterpay;
use Afterpay\Payment\Api\PaymentMethodFlexInterface;
use Afterpay\Payment\Helper\Service\Data;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Directory\Model\Currency;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use function in_array;

class PaymentMethodFlex implements PaymentMethodFlexInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var CheckoutSession
     */
    private $session;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * Ajax constructor.
     *
     * @param Afterpay             $afterpay
     * @param CheckoutSession      $session
     * @param Currency             $currency
     * @param Data                 $helper
     * @param AuthorizationHandler $authorizationHandler
     */
    public function __construct(
        Afterpay $afterpay,
        CheckoutSession $session,
        Currency $currency,
        Data $helper,
        AuthorizationHandler $authorizationHandler
    ) {
        $this->helper = $helper;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->authorizationHandler = $authorizationHandler;
    }

    /**
     * @param mixed $paymentMethod
     *
     * @return array|string
     * @throws GuzzleException
     */
    public function lookup($paymentMethod)
    {
        if (in_array($paymentMethod, Data::$allowedFlex, true)) {
            if($this->session->getAvailablePayments()) {
                $availablePayments = $this->session->getAvailablePayments()[$paymentMethod]['response'];
            }
            else {
                $quote = $this->session->getQuote();
                $auth = $this->helper->getConfiguration($paymentMethod, $quote->getStoreId());
                $availablePayments = $this->helper->getAvailablePayments($auth, $quote);
            }
            return $this->parseResponse($availablePayments);
        }
    }

    /**
     * @param \stdClass $response
     * @param string $methodCode
     *
     * @return bool
     */
    private function parseResponse(\stdClass $response): array
    {
        $resultResponse = [];
        if (property_exists($response, 'paymentMethods')) {
            foreach ($response->paymentMethods as $paymentMethod) {
                if ($paymentMethod->type === 'Account' && property_exists($paymentMethod, 'account')) {
                    $accountInformation = $this->parseAccountInformation($paymentMethod->account);
                    $this->session->setAccountInformation($accountInformation);
                    $resultResponse[] = $accountInformation;
                    return $resultResponse;
                }
            }
        }
        $resultResponse[] = ['error' => true];
        return $resultResponse;
    }

    /**
     * @param \stdClass $accountInformation
     *
     * @return array
     */
    private function parseAccountInformation(\stdClass $accountInformation): array
    {
        return [
            'monthlyFee' => $this->currency->format($accountInformation->monthlyFee, null, false),
            'installmentAmount' => $this->currency->format($accountInformation->installmentAmount, null, false),
            'interestRate' => $accountInformation->interestRate,
            'readMore' => property_exists($accountInformation, 'readMore') ? $accountInformation->readMore : '#'
        ];
    }
}
