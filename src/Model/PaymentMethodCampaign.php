<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Afterpay;
use Afterpay\Payment\Api\PaymentMethodCampaignInterface;
use Afterpay\Payment\Helper\Service\Data;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;
use stdClass;
use function in_array;

class PaymentMethodCampaign implements PaymentMethodCampaignInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CheckoutSession
     */
    private $session;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var int
     */
    private $storeId;
    
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * @var string
     */
    private $methodCode;

    /**
     * Ajax constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param Afterpay             $afterpay
     * @param CheckoutSession      $session
     * @param Currency             $currency
     * @param Data                 $helper
     * @param Session              $customerSession
     * @param AuthorizationHandler $authorizationHandler
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Afterpay $afterpay,
        CheckoutSession $session,
        Currency $currency,
        Data $helper,
        Session $customerSession,
        AuthorizationHandler $authorizationHandler
    ) {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->customerSession = $customerSession;
        $this->authorizationHandler = $authorizationHandler;
    }

    /**
     * @param mixed $paymentMethod
     *
     * @return array|string
     * @throws GuzzleException
     */
    public function lookup($paymentMethod)
    {
        $this->setMethodCode($paymentMethod);
        if (in_array($this->getMethodCode(), Data::$allowedCampaigns, true)) {
            if($this->session->getAvailablePayments()) {
                $availablePayments = $this->session->getAvailablePayments()[$paymentMethod]['response'];
            }
            else {
                $quote = $this->session->getQuote();
                $auth = $this->helper->getConfiguration($paymentMethod, $quote->getStoreId());
                $availablePayments = $this->helper->getAvailablePayments($auth, $quote);
            }
            return $this->parseResponse($availablePayments);
        }
    }

    /**
     * @param stdClass $response
     *
     * @return array
     */
    private function parseResponse(\stdClass $response): array
    {
        $result = [];
        $position = $this->getCampaignPosition();
        $currentPosition = 1;
        if (property_exists($response, 'paymentMethods')) {
            foreach ($response->paymentMethods as $paymentMethod) {
                if (\in_array($this->getMethodCode(), Data::$allowedCampaigns, true)) {
                    if ($paymentMethod->type === 'Invoice' && property_exists($paymentMethod, 'campaigns')) {
                        if ($currentPosition === $position) {
                            $campaign = $paymentMethod->campaigns;
                            $result[] = [
                                'campaignNumber' => $campaign->campaignNumber,
                                'title' => $paymentMethod->title,
                                'description' => $paymentMethod->tag,
                                'consumerFeeAmount' => $this->currency->format(
                                    $campaign->consumerFeeAmount,
                                    null,
                                    false
                                ),
                            ];
                            break;
                        }
                        $currentPosition++;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Gets position to which use from the response, as the response can contain multiple campaign invoices
     *
     * @return int|null
     */
    private function getCampaignPosition(): int
    {
        $positionMapping = [
            'campaign' => 1,
            'campaign2' => 2,
            'campaign3' => 3,
        ];
        $splitMethodCode = explode('_', $this->getMethodCode());
        $normalizedString = end($splitMethodCode);
        return $positionMapping[$normalizedString] ?? 0;
    }

    /**
     * @return string
     */
    public function getMethodCode(): string
    {
        return $this->methodCode;
    }

    /**
     * @param string $methodCode
     */
    public function setMethodCode(string $methodCode): void
    {
        $this->methodCode = $methodCode;
    }
}
