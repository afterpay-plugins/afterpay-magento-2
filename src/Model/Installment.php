<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Payment\Api\InstallmentInterface;
use Afterpay\Afterpay;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Installment implements InstallmentInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var CheckoutSession
     */
    private $session;

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * Ajax constructor
     *
     * @param Afterpay $afterpay
     * @param CheckoutSession $session
     * @param Data $helper
     * @param AuthorizationHandler $authorizationHandler
     * @param StoreManagerInterface $storeManager
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        Afterpay $afterpay,
        CheckoutSession $session,
        Data $helper,
        AuthorizationHandler $authorizationHandler,
        StoreManagerInterface $storeManager,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->helper = $helper;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->authorizationHandler = $authorizationHandler;
        $this->storeManager = $storeManager;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param mixed $paymentMethod
     *
     * @return array|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function lookup($paymentMethod)
    {
        if (\in_array($paymentMethod, Data::$allowedInstalment, true)) {
            if($this->session->getAvailableInstallmentPlans()) {
                $availableInstallmentPlans = $this->session->getAvailableInstallmentPlans();
            }
            else {
                $quote = $this->session->getQuote();
                $auth = $this->helper->getConfiguration($paymentMethod, $quote->getStoreId());
                $availableInstallmentPlans = $this->getAvailableInstallmentPlans($auth, $quote);
            }
            return $this->parseResponse($availableInstallmentPlans);
        }
    }

    /**
     * @param \stdClass $response
     *
     * @return array
     */
    private function parseResponse(\stdClass $response): array
    {
        $result = [];
        $resultResponse = [];
        $currentNumElements = 0;
        $maxElements = 5; // Max number of allowed installment options is 5
        if (property_exists($response, 'availableInstallmentPlans')) {
            $result['installmentMonths'] = $this->getInstallmentMonths($response->availableInstallmentPlans);
            foreach ($response->availableInstallmentPlans as $installmentPlan) {
                if (++$currentNumElements == $maxElements+1) break;
                $result['value'] = $installmentPlan->installmentProfileNumber;
                $result['totalAmount'] = $this->priceCurrency->format($installmentPlan->totalAmount, false);
                $result['installmentAmount'] = $this->priceCurrency->format($installmentPlan->installmentAmount, false);
                $result['startupFee'] = $this->priceCurrency->format($installmentPlan->startupFee, false);
                $result['monthlyFee'] = $this->priceCurrency->format($installmentPlan->monthlyFee, false);
                $result['basketAmount'] = $installmentPlan->basketAmount;
                $result['totalAmount_nonformatted'] = $installmentPlan->totalAmount;
                $result['differenceAmount'] = $installmentPlan->totalAmount - $installmentPlan->basketAmount;
                $result['readMore'] = property_exists($installmentPlan, 'readMore') ? $installmentPlan->readMore : '#';
                $result['effectiveAnnualPercentageRate'] = $installmentPlan->effectiveAnnualPercentageRate;
                $result['numberOfInstallments'] = $installmentPlan->numberOfInstallments;
                $result['effectiveInterestRate'] = $installmentPlan->effectiveInterestRate;
                $result['interestRate'] = $installmentPlan->interestRate;
                $result['optionText'] = sprintf(
                    __(
                        '<b>%s per month</b><br/><span class="optionTextLower">For %s months</span>'
                    )->render(),
                    $result['installmentAmount'],
                    $result['numberOfInstallments']
                );
                $resultResponse[] = $result;
            }
            return $resultResponse;
        }
        $resultResponse[] = ['error' => true];
        return $resultResponse;
    }

    /**
     * @param array $availableInstallmentPlans
     *
     * @return string
     */
    private function getInstallmentMonths($availableInstallmentPlans){
        $number_of_installments_array = array_unique( array_column( $availableInstallmentPlans, 'numberOfInstallments' ) );
        sort($number_of_installments_array);
        $installment_amounts = '';

        foreach( $number_of_installments_array as $installment){
            if ( count($number_of_installments_array) >= 2 && $number_of_installments_array[count($number_of_installments_array) - 2] == $installment ) {
                $installment_amounts .= $installment . ' ' . __('or') . ' ';
            }
            elseif ( $number_of_installments_array[count($number_of_installments_array) - 1] == $installment ) {
                $installment_amounts .= $installment;
            }
            else {
                $installment_amounts .= $installment . ', ';
            }
        }

        return $installment_amounts;
    }

    /**
     * @param array $auth
     * @param object $quote
     * 
     * @return object
     */
    private function getAvailableInstallmentPlans(array $auth, object $quote){
        $requestData = [
            'amount' => round((float)$quote->getGrandTotal(), 2)
        ];
        $this->afterpay->setRest();
        $this->afterpay->set_ordermanagement('installmentplans_lookup');
        $this->afterpay->set_order($requestData, 'OM');
        $this->afterpay->do_request(
            $auth,
            $auth['mode'],
            $this->helper->getCurrentLocaleNormalized()
        );
        return $this->afterpay->order_result->return;
    }
}
