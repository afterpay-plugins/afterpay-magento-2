<?php
/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Afterpay;
use Afterpay\Payment\Api\MethodsFieldsReceiverInterface;
use Afterpay\Payment\Api\PaymentMethodCampaignInterface;
use Afterpay\Payment\Helper\Service\Data;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Locale\Resolver;
use stdClass;

use function in_array;

class MethodsFieldsReceiver implements MethodsFieldsReceiverInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CheckoutSession
     */
    private $session;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var int
     */
    private $storeId;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * @var string
     */
    private $methodCode;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Resolver
     */
    private $resolver;

    /**
     * Ajax constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param Afterpay $afterpay
     * @param CheckoutSession $session
     * @param Currency $currency
     * @param Data $helper
     * @param Session $customerSession
     * @param AuthorizationHandler $authorizationHandler
     * @param StoreManagerInterface $storeManager
     * @param Resolver $resolver
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Afterpay $afterpay,
        CheckoutSession $session,
        Currency $currency,
        Data $helper,
        Session $customerSession,
        AuthorizationHandler $authorizationHandler,
        StoreManagerInterface $storeManager,
        Resolver $resolver
    ) {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->afterpay = $afterpay;
        $this->session = $session;
        $this->currency = $currency;
        $this->customerSession = $customerSession;
        $this->authorizationHandler = $authorizationHandler;
        $this->storeManager = $storeManager;
        $this->resolver = $resolver;
    }

    /**
     * @param mixed $paymentMethod
     *
     * @return array|string
     */
    public function availability($paymentMethod)
    {
        $this->setMethodCode($paymentMethod);
        if ((str_contains($paymentMethod, 'be') || str_contains($paymentMethod, 'nl'))
            && ! str_contains($paymentMethod, 'rest')) {
            $availablePayments = new stdClass();
        }
        else {
            if($this->session->getAvailablePayments()) {
                $availablePayments = $this->session->getAvailablePayments()[$paymentMethod]['response'];
            }
            else {
                $quote = $this->session->getQuote();
                $auth = $this->helper->getConfiguration($paymentMethod, $quote->getStoreId());
                $availablePayments = $this->helper->getAvailablePayments($auth, $quote);
            }
        }
        return $this->parseResponse($availablePayments);
    }

    /**
     * @param stdClass $response
     *
     * @return array
     */
    private function parseResponse(\stdClass $response): array
    {
        $result = [];
        $availablePaymentMethods = json_decode(json_encode($response), true);

        if (array_key_exists('paymentMethods', $availablePaymentMethods)) {
            $quote = $this->session->getQuote();
            $orderTotalGrossAmount = round((float)$quote->getGrandTotal(), 2);
            $currentLocale = $this->resolver->getLocale();
            $profileTrackingUniqueId = substr(substr(md5($quote->getId()), -6) . '-' . mt_rand(1000, 9999), 0, 11);
            foreach ($availablePaymentMethods['paymentMethods'] as $paymentMethod) {
                if ($paymentMethod['type'] == $this->getPaymentMethodType()) {
					if ($paymentMethod['type'] == 'Invoice') {
						if (str_contains($this->getMethodCode(), 'direct_debit') && array_key_exists('directDebit', $paymentMethod)
							&& $paymentMethod['directDebit']['available'] == true
						) {
                            $result = [
                                $paymentMethod['title'],
                                $paymentMethod['tag'],
                                $paymentMethod['logo'],
                                $paymentMethod['legalInfo']['termsAndConditionsUrl'],
                                $paymentMethod['legalInfo']['privacyStatementUrl'],
                                $paymentMethod['legalInfo']['requiresCustomerConsent'],
                                $paymentMethod['legalInfo']['text'],
                                $orderTotalGrossAmount,
                                $currentLocale,
                                $profileTrackingUniqueId 
                            ];
                            if($this->session->getMethodCountry() === 'NL') {
                                if(isset($paymentMethod['legalInfo']['codeOfConduct'])) {
                                    $result[10] = $paymentMethod['legalInfo']['codeOfConduct'];   
                                }
                            }
							break;
						}
						elseif (str_contains($this->getMethodCode(), 'campaign') && array_key_exists('campaigns', $paymentMethod)) {
                            $result = [
                                $paymentMethod['title'],
                                $paymentMethod['tag'],
                                $paymentMethod['logo'],
                                $paymentMethod['legalInfo']['termsAndConditionsUrl'],
                                $paymentMethod['legalInfo']['privacyStatementUrl'],
                                $paymentMethod['legalInfo']['requiresCustomerConsent'],
                                $paymentMethod['legalInfo']['text'],
                                $orderTotalGrossAmount,
                                $currentLocale,
                                $profileTrackingUniqueId 
                            ];
                            if($this->session->getMethodCountry() === 'NL') {
                                if(isset($paymentMethod['legalInfo']['codeOfConduct'])) {
                                    $result[10] = $paymentMethod['legalInfo']['codeOfConduct'];   
                                }
                            }
							break;
						}
						else {
							if (! str_contains( $this->getMethodCode(), 'direct_debit') && ! str_contains($this->getMethodCode(), 'campaign')
								&& ! array_key_exists('directDebit', $paymentMethod) && ! array_key_exists('campaigns', $paymentMethod)
							) {
                                $result = [
                                    $paymentMethod['title'],
                                    $paymentMethod['tag'],
                                    $paymentMethod['logo'],
                                    $paymentMethod['legalInfo']['termsAndConditionsUrl'],
                                    $paymentMethod['legalInfo']['privacyStatementUrl'],
                                    $paymentMethod['legalInfo']['requiresCustomerConsent'],
                                    $paymentMethod['legalInfo']['text'],
                                    $orderTotalGrossAmount,
                                    $currentLocale,
                                    $profileTrackingUniqueId 
                                ];
                                if($this->session->getMethodCountry() === 'NL') {
                                    if(isset($paymentMethod['legalInfo']['codeOfConduct'])) {
                                        $result[10] = $paymentMethod['legalInfo']['codeOfConduct'];   
                                    }
                                }
								break;
							}
						}
					}
					else {
                        $result = [
                            $paymentMethod['title'],
                            $paymentMethod['tag'],
                            $paymentMethod['logo'],
                            $paymentMethod['legalInfo']['termsAndConditionsUrl'],
                            $paymentMethod['legalInfo']['privacyStatementUrl'],
                            $paymentMethod['legalInfo']['requiresCustomerConsent'],
                            $paymentMethod['legalInfo']['text'],
                            $orderTotalGrossAmount,
                            $currentLocale,
                            $profileTrackingUniqueId 
                        ];
                        if($this->session->getMethodCountry() === 'NL') {
                            if(isset($paymentMethod['legalInfo']['codeOfConduct'])) {
                                $result[10] = $paymentMethod['legalInfo']['codeOfConduct'];   
                            }
                        }
						break;
					}
				}
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getMethodCode(): string
    {
        return $this->methodCode;
    }

    /**
     * @param string $methodCode
     */
    public function setMethodCode(string $methodCode): void
    {
        $this->methodCode = $methodCode;
    }

    /**
     * @return string
     */
    public function getPaymentMethodType(): string
    {
        if (str_contains($this->getMethodCode(), 'installment' )) {
			$paymentMethodType = 'Installment';
		}
		elseif (str_contains($this->getMethodCode(), 'flex')) {
			$paymentMethodType = 'Account';
		}
        elseif (str_contains($this->getMethodCode(), 'payinx')) {
            $paymentMethodType = 'PayinX';
        }
		else {
			$paymentMethodType = 'Invoice';
		}

		return $paymentMethodType;
    }
}
