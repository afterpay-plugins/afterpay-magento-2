<?php
/**
 * Copyright (c) 2022  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2022 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Api\BankAccountVerificationInterface;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class BankAccountVerification implements BankAccountVerificationInterface
{
    /**
     * @var string
     */
    private $paymentMethodCode;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param AfterpayFactory $afterpay
     * @param Data $helper
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        AfterpayFactory $afterpay,
        Data $helper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->afterpay = $afterpay->create();
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param $paymentMethodCode
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function verify($paymentMethodCode, $bankNumber)
    {
        $isValid = true;
        $message = '';

        $this->setPaymentMethodCode($paymentMethodCode);

        $auth = $this->getConfiguration();
        $requestData = [
            'bankAccount' => $bankNumber
        ];
        $this->afterpay->setRest();
        $this->afterpay->set_ordermanagement('validate_bankaccount');
        $this->afterpay->set_order($requestData, 'OM');
        $this->afterpay->do_request(
            $auth,
            $auth['mode'],
            $this->helper->getCurrentLocaleNormalized()
        );

        if (isset($this->afterpay->order_result->return->isValid)) {
            if (!$this->afterpay->order_result->return->isValid) {
                $isValid = false;
            }
        }

        if (!$isValid) {
            if (isset($this->afterpay->order_result->return->customerFacingMessage)) {
                if ($this->afterpay->order_result->return->customerFacingMessage === '') {
                    $message = 'The entered bank details are invalid, please check and correct.';
                } else {
                    $message = $this->afterpay->order_result->return->customerFacingMessage;
                }
            } else {
                $message = 'The entered bank details are invalid, please check and correct.';
            }
        } else {
            $message = 'Bank account validated';
        }

        $message = __($message)->render();

        return [
            (isset($this->afterpay->order_result->return->isValid) ? $this->afterpay->order_result->return->isValid : true),
            $message
        ];
    }

    /**
     * @return array
     */
    private function getConfiguration()
    {
        $connectionType = (int)$this->loadConfig('testmode');
        $result['modus'] = $this->getConnectionType($connectionType);
        $result['mode'] = $this->getConnectionType($connectionType, true);
        $result['apiKey'] = $this->loadConfig(sprintf('%s_api_key', $result['modus']));

        return $result;
    }

    /**
     * @param int $connectionType
     * @param bool $alt
     *
     * @return string
     */
    public function getConnectionType(int $connectionType, bool $alt = false): string
    {
        $connectionTypeMapped = ['production', 'testmode'];
        if ($alt) {
            $connectionTypeMapped = ['live', 'test'];
        }
        return $connectionTypeMapped[$connectionType];
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    private function loadConfig(string $path)
    {
        $path = sprintf('payment/%s/%s', $this->getPaymentMethodCode(), $path);
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, 'default');
    }

    /**
     * @param string $paymentMethodCode
     */
    public function setPaymentMethodCode(string $paymentMethodCode): void
    {
        $this->paymentMethodCode = $paymentMethodCode;
    }

    /**
     * @return string
     */
    public function getPaymentMethodCode(): string
    {
        return $this->paymentMethodCode;
    }

    /**
     * @return int|null
     */
    public function getStoreId(): ?int
    {
        return $this->storeId;
    }

    /**
     * @param int $storeId
     */
    public function setStoreId(int $storeId): void
    {
        $this->storeId = $storeId;
    }
}
