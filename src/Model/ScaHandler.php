<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Model;

use Afterpay\Afterpay;
use Afterpay\Payment\Helper\Service\Data;
use Magento\Sales\Api\Data\OrderInterface;

class ScaHandler
{
    public const ORDER_STATUS_SCA_PENDING_LABEL = 'Afterpay SCA Pending';
    public const ORDER_STATUS_HCP_PENDING_LABEL = 'Riverty HCP Pending';
    public const ORDER_STATUS_SCA_FAILED_LABEL = 'Afterpay SCA Failed';
    public const ORDER_STATUS_HCP_FAILED_LABEL = 'Riverty HCP Failed';
    public const ORDER_STATUS_SCA_PENDING_CODE = 'afterpay_sca_pending';
    public const ORDER_STATUS_HCP_PENDING_CODE = 'riverty_hcp_pending';
    public const ORDER_STATUS_SCA_FAILED_CODE = 'afterpay_sca_failed';
    public const ORDER_STATUS_HCP_FAILED_CODE = 'riverty_hcp_failed';

    /**
     * @var AuthorizationHandler
     */
    private $authorizationHandler;

    /**
     * @var Afterpay
     */
    private $afterpay;

    /**
     * @var Data
     */
    private $helper;

    /**
     * ScaHandler constructor.
     *
     * @param AuthorizationHandler $authorizationHandler
     * @param Afterpay             $afterpay
     * @param Data                 $helper
     */
    public function __construct(
        AuthorizationHandler $authorizationHandler,
        Afterpay $afterpay,
        Data $helper
    ) {
        $this->authorizationHandler = $authorizationHandler;
        $this->afterpay = $afterpay;
        $this->helper = $helper;
    }

    /**
     * @param OrderInterface $order
     *
     * @return \stdClass
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOrderStatus(OrderInterface $order): \stdClass
    {
        $paymentMethod = $order->getPayment()->getMethodInstance()->getCode();
        $this->authorizationHandler->setPaymentMethodCode($paymentMethod);
        $this->authorizationHandler->setStoreId((int) $order->getStoreId());
        $auth = $this->authorizationHandler->getConfiguration();
        $requestData = [
            'ordernumber' => $order->getIncrementId()
        ];
        $this->afterpay->setRest();
        $this->afterpay->set_ordermanagement('get_order');
        $this->afterpay->set_order($requestData, 'OM');
        $this->afterpay->do_request(
            $auth,
            $auth['mode'],
            $this->helper->getCurrentLocaleNormalized()
        );
        return $this->afterpay->order_result->return;
    }
}
