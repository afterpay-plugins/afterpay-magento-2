<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
-->
<include xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_include.xsd">
    <group id="afterpay_nl_direct_debit" translate="label" type="text" sortOrder="220" showInDefault="1" showInWebsite="1" showInStore="1">
        <label>Direct Debit (Legacy SOAP)</label>
        <attribute type="activity_path">payment/afterpay_nl_direct_debit/active</attribute>
        <field id="active" translate="label" type="select" sortOrder="100" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Enable payment method</label>
            <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
            <config_path>payment/afterpay_nl_direct_debit/active</config_path>
        </field>
        <field id="max_order_total" translate="label" type="text" sortOrder="110" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Maximum order total</label>
            <config_path>payment/afterpay_nl_direct_debit/max_order_total</config_path>
        </field>
        <field id="test_mode_active" translate="label" type="select" sortOrder="120" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Connection mode</label>
            <source_model>Afterpay\Payment\Model\Config\Source\ConnectionTypeSoap</source_model>
            <config_path>payment/afterpay_nl_direct_debit/testmode</config_path>
        </field>
        <field id="restrict" translate="label comment tooltip" type="select" sortOrder="130" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Enable IP restriction</label>
            <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
            <config_path>payment/afterpay_nl_direct_debit/restrict</config_path>
            <comment>Use the configuration option "Allowed Developer IP" in the "Developer Settings" to restrict this payment method to the configured IP address.</comment>
        </field>
        <field id="test_mode_merchant_id" translate="label" type="text" sortOrder="210" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Test mode merchant ID</label>
            <config_path>payment/afterpay_nl_direct_debit/testmode_merchant_id</config_path>
        </field>
        <field id="test_mode_portfolio_id" translate="label" type="text" sortOrder="220" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Test mode portfolio ID</label>
            <config_path>payment/afterpay_nl_direct_debit/testmode_portfolio_id</config_path>
        </field>
        <field id="test_mode_password" translate="label" type="obscure" sortOrder="230" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Test mode password</label>
            <backend_model>Magento\Config\Model\Config\Backend\Encrypted</backend_model>
            <config_path>payment/afterpay_nl_direct_debit/testmode_password</config_path>
        </field>
        <field id="production_merchant_id" translate="label" type="text" sortOrder="240" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Production mode merchant ID</label>
            <config_path>payment/afterpay_nl_direct_debit/production_merchant_id</config_path>
        </field>
        <field id="production_portfolio_id" translate="label" type="text" sortOrder="250" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Production mode portfolio ID</label>
            <config_path>payment/afterpay_nl_direct_debit/production_portfolio_id</config_path>
        </field>
        <field id="production_password" translate="label" type="obscure" sortOrder="260" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Production mode password</label>
            <backend_model>Magento\Config\Model\Config\Backend\Encrypted</backend_model>
            <config_path>payment/afterpay_nl_direct_debit/production_password</config_path>
        </field>
        <field id="sort_order" translate="label" type="text" sortOrder="290" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Payment method display position</label>
            <config_path>payment/afterpay_nl_direct_debit/sort_order</config_path>
        </field>
        <group id="advanced_settings" translate="label" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="320">
            <label>Advanced settings</label>
            <field id="min_order_total" translate="label" type="text" sortOrder="125" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Minimum Order Total</label>
                <config_path>payment/afterpay_nl_direct_debit/min_order_total</config_path>
            </field>
            <field id="exludeships" translate="label" type="multiselect" sortOrder="130" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Exclude Riverty for shipping methods</label>
                <source_model>Magento\Shipping\Model\Config\Source\Allmethods</source_model>
                <config_path>payment/afterpay_nl_direct_debit/excludeships</config_path>
                <can_be_empty>1</can_be_empty>
            </field>
            <field id="allowspecific" translate="label" type="allowspecific" sortOrder="140" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Payment from Applicable Countries</label>
                <source_model>Magento\Payment\Model\Config\Source\Allspecificcountries</source_model>
                <config_path>payment/afterpay_nl_direct_debit/allowspecific</config_path>
            </field>
            <field id="specificcountry" translate="label" type="multiselect" sortOrder="150" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Payment from Specific Countries</label>
                <source_model>Magento\Directory\Model\Config\Source\Country</source_model>
                <config_path>payment/afterpay_nl_direct_debit/specificcountry</config_path>
            </field>
            <field id="allowspecificgroup" translate="label" type="select" sortOrder="160" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Payment for Applicable Customer Groups</label>
                <source_model>Afterpay\Payment\Model\Config\Source\AllspecificGroups</source_model>
                <config_path>payment/afterpay_nl_direct_debit/allowspecificgroup</config_path>
            </field>
            <field id="specificgroup" translate="label" type="multiselect" sortOrder="170" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Allowed Customer Groups</label>
                <source_model>Afterpay\Payment\Model\Config\Source\CustomerGroup</source_model>
                <config_path>payment/afterpay_nl_direct_debit/specificgroup</config_path>
                <depends>
                    <field id="allowspecificgroup">1</field>
                </depends>
            </field>
            <field id="debug" translate="label comment" type="select" sortOrder="180" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Debug Logfile</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_nl_direct_debit/debug</config_path>
                <comment>Log file location: /var/log/afterpay/</comment>
            </field>
            <field id="debug_email" translate="label" type="select" sortOrder="190" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Debug Email</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_nl_direct_debit/debug_email</config_path>
            </field>
            <field id="debug_email_address" translate="label" type="text" sortOrder="200" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Debug Email To</label>
                <config_path>payment/afterpay_nl_digital_invoice/debug_email_address</config_path>
                <comment>Separated by commas</comment>
                <depends>
                    <field id="debug_email">1</field>
                </depends>
            </field>
            <field id="success" translate="label comment" type="text" sortOrder="230" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Success Page URL</label>
                <config_path>payment/afterpay_nl_direct_debit/success</config_path>
                <comment>The page a customer is redirected to when an order is accepted. Leave blank for default magento.</comment>
            </field>
            <field id="terms_and_conditions" translate="label" type="select" sortOrder="250" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Enable Terms and Conditions</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_nl_direct_debit/terms_and_conditions</config_path>
            </field>
            <field id="phone_field" translate="label" type="select" sortOrder="300" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Enable Phone Number Input Field</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_nl_direct_debit/number</config_path>
            </field>
            <field id="phone_fallback_field" translate="label" type="text" sortOrder="305" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Fallback Input Field For Phone Number</label>
                <config_path>payment/afterpay_nl_direct_debit/phone_fallback</config_path>
                <comment>Only can be one input field</comment>
                <depends>
                    <field id="phone_field">0</field>
                </depends>
            </field>
            <field id="gender_field" translate="label" type="select" sortOrder="320" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Enable Gender Input Field</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_nl_direct_debit/gender</config_path>
            </field>
            <field id="gender_fallback_field" translate="label" type="text" sortOrder="325" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Fallback Input Field For Gender</label>
                <config_path>payment/afterpay_nl_direct_debit/gander_fallback</config_path>
                <comment>Only can be one input field</comment>
                <depends>
                    <field id="gender_field">0</field>
                </depends>
            </field>
        </group>
    </group>
</include>
