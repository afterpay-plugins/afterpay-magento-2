<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
-->
<include xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_include.xsd">
    <group id="afterpay_dk_b2b_digital_invoice" translate="label" type="text" sortOrder="110" showInDefault="1" showInWebsite="1" showInStore="1">
        <label>B2B invoice</label>
        <attribute type="activity_path">payment/afterpay_dk_b2b_digital_invoice/active</attribute>
        <field id="active" translate="label" type="select" sortOrder="100" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Enable payment method</label>
            <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/active</config_path>
        </field>
        <field id="max_order_total" translate="label" type="text" sortOrder="160" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Maximum order total</label>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/max_order_total</config_path>
        </field>
        <field id="test_mode_active" translate="label" type="select" sortOrder="170" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Connection mode</label>
            <source_model>Afterpay\Payment\Model\Config\Source\ConnectionTypeRest</source_model>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/testmode</config_path>
        </field>
        <field id="restrict" translate="label comment tooltip" type="select" sortOrder="180" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Enable IP restriction</label>
            <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/restrict</config_path>
            <comment>Use the configuration option "Allowed Developer IP" in the "Developer Settings" to restrict this payment method to the configured IP address.</comment>
        </field>
        <field id="production_api_key" translate="label" type="text" sortOrder="180" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Production mode API key</label>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/production_api_key</config_path>
        </field>
        <field id="production_api_key_verification_button" translate="label" type="button" sortOrder="181" showInDefault="1" showInWebsite="1" showInStore="1">
            <frontend_model>Afterpay\Payment\Block\System\Config\ApiKeyVerificationButton</frontend_model>
        </field>
        <field id="testmode_api_key" translate="label" type="text" sortOrder="182" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Test mode API key</label>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/testmode_api_key</config_path>
        </field>
        <field id="testmode_api_key_verification_button" translate="label" type="button" sortOrder="183" showInDefault="1" showInWebsite="1" showInStore="1">
            <frontend_model>Afterpay\Payment\Block\System\Config\ApiKeyVerificationButton</frontend_model>
        </field>
        <field id="sort_order" translate="label" type="text" sortOrder="290" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Payment method display position</label>
            <config_path>payment/afterpay_dk_b2b_digital_invoice/sort_order</config_path>
        </field>
        <group id="advanced_settings" translate="label" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="320">
            <label>Advanced settings</label>
            <field id="exludeships" translate="label" type="multiselect" sortOrder="130" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Exclude Riverty for shipping methods</label>
                <source_model>Magento\Shipping\Model\Config\Source\Allmethods</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/excludeships</config_path>
                <can_be_empty>1</can_be_empty>
            </field>
            <field id="allowspecific" translate="label" type="allowspecific" sortOrder="130" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Payment from Applicable Countries</label>
                <source_model>Magento\Payment\Model\Config\Source\Allspecificcountries</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/allowspecific</config_path>
            </field>
            <field id="specificcountry" translate="label" type="multiselect" sortOrder="140" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Payment from Specific Countries</label>
                <source_model>Magento\Directory\Model\Config\Source\Country</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/specificcountry</config_path>
            </field>
            <field id="allowspecificgroup" translate="label" type="select" sortOrder="270" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Payment for Applicable Customer Groups</label>
                <source_model>Afterpay\Payment\Model\Config\Source\AllspecificGroups</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/allowspecificgroup</config_path>
            </field>
            <field id="specificgroup" translate="label" type="multiselect" sortOrder="280" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Allowed Customer Groups</label>
                <source_model>Afterpay\Payment\Model\Config\Source\CustomerGroup</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/specificgroup</config_path>
                <depends>
                    <field id="allowspecificgroup">1</field>
                </depends>
            </field>
            <field id="min_order_total" translate="label" type="text" sortOrder="160" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Minimum Order Total</label>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/min_order_total</config_path>
            </field>
            <field id="debug" translate="label comment" type="select" sortOrder="180" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Debug Logfile</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/debug</config_path>
                <comment>Log file location: /var/log/afterpay/</comment>
            </field>
            <field id="debug_email" translate="label" type="select" sortOrder="190" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Debug Email</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/debug_email</config_path>
            </field>
            <field id="debug_email_address" translate="label" type="text" sortOrder="200" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Debug Email To</label>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/debug_email_address</config_path>
                <comment>Separated by commas</comment>
                <depends>
                    <field id="debug_email">1</field>
                </depends>
            </field>
            <field id="success" translate="label comment" type="text" sortOrder="140" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Success Page URL</label>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/success</config_path>
                <comment>The page a customer is redirected to when an order is accepted. Leave blank for default magento.</comment>
            </field>
            <field id="terms_and_conditions" translate="label" type="select" sortOrder="150" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Enable Terms and Conditions</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/terms_and_conditions</config_path>
            </field>
            <field id="product_group_code" translate="label" type="select" sortOrder="150" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Use GroupId</label>
                <source_model>Afterpay\Payment\Model\Config\Source\ProductGroupId</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/product_group_code</config_path>
            </field>
            <field id="disable_billing_step" translate="label" type="select" sortOrder="1000" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Disable Alternative Billing Address</label>
                <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/disable_billing_step</config_path>
            </field>
            <field id="rest_merchant_id" translate="label" type="text" sortOrder="400" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Merchant ID</label>
                <config_path>payment/afterpay_dk_b2b_digital_invoice/rest_merchant_id</config_path>
            </field>
        </group>
    </group>
</include>
