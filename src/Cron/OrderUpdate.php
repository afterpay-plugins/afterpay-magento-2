<?php
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION lITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */

namespace Afterpay\Payment\Cron;

use Afterpay\Payment\Model\ScaHandler;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Framework\Event\ManagerInterface;
use Afterpay\Payment\Model\Config\Advanced;

class OrderUpdate
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ScaHandler
     */
    private $scaHandler;
    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var Advanced
     */
    private $advancedConfig;

    /**
     * OrderUpdate constructor.
     *
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder
     * @param OrderRepositoryInterface $orderRepository
     * @param ScaHandler               $scaHandler
     * @param Advanced                 $advancedConfig
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderRepositoryInterface $orderRepository,
        ScaHandler $scaHandler,
        ManagerInterface $eventManager,
        Advanced $advancedConfig
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderRepository = $orderRepository;
        $this->scaHandler = $scaHandler;
        $this->eventManager = $eventManager;
        $this->advancedConfig = $advancedConfig;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(): void
    {
        $this->handleOrders();
    }

    /**
     * @return array
     */
    private function getOrderList(): array
    {
        $date = (new \DateTime())->modify('-10 min');
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', [ScaHandler::ORDER_STATUS_SCA_PENDING_CODE, ScaHandler::ORDER_STATUS_HCP_PENDING_CODE], 'in')
            ->addFilter('created_at', $date, 'lt')
            ->create();
        return $this->orderRepository->getList($searchCriteria)->getItems();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function handleOrders(): void
    {
        $orderList = $this->getOrderList();
        foreach ($orderList as $order) {
            $orderStatusResponse = $this->scaHandler->getOrderStatus($order);
            $orderStatus = $this->parseResponse($orderStatusResponse);
            if ($orderStatus) {
                $this->updateOrderStatusSuccess($order);
                $this->eventManager->dispatch('afterpay_order_capture', ['order' => $order]);
            } else {
                $this->updateOrderStatusFail($order);
                $this->cancelOrder($order);
            }
        }
    }

    /**
     * @param OrderInterface $order
     */
    private function updateOrderStatusSuccess(OrderInterface $order): void
    {
        $order->setStatus(Order::STATE_PROCESSING)
              ->setState(Order::STATE_PROCESSING);
        if($this->advancedConfig->isHostedCheckoutEnabled()) {
            $order->addCommentToStatusHistory(
                'From cron: Payment Authorization through hosted checkout has been successfully completed by the customer'
            );
        }
        else {
            $order->addCommentToStatusHistory(
                'From cron: Strong Customer Authentication has been successfully completed by the customer'
            );
        }
        $order->setIsTransactionPending(false);
        $order->setAfterpayCaptured(1);
        $this->orderRepository->save($order);
    }

    /**
     * @param OrderInterface $order
     */
    private function updateOrderStatusFail(OrderInterface $order): void
    {
        if($this->advancedConfig->isHostedCheckoutEnabled()) {
            $order->setStatus(ScaHandler::ORDER_STATUS_HCP_FAILED_CODE)->setState(Order::STATE_PROCESSING);
            $order->addCommentToStatusHistory(
                'From cron: Payment Authorization through hosted checkout hasn\'t been successfully completed by the customer'
            );
        }
        else {
            $order->setStatus(ScaHandler::ORDER_STATUS_SCA_FAILED_CODE)->setState(Order::STATE_PROCESSING);
            $order->addCommentToStatusHistory(
                'From cron: Strong Customer Authentication hasn\'t been successfully completed by the customer'
            );
        }
        $order->setIsTransactionPending(false);
        $this->orderRepository->save($order);
    }

    /**
     * @param OrderInterface $order
     *
     * @return void
     */
    protected function cancelOrder($order)
    {
        $orderEntity = $this->orderRepository->get($order->getEntityId());
        if ($orderEntity->canCancel()) {
            $orderEntity->cancel();
            $this->orderRepository->save($orderEntity);
        }
    }

    /**
     * @param \stdClass $response
     *
     * @return bool
     */
    private function parseResponse(\stdClass $response): bool
    {
        if(property_exists($response, 'orderDetails')) {
            if ($response->orderDetails->status === 'Accepted') {
                return true;
            }
            if (in_array($response->orderDetails->status, ['Rejected', 'Cancelled'], true)) {
                return false;
            }
        }
        return false;
    }
}
