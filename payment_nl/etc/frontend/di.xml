<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2021  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2021 arvato Finance B.V.
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <type name="Magento\Checkout\Model\CompositeConfigProvider">
        <arguments>
            <argument name="configProviders" xsi:type="array">
                <item name="afterpay_nl_digital_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNL\ConfigProvider</item>
                <item name="afterpay_nl_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLDebit\ConfigProvider</item>
                <item name="afterpay_nl_digital_invoice_extra" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLExtra\ConfigProvider</item>
                <item name="afterpay_nl_business_2_business" xsi:type="object">Afterpay\Payment\Model\Config\Provider\B2BNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InvoiceRestNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_invoice_extra" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InvoiceRestExtraNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DirectDebitRestNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_b2b" xsi:type="object">Afterpay\Payment\Model\Config\Provider\B2BRestNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_payinx" xsi:type="object">Afterpay\Payment\Model\Config\Provider\PayInX\ConfigProvider</item>
            </argument>
        </arguments>
    </type>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLDebit\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceNLDebit\Config
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLExtra\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_DI_EXTRA</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceNLExtra\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\B2BNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_B2B</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\B2BNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InvoiceRestNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InvoiceRestNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InvoiceRestExtraNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_EXTRA_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InvoiceRestExtraNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DirectDebitRestNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DirectDebitRestNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\B2BRestNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_B2B</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\B2BRestNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\PayInX\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_PAYINX</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\PayInX\Config</argument>
        </arguments>
    </virtualType>
</config>
